<?php

/**
 * OrangeMantra.
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    OrangeMantra
 * @package     OM_Press
 * @author      Shiv Kr Maurya (Senior Magento Developer)
 * @copyright   Copyright (c) 2017 OrangeMantra
 */
namespace OM\Press\Block\Adminhtml\Testimonial;
 
use Magento\Backend\Block\Widget\Grid as WidgetGrid;
 
class Grid extends WidgetGrid
{
   
}