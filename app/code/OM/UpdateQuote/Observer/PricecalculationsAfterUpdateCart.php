<?php
namespace OM\UpdateQuote\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

class PricecalculationsAfterUpdateCart implements ObserverInterface
{
	protected $checkoutSession;

    public function __construct(CheckoutSession $checkoutSession) {
       $this->_checkoutSession = $checkoutSession;
    }
    
	public function execute(\Magento\Framework\Event\Observer $observer) 
	{
		  $items = $observer->getEvent()->getCart()->getItems();
		  foreach($items as $quote_item){
			  $itemID = $quote_item->getItemId();
			  $sr = $_POST['cart'][$itemID];
			  if(array_key_exists('subscribe_active',$sr)){
				  $subscribedStatus = $sr['subscribe_active'];				  
				  if($subscribedStatus == 1){
					  $quote_item->setIsSubscribed($subscribedStatus);	 	
					  $quote_item->setDescription(1);
					  $quote_item->getProduct()->setIsSuperMode(true);
					  $quote_item->save();
				  }
			  }
		  }
		  $observer->getEvent()->getCart()->getQuote()->setTotalsCollectedFlag(false)->collectTotals()->save();
		  $this->_checkoutSession->getQuote()->setTotalsCollectedFlag(false)->collectTotals()->save();
	}
}
   
