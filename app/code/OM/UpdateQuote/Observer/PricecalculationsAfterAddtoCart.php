<?php
namespace OM\UpdateQuote\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class PricecalculationsAfterAddtoCart implements ObserverInterface
{
public function execute(\Magento\Framework\Event\Observer $observer) 
{
      $subscribedStatus = 0;
      if(isset($_POST['subscribe_active'])){
    	  $subscribedStatus = $_POST['subscribe_active'];
  		}
  		
      $quote_item = $observer->getEvent()->getQuoteItem();
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      if($subscribedStatus == 1){
		$quote_item->setIsSubscribed($subscribedStatus);	 	
		$quote_item->getProduct()->setIsSuperMode(true);
	}      
}
}
