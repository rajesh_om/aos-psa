<?php
namespace OM\Homepage\Block;
class Homepage extends \Magento\Framework\View\Element\Template
{    
    protected $_productCollectionFactory;
    protected $_imageBuilder;
    protected $_optionFactory;
    protected $_productRepository;
    protected $_productImageHelper;
        
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Block\Product\ImageBuilder $_imageBuilder,
        \Magento\Catalog\Model\Product\OptionFactory $_optionFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Catalog\Helper\Image $productImageHelper,
        array $data = []
    )
    {    
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_imageBuilder = $_imageBuilder;
        $this->_optionFactory = $_optionFactory;
        $this->_productRepository = $productRepository;
        $this->_productImageHelper = $productImageHelper;
        parent::__construct($context, $data);
    }
    
    public function getProductCollection()
    {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('is_homepage', 1);
        $collection->setOrder('product_position', 'ASC');
        /*echo $collection->getSelect();
        exit();*/
        $collection->setPageSize(20);
        return $collection;
    }

    public function getImage($product, $imageId, $attributes = [])
    {
        return $this->_imageBuilder->setProduct($product)
            ->setImageId($imageId)
            ->setAttributes($attributes)
            ->create();
    }

    public function resizeImage($product, $imageId, $width, $height = null)
    {
        $resizedImage = $this->_productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }

    public function getAddToCartUrl($product)
    {
        $params = [
            'product' => $product->getId(),
            '_secure' => $this->getRequest()->isSecure()
        ];

        return $this->getUrl('checkout/cart/add', $params);
    }

    public function getOptions($product)
    {
        return $this->_optionFactory->create()->getProductOptionCollection($product);
    }

    public function getFavouriteCollection()
    {
        $favoriteCollection = $this->_productCollectionFactory->create();
        $favoriteCollection->addAttributeToSelect('*');
        $favoriteCollection->addAttributeToFilter('favourite', 1);
        $favoriteCollection->setPageSize(1);
        return $favoriteCollection;
    }
    public function getRegularCollection()
    {
        $regularCollection = $this->_productCollectionFactory->create();
        $regularCollection->addAttributeToSelect('*');
        $regularCollection->addAttributeToFilter('regular_care', 1);
        $regularCollection->setPageSize(3);
        return $regularCollection;
    }
    public function getSubscriptionCollection()
    {
        $subscriptionCollection = $this->_productCollectionFactory->create();
        $subscriptionCollection->addAttributeToSelect('*');
        $subscriptionCollection->addAttributeToFilter('skincare_subscription', 1);
        $subscriptionCollection->setPageSize(2);
        return $subscriptionCollection;
    }
    public function getEyeCaringCollection()
    {
        $subscriptionCollection = $this->_productCollectionFactory->create();
        $subscriptionCollection->addAttributeToSelect('*');
        $subscriptionCollection->addAttributeToFilter('landing_page', 1);
        $subscriptionCollection->setPageSize(2);
        return $subscriptionCollection;
    }
    public function getFirmingDuoCollection()
    {
        $subscriptionCollection = $this->_productCollectionFactory->create();
        $subscriptionCollection->addAttributeToSelect('*');
        $subscriptionCollection->addAttributeToFilter('firming', 1);
        $subscriptionCollection->setPageSize(2);
        return $subscriptionCollection;
    }
}
?>