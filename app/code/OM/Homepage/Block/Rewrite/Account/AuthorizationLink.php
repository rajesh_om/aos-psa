<?php

namespace OM\Homepage\Block\Rewrite\Account;

class AuthorizationLink extends \Magento\Customer\Block\Account\AuthorizationLink
{
    public function getLabel()
    {
        return $this->isLoggedIn() ? __('Logout') : __('Login');
    }

}