<?php
namespace OM\Regimenrecommender\Controller\Adminhtml\Index;

class Export extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory)
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager   = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $regimenCollection = $objectManager->create('OM\Regimenrecommender\Model\ResourceModel\SaveData\CollectionFactory');
        $collection = $regimenCollection->create();
        $exportdata = $collection->getData();
        /*echo '<pre>';
        print_r($exportdata);
        exit();*/
        $regimenData = [];
        foreach ($exportdata as $data) {
            $regimenData[] = [
                    'Id'                    => $data['id'],
                    'FirstName'             => $data['fname'],
                    'Member'                => $data['member'],
                    'Age'                   => $data['age'],
                    'Gender'                => $data['gender'],
                    'Pregnant'              => $data['pregnant_or_breastfeeding'],
                    'Smoke'                 => $data['smoke'],
                    'Alcohol Consumption'   => $data['Alcohol_consumption'],
                    'Hours on computer'     => $data['hours_spent_on_computer'],
                    'Sleep per night'       => $data['sleep_per_night'],
                    'Excercise'             => $data['exercise_per_week'],
                    'Products On Skin'      => $data['products_on_skin'],
                    'Skin Brands'           => $data['skin-brands'],
                    'Skin Problems'         => $data['skin-problem'],
                    'Email'                 => $data['email']
                    
            ];
        }
        $fh = fopen('php://temp', 'rw');
        # write out the headers
        fputcsv($fh, array_keys(current($regimenData)));

        # write out the data
        foreach ( $regimenData as $row ) {
                fputcsv($fh, $row);
        }
        rewind($fh);
        $csv = stream_get_contents($fh);
        fclose($fh);
        $filename = 'exportregimen.csv';
        header( 'Content-Type: application/csv' );
        header( 'Content-Disposition: attachment; filename="' . $filename . '";' );

        echo $csv;
        exit();
        return $this->_pageFactory->create();
    }
}