<?php

namespace OM\Regimenrecommender\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use OM\Regimenrecommender\Model\SaveDataFactory;



class Post extends \Magento\Framework\App\Action\Action
{
    /**
     * Index action
     *
     * @return void
     */
protected $_modelSaveDataFactory;    
public function __construct(
     \Magento\Framework\App\Action\Context $context,
     SaveDataFactory $modelSaveDataFactory
) {
    parent::__construct($context);
    $this->_modelSaveDataFactory = $modelSaveDataFactory;

}
 public function execute()
    {
            
            $saveDataModel = $this->_modelSaveDataFactory->create();
            $data = $this->getRequest()->getPostValue();
            $saveDataModel->setData($data);
            $saveDataModel->setData('skin-problem',implode(',',$data['skin-problem']));
            $saveDataModel->save();
            $redirecturl = $this->_redirect->getRefererurl().'Result/Index';
            
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setUrl($redirecturl);
    }
           
}
