<?php

namespace OM\Regimenrecommender\Controller\Result;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    const XML_PATH_EMAIL_RECIPIENT_EMAIL = 'trans_email/ident_support/email';
    protected $_inlineTranslation;
    protected $_transportBuilder;
    protected $_scopeConfig;
    protected $_logLoggerInterface;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $loggerInterface,
        array $data = []
         
        )
    {
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->_logLoggerInterface = $loggerInterface;
        $this->messageManager = $context->getMessageManager();
         
         
        parent::__construct($context);
         
         
    }
    public function execute()
    {
       
        $data = $this->getRequest()->getPostValue();
        $objectManager  = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager   = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseUrl        = $storeManager->getStore()->getBaseUrl();   
        if($data){    
        $question = $objectManager->create('OM\Regimenrecommender\Model\SaveData');
        $question->setData($data);
        $question->setData('skin-problem',implode('/',$data['skin-problem']));
        $question->save();
       $skin_problems = implode('/',$data['skin-problem']);
        if($data['gender'] != 'M'){
           $skin_problems = $data['pregnant_or_breastfeeding'].'+'.$skin_problems;
        }
        else{
        $skin_problems = $skin_problems;
        }
        $gender = $data["gender"];

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        
        $query = "SELECT `daytime`,`nighttime`,`weekly` FROM regimen_quiz WHERE gender = '$gender' and skin_problem ='$skin_problems'";
        $result = $connection->fetchAll($query);
        foreach($result as $_res){
            $daytime= $_res['daytime'];
            $nighttime= $_res['nighttime'];
            $weekly= $_res['weekly'];
        }

        $explodeDay = array_map('trim', explode(',', $daytime));
        $explodeNight = array_map('trim', explode(',', $nighttime));
        $explodeWeekly = array_map('trim', explode('+', $weekly));
        $productDayCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
        $productNightCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
        $productWeeklyCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
        $dayCollection = $productDayCollection->addAttributeToSelect('*')->addAttributeToFilter('product_code',array($explodeDay))->setOrder('regimen_order', 'ASC')->load();
        $NightCollection = $productNightCollection->addAttributeToSelect('*')->addAttributeToFilter('product_code',array($explodeNight))->setOrder('regimen_order', 'ASC')->load();
        $WeeklyCollection = $productWeeklyCollection->addAttributeToSelect('*')->addAttributeToFilter('product_code',array($explodeWeekly))->setOrder('regimen_order', 'ASC')->load();
        $skin_concerns = $data['skin-problem'];
        $magentoDateObject = $objectManager->create('Magento\Framework\Stdlib\DateTime\TimezoneInterface');
        $magentoTime =  $magentoDateObject->date()->format('d M Y');

        $savedataCollectionDay = [];
        foreach ($dayCollection as $key => $dayCollection) {
            $savedataCollectionDay[] = $dayCollection->getName();
        }
        $savedataCollectionNight = [];
        foreach ($productNightCollection as $key => $productNightCollection) {
            $savedataCollectionNight[] = $productNightCollection->getName();
        }
        if($WeeklyCollection->getData()){
            $savedataCollectionWeekly = [];
            foreach ($WeeklyCollection as $key => $WeeklyCollection) {
                $savedataCollectionWeekly[] = $WeeklyCollection->getName();
            }
        }

        $savedataCollectionWeekly['0'] = isset($savedataCollectionWeekly['0'])? $savedataCollectionWeekly['0'] : '';
        $savedataCollectionWeekly['1'] = isset($savedataCollectionWeekly['1'])? $savedataCollectionWeekly['1'] : '';

        if(!empty($data) && !empty($data['email'])){
            $customerMail = $data['email'];
            $this->_inlineTranslation->suspend();
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
             
            $sender = [
                'name' => 'Allies of Skin',
                'email' => 'ask@mail.alliesofskin.com'
            ];
             
            $sentToEmail = $data['email'];
            $sentToName =  $data['fname'];
             
            if($savedataCollectionWeekly['1'] != ''){
            $transport = $this->_transportBuilder
            ->setTemplateIdentifier('regimen_mail')
            ->setTemplateOptions(
                [
                    'area' => 'frontend',
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
                )
                ->setTemplateVars([
                    'email'                 => $data['email'],
                    'name'                  => $data['fname'],
                    'date'                  => $magentoTime,
                    'skin_concern_one'      => $skin_concerns['0'],
                    'skin_concern_two'      => $skin_concerns['1'],
                    'skin_concern_three'    => $skin_concerns['2'],
                    'dayproductname1'       => $savedataCollectionDay['0'],
                    'dayproductname2'       => $savedataCollectionDay['1'],
                    'dayproductname3'       => $savedataCollectionDay['2'],
                    'dayproductname4'       => $savedataCollectionDay['3'],
                    'nightproductname1'     => $savedataCollectionNight['0'],
                    'nightproductname2'     => $savedataCollectionNight['1'],
                    'nightproductname3'     => $savedataCollectionNight['2'],
                    'nightproductname4'     => $savedataCollectionNight['3'],
                    'weeklyproductname1'    => $savedataCollectionWeekly['0'],
                    'weeklyproductname2'    => $savedataCollectionWeekly['1'],

                ])
                ->setFrom($sender)
                ->addTo($sentToEmail,$sentToName)
                ->getTransport();
            }
            elseif ($savedataCollectionWeekly['1'] == '' && $savedataCollectionWeekly['0'] != '') {
                 $transport = $this->_transportBuilder
                ->setTemplateIdentifier('regimen_mail_weekone')
                ->setTemplateOptions(
                [
                    'area' => 'frontend',
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
                )
                ->setTemplateVars([
                    'email'                 => $data['email'],
                    'name'                  => $data['fname'],
                    'date'                  => $magentoTime,
                    'skin_concern_one'      => $skin_concerns['0'],
                    'skin_concern_two'      => $skin_concerns['1'],
                    'skin_concern_three'    => $skin_concerns['2'],
                    'dayproductname1'       => $savedataCollectionDay['0'],
                    'dayproductname2'       => $savedataCollectionDay['1'],
                    'dayproductname3'       => $savedataCollectionDay['2'],
                    'dayproductname4'       => $savedataCollectionDay['3'],
                    'nightproductname1'     => $savedataCollectionNight['0'],
                    'nightproductname2'     => $savedataCollectionNight['1'],
                    'nightproductname3'     => $savedataCollectionNight['2'],
                    'nightproductname4'     => $savedataCollectionNight['3'],
                    'weeklyproductname1'    => $savedataCollectionWeekly['0'],

                ])
                ->setFrom($sender)
                ->addTo($sentToEmail,$sentToName)
                ->getTransport();
            }
               
              else{
                  $transport = $this->_transportBuilder
                    ->setTemplateIdentifier('regimen_week_mail')
                    ->setTemplateOptions(
                [
                    'area' => 'frontend',
                    'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID,
                ]
                )
                ->setTemplateVars([
                    'email'                 => $data['email'],
                    'name'                  => $data['fname'],
                    'date'                  => $magentoTime,
                    'skin_concern_one'      => $skin_concerns['0'],
                    'skin_concern_two'      => $skin_concerns['1'],
                    'skin_concern_three'    => $skin_concerns['2'],
                    'dayproductname1'       => $savedataCollectionDay['0'],
                    'dayproductname2'       => $savedataCollectionDay['1'],
                    'dayproductname3'       => $savedataCollectionDay['2'],
                    'dayproductname4'       => $savedataCollectionDay['3'],
                    'nightproductname1'     => $savedataCollectionNight['0'],
                    'nightproductname2'     => $savedataCollectionNight['1'],
                    'nightproductname3'     => $savedataCollectionNight['2'],
                    'nightproductname4'     => $savedataCollectionNight['3'],
                ])
                ->setFrom($sender)
                ->addTo($sentToEmail,$sentToName)
                ->getTransport();
              }   
                $transport->sendMessage();
                 
                $this->_inlineTranslation->resume();
    
             
        }
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
       else{
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath($baseUrl.'regimen-recommender');
            return $resultRedirect;
       }
    }
}