require(
    ['jquery', 'jquery/ui'], 
    function($) {

        setTimeout(function() {
                $(".intro-container").addClass("deactivate");
                $('.regimen-form-container').addClass("activate");
        }, 1000);
        $('#next-qustn-click-event').click(function(){
              
            if($(this).parents('#recommender-container').find('.active #your-name').val() == ''){
                $(this).parents('#recommender-container').find('.active #your-name').focus();
                return;
            }

            else{
              var $current = $('.question-container.active');
                $('.question-container').removeClass('active');
                $current.next().addClass('active');
                $($current).addClass('answerd');
                $('input[type="text"]').each(function(){    
                    var id = $(this).attr('name');
                    var value = $(this).val();
                    localStorage.setItem(id, value);
                    //array[id]=value;
                    //alert(array[id]);
                         
                });
            }
            if($(this).parents('.question-container').siblings('.question-container.active').hasClass('name-display-container')){
                 $('#show-name-display').text(localStorage.getItem('fname'));
                //alert('ssss');
                setTimeout(function(){
                    $('.name-display-container').removeClass('active').addClass('answerd');
                    $('.name-display-container').next().addClass('active');
                },3000);
            }
            if($(this).parents('.question-container').siblings('.question-container').hasClass('more-intro')){
                setTimeout(function(){
                    $('.more-intro').removeClass('active').addClass('answerd');
                    $('.more-intro').next().addClass('active');
                    $('.quiz-back-button').addClass('active');
                },6000);
            }
        });
        $('#recommender-container').keypress(function(e){
            var key = e.which;
             if (key == 13)
            {
             
              return false;
            }
        });
        $('#your-name').keypress(function(e) {
            var key = e.which;
            if (key == 13)
            {
              $('#next-qustn-click-event').click();
              return false;
            }
        });
        $("input[attr$='onclick']").click(function() {
            var id = $(this).attr('name');
            var value = $(this).val();
             //var array = [id];
            //localStorage.setItem(id, value);
            //array[id]=value;
            $(this).parents('.radio-box').addClass('active');
            var $current = $('.question-container.active');
            $('.question-container').removeClass('active');
            $($current).addClass('answerd');
            $current.next().addClass('active'); 
            var member = $(this).val();
            $(".member-container").hide();
            $("#question-container" + member).show().addClass('active');
            if($(this).parents('.question-container').siblings('#fast-allies').hasClass('active')){
                setTimeout(function() {
                $('#fast-allies').removeClass('active');
                $('#fast-allies').next().addClass('active');
               }, 3000); 
            }
             if(value=="M"){
                $('#dont-show-for-male').removeClass('active');
                 $('#dont-show-for-male').next().addClass('active');
            }
            if(value =='not-member'){
                $('.login-form').removeClass('active');
                $('.login-form').next().addClass('active');
            }
            if(value == 'yes-member'){
                 setTimeout(function(){
                    $('.on-registration').addClass('active');
                },1000);
            }
            if($(this).parents('.question-container').siblings('.question-container.active').hasClass('not-member')){
                $('.quiz-back-button').removeClass('active');
                $('.quiz-back-button').hide();
                setTimeout(function(){
                    $('.not-member').removeClass('active');
                    $('.not-member').next().addClass('active');
                    $('.quiz-back-button').addClass('active');
                },3000);
                setTimeout(function(){
                    $('.quiz-back-button').show();
                },5000);
            }
            
        });
        $('.next-qustns').click(function(){
            var $current = $('.question-container.active');
            $('.question-container').removeClass('active');
            $current.next().addClass('active'); 
        });
        
        $('.LCheckbox').click(function () {
            $(this).parents('.radio-box').next().next().prop('disabled', !this.checked);
            $(this).parents('.radio-box').toggleClass('active');
            $('.LCheckbox').not(':checked').prop('disabled', $('.LCheckbox:checked').length == 3);
            if($('.LCheckbox:checked').length == 3){
                if($('.step-ques-12').hasClass('final-step')){
                   setTimeout(function() { 
                   $('.routine-container-already-signed').show();
               },2000);
                }
                 if($('.step-ques-12').hasClass('final-step-already-loggedin')){
                  setTimeout(function() { 
                       $('.routine-container-already-signed').show();
                   },2000);
                }
                var $current = $('.question-container.active');
            $('.question-container').removeClass('active');
            $current.next().addClass('active');
            }
        });
        if($('#recommender-container').length > 0){
        $(window).bind("pageshow", function() {
            var form = $('#recommender-container'); 
            form[0].reset();
        });
    }
        $('.time-click').click(function(){
            $(this).parents('.suggestion-product').toggleClass('active');
            $(this).parents('.suggestion-product').siblings('.suggestion-product').removeClass('active');
        });
        $('.regimen-product-name').first().addClass('active');
        $('.regimen-product-page-suggest').first().addClass('active');
        $('.regimen-product-name').click(function(e){
            e.preventDefault();
            $('.regimen-product-name').removeClass('active');
            $(this).addClass('active');
            var element = $(this).attr('href');
            $('.regimen-product-collection').find('.regimen-product-page-suggest').removeClass('active');
            $(element).addClass('active');
        });
        $('#submit-action').click(function(){
            $(this).parents('.regimen-form-container-on-registration').find('.popup-login-form').submit();
           
        });

        if($('.routine-container-already-signed').hasClass('active')){
            $('.quiz-back-button').removeClass('active');
            $('.quiz-back-button').hide();
        }

        
        $('.quiz-back-button').click(function(){
            var $current = $('.question-container.active');
            $('.question-container').removeClass('active');
            $current.prev().addClass('active').removeClass('answerd');
            $current.prev().find('.radio-box').removeClass('active');
            $current.prev().find(".custom-radio").prop("checked", false);
            if($('.not-member').hasClass('active')){
                    $('.not-member').removeClass('active');
                    $('.not-member').prev().addClass('active');
            }
            if($('.login-form').hasClass('active')){
                $('.login-form').removeClass('active');
                $('.login-form').prev().addClass('active');
                $('.step-ques-1-a').removeClass('answerd');
            }
            if($('.on-registration').hasClass('active')){
              $('.on-registration').removeClass('active');
            }

                if($('.register-container').hasClass('active')){
                    $('.register-container').removeClass('active');
                    $('.register-container').hide();
                    $('.step-ques-1-c-registration-choice').removeClass('answerd');
                    $('.step-ques-1-c-registration-choice').addClass('active');
                    $('.step-ques-1-a').find('.radio-box').removeClass('active');
                    $('.step-ques-1-a').find(".custom-radio").prop("checked", false);
                    $('.step-ques-1-c-registration-choice').find('.radio-box').removeClass('active');
                    $('.step-ques-1-c-registration-choice').find(".custom-radio").prop("checked", false);
                    
                }

                if($('.step-ques-1-a').hasClass('active')){
                    $('.step-ques-1-a').find('.radio-box').removeClass('active');
                    $('.step-ques-1-a').find(".custom-radio").prop("checked", false);
                }

            if($('.not-member').hasClass('active')){
                $('.quiz-back-button').removeClass('active');
                $('.quiz-back-button').hide();
            }    
           
            if($('.regimen-form-container').hasClass('active')){

                $('.regimen-form-container').removeClass('active');
                $('.regimen-form-container').prev().addClass('active');
            }
            if($('#dont-show-for-male').hasClass('active')){
                $('#dont-show-for-male').removeClass('active').prev().addClass('active');
                $('.step-ques-3').removeClass('answerd');
                $('.step-ques-3').find('.radio-box').removeClass('active');
            }

            if($('.step-ques-1').hasClass('active')){
                $('.quiz-back-button').removeClass('active');
            }

        });
        $('.step-ques-12').removeClass('final-step');

        $('.suggest-opt').click(function(){
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            $(this).find('.regimen-product-name').click();
        });
        $('.add-all-product-to-cart').click(function(){
            var skus = [];
            $('.regimen-product-page-suggest').each(function(){
                var sku = $(this).data('id');                
                if($.inArray(sku, skus) !== -1){
                    $(this).find('.product-add-form .custom_addto_form button.tocart').click();               
                }
                skus.push(sku);

            });
        });
        $('.regimen-product-page-suggest').first().addClass('active');
});