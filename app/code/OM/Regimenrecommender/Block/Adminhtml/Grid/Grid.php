<?php
namespace OM\Regimenrecommender\Block\Adminhtml\Grid;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $moduleManager;


    protected $_status;   
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \OM\Regimenrecommender\Model\Status $status,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {

        $this->_status = $status;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    } 

    protected function _construct()
    {
        parent::_construct();
        $this->setId('gridGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('grid_record');

    } 

    protected function _prepareCollection()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();       
        $collection = $objectManager->create('OM\Regimenrecommender\Model\SaveData');
        $collection = $collection->getCollection();
        $this->setCollection($collection);
        parent::_prepareCollection();

        return $this;
    }

    protected function _prepareColumns()
    {

         $this->addColumn(
            'id',
            [
                'header' => __('ID'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'fname',
            [
                'header' => __('fname'),
                'index' => 'fname',                
                'class' => 'xxx'
            ]
        );

        $this->addColumn(
            'member',
            [
                'header' => __('member'),
                'index' => 'member',                
                'class' => 'xxx'
            ]

        );


        $this->addColumn(
            'age',
            [
                'header' => __('age'),
                'index' => 'age'
                
            ]
        );


        $this->addColumn(
            'gender',
            [
                'header' => __('gender'),
                'index' => 'gender'
            ]
        );
        $this->addColumn(
            'smoke',
            [
                'header' => __('smoke'),
                'index' => 'smoke'
            ]
        );
        $this->addColumn(
            'Alcohol_consumption',
            [
                'header' => __('Alcohol_consumption'),
                'index' => 'Alcohol_consumption'
            ]
        );
        $this->addColumn(
            'hours_spent_on_computer',
            [
                'header' => __('hours_spent_on_computer'),
                'index' => 'hours_spent_on_computer'
            ]
        );
        $this->addColumn(
            'sleep_per_night',
            [
                'header' => __('sleep_per_night'),
                'index' => 'sleep_per_night'
            ]
        );
        $this->addColumn(
            'exercise_per_week',
            [
                'header' => __('exercise_per_week'),
                'index' => 'exercise_per_week'
            ]
        );
        $this->addColumn(
            'products_on_skin',
            [
                'header' => __('products_on_skin'),
                'index' => 'products_on_skin'
            ]
        );
        $this->addColumn(
            'skin-brands',
            [
                'header' => __('skin-brands'),
                'index' => 'skin-brands'
            ]
        );
        $this->addColumn(
            'skin-problem',
            [
                'header' => __('skin-problem'),
                'index' => 'skin-problem'
            ]
        );
        $this->addColumn(
            'email',
            [
                'header' => __('email'),
                'index' => 'email'
            ]
        );



        




        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }


    
}