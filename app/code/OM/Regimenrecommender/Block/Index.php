<?php
namespace OM\Regimenrecommender\Block;
use Magento\Framework\View\Element\Template;
use OM\Regimenrecommender\Model\Quiz;
use OM\Regimenrecommender\Model\SaveData;
/**
 * Main contact form block
 *
 * @api
 * @since 100.0.2
 */
class Index extends Template
{
    /**
     * @param Template\Context $context
     * @param array $data
     */
    /**
     * @var \Magento\Catalog\Model\ProductTypes\ConfigInterface
     */
    protected $productTypeConfig;
    protected $request;
     /**
    * @param \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig
    */
    public function __construct(Template\Context $context, array $data = [], Quiz $model, \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,\Magento\Framework\App\RequestInterface $request,SaveData $modelData)
    {
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
        $this->model = $model;
        $this->modelData = $modelData;
        $this->productTypeConfig = $productTypeConfig;
        $this->request = $request;
       
    }

    /**
     * Returns action url for contact form
     *
     * @return string
     */
    public function getFormAction()
    {
        return $this->getUrl('regimen/result/index', ['_secure' => true]);
    }
    public function getQuizCollection()
    {
        $quizCollection = $this->model->getCollection();
        return $quizCollection;
    }
     public function getAddToCartUrl($product)
    {
        $params = [
            'product' => $product->getId(),
            '_secure' => $this->getRequest()->isSecure()
        ];

        return $this->getUrl('checkout/cart/add', $params);
    }
    public function getPost()
    {
       return $this->request->getPostValue();
    }

    public function getProductData()
    {   
        $data =  $this->getPost();
        $skin_problems = implode('/',$data['skin-problem']);
        if($data['gender'] != 'M'){
           $skin_problems = $data['pregnant_or_breastfeeding'].'+'.$skin_problems;
        }
        else{
        $skin_problems = $skin_problems;
        }
        $gender = $data["gender"];

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        
        $query = "SELECT `daytime`,`nighttime`,`weekly` FROM regimen_quiz WHERE gender = '$gender' and skin_problem ='$skin_problems'";
        $result = $connection->fetchAll($query);
        return $result;
    }

    public function getModelData(){
        $_modelData = $this->modelData->create();
        $data = $_modelData->getCollection();
        foreach ($data as  $value) {
            var_dump($value);
        }
    }
}
