<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OM\Regimenrecommender\Model;
use Magento\Framework\Model\AbstractModel;

class SaveData extends \Magento\Framework\Model\AbstractModel {

    protected function _construct() {
        $this->_init('OM\Regimenrecommender\Model\ResourceModel\SaveData');
    }
}