<?php
namespace OM\Regimenrecommender\Model;
use Magento\Framework\Model\AbstractModel;
class Quiz extends AbstractModel{	
	protected function _construct()
	{
		$this->_init('OM\Regimenrecommender\Model\ResourceModel\Quiz');
	}
}