<?php
namespace OM\Regimenrecommender\Model\ResourceModel;
use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;
class Quiz extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('regimen_quiz', 'id');
    }
}