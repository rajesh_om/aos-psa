<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OM\Regimenrecommender\Model\ResourceModel\SaveData;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection{
    protected function _construct()
    {
        $this->_init('OM\Regimenrecommender\Model\SaveData', 'OM\Regimenrecommender\Model\ResourceModel\SaveData');
    }
}