<?php
 namespace OM\Regimenrecommender\Model\ResourceModel\Quiz;
 use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
	    'OM\Regimenrecommender\Model\Quiz',
	    'OM\Regimenrecommender\Model\ResourceModel\Quiz'
	);
    }
}