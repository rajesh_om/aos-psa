<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace OM\Regimenrecommender\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
class SaveData extends AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('regimen_data', 'id');
    }
}