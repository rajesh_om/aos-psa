<?php
 /**
 * OrangeMantra.
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    OrangeMantra
 * @package     OM_Alphabroder
 * @author      Shiv Kr Maurya (Senior Magento Developer)
 * @copyright   Copyright (c) 2018-19 OrangeMantra
 */
namespace OM\Restrictproduct\Model\Config\Source;
 
use Magento\Eav\Model\ResourceModel\Entity\Attribute\OptionFactory;
use Magento\Framework\DB\Ddl\Table;
 
/**
 * Custom Attribute Renderer
 *
 * @author     
 */
class Country extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    
 
    /**
     * Get all options
     *
     * @return array
     */
    public function getAllOptions()
    {
        
        $objectManager      = \Magento\Framework\App\ObjectManager::getInstance();
        $countrySourceModel = $objectManager->get('Magento\Directory\Model\Config\Source\Country');         
        
        $this->_options     = $countrySourceModel->toOptionArray();
        //$this->_options[0]  = ['label'=>'', 'value'=>'no','is_region_visible'=>1];


        return $this->_options;

    }
    
     /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string|bool
     */
    public function getOptionText($value)
    {   
       
        foreach ($this->getAllOptions() as $option) {
            if ($option['value'] == $value) {
                return $option['label'];
            }
        }
        return false;
    }
 

    
}