<?php

/**
 * OrangeMantra.
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    OrangeMantra
 * @package     OM_Restrictproduct
 * @author      Shiv Kr Maurya (Senior Magento Developer)
 * @copyright   Copyright (c) 2018-19 OrangeMantra
 */
namespace OM\Restrictproduct\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
	
private $eavSetupFactory;

public function __construct(EavSetupFactory $eavSetupFactory)
{
	$this->eavSetupFactory = $eavSetupFactory;
}

public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
{

	
		// TO CREATE PRODUCT ATTRIBUTE
		$eavSetup = $this->eavSetupFactory->create(['setup'=>$setup]);
		$eavSetup->addAttribute(
						\Magento\Catalog\Model\Product::ENTITY,

						'restrict_for_country',
						[

						'group' => 'General',
						'type' => 'text',
						'source' => 'OM\Restrictproduct\Model\Config\Source\Country',
						'frontend' => '',
						'backend' => 'Magento\Eav\Model\Entity\Attribute\Backend\ArrayBackend',
						'label' => 'Restrict For Country',
						'input' => 'multiselect',
						'class' => '',						
						'global' => \Magento\Catalog\Model\ResourceModel\Eav\Attribute::SCOPE_GLOBAL,
						'visible' => true,
						'required' => false,
						'user_defined' => false,
						'default' => '',
						'searchable' => false,
						'filterable' => false,
						'comparable' => false,
						'visible_on_front' => false,
						'used_in_product_listing' => true,
						'unique' => false
						]

		);		       

	}

}
