<?php
/*
Country Restriction Module
*/
namespace OM\Restrictproduct\Observer\Product;

use Magento\Framework\Event\ObserverInterface;

class Restrict implements ObserverInterface
{
   protected $messageManager;
   protected $_responseFactory;
   protected $_url;

    public function __construct(\Magento\Framework\Message\ManagerInterface $messageManager,
    \Magento\Framework\App\ResponseFactory $responseFactory,
    \Magento\Framework\UrlInterface $url)
   {
       $this->messageManager   = $messageManager;
       $this->_responseFactory = $responseFactory;
       $this->_url             = $url;
   }

   public function execute(\Magento\Framework\Event\Observer $observer)
   {
        
        /* ----------Fetching the cart items details ------ */
       $objectManager     = \Magento\Framework\App\ObjectManager::getInstance();
       $cart              = $objectManager->get('\Magento\Checkout\Model\Cart');
       $shippingAddress   = $cart->getQuote()->getShippingAddress(); 
       $shippingCountry   = $cart->getQuote()->getShippingAddress()->getCountryId();
       $countryName       = $objectManager->create('\Magento\Directory\Model\Country')->load($shippingCountry)->getName();
       $items             = $cart->getQuote()->getAllItems();
       $restrict          = false;
       $productName       = null;

       foreach($items as $item) {
          $product          = $objectManager->create('Magento\Catalog\Model\Product')->load($item->getProductId());
          $restricedProduct = explode(',',$product->getData('restrict_for_country'));

          if(in_array($shippingCountry,$restricedProduct)){
            $restrict     = true;
            $productName  = $product->getName();
            break;
          }

       }

        if($restrict){
          $message = 'The "'. $productName.'" is not available for choosen country "'.$countryName.'" to ship.';
          $this->messageManager->addError($message); 
          $cartUrl = $this->_url->getUrl('checkout/cart/index');
          $this->_responseFactory->create()->setRedirect($cartUrl)->sendResponse();            
         exit;
    } 

   
  }
      

}