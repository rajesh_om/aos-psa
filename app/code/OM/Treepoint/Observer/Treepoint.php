<?php
 
namespace OM\Treepoint\Observer;
 
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\StoreManagerInterface;
 
class Treepoint implements ObserverInterface
{
    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $orderModel;
 
    /**
     * @var \Magento\Sales\Model\Order\Email\Sender\OrderSender
     */
    protected $orderSender;
 
    /**
     * @var \Magento\Checkout\Model\Session $checkoutSession
     */
    protected $checkoutSession;

    const XML_PATH_EMAIL_RECIPIENT_EMAIL = 'trans_email/ident_support/email';
    protected $_inlineTranslation;
    protected $_transportBuilder;
    protected $_scopeConfig;
    protected $_logLoggerInterface;
    protected $storeManager;
 
    /**
     * @param \Magento\Sales\Model\OrderFactory $orderModel
     * @param \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender
     * @param \Magento\Checkout\Model\Session $checkoutSession
     *
     * @codeCoverageIgnore
     */
    public function __construct(
        \Magento\Sales\Model\OrderFactory $orderModel,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Checkout\Model\Session $checkoutSession,
         \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $loggerInterface,
        StoreManagerInterface $storeManager

    )
    {
        $this->orderModel = $orderModel;
        $this->orderSender = $orderSender;
        $this->checkoutSession = $checkoutSession;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_transportBuilder = $transportBuilder;
        $this->_scopeConfig = $scopeConfig;
        $this->_logLoggerInterface = $loggerInterface;
        $this->storeManager = $storeManager;
    }
 
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $orderIds = $observer->getEvent()->getOrderIds();
        if(count($orderIds))
        {
            $order = $this->orderModel->create()->load($orderIds[0]);
            $orderTotal = $order->getGrandTotal();
            $currency = $order->getOrderCurrencyCode();
            $loginUrl = $this->_scopeConfig->getValue('treepoint/general/login_endpoint');
            $user = $this->_scopeConfig->getValue('treepoint/general/user');
            $password = $this->_scopeConfig->getValue('treepoint/general/password');
            $offsetUrl = $this->_scopeConfig->getValue('treepoint/general/offset_endpoint');

            $url = $loginUrl;
            $post = [
                  "email" => $user,
                  "password"=> $password
            ];
            $data_json = json_encode($post);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $response = curl_exec($ch);
            curl_close($ch);
            $cResponse = json_decode($response,true);
            if($cResponse['status'] == 'success'){
                  $authKey = $cResponse['auth_token'];
                  $url = $offsetUrl;

                  /*request for offset*/
                  $post = [
                        "amount" => 20,
                        "product"=> "CARBON"
                  ];
                  $data_json = json_encode($post);
                  $ch = curl_init($url);
                  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: Token '.$authKey));
                  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                  $response = curl_exec($ch);
                  curl_close($ch);

                  /*request for planting a tree*/
                   $post = [
                        "amount" => 1,
                        "product"=> "TREE"
                  ];
                  $data_json = json_encode($post);
                  $ch = curl_init($url);
                  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Authorization: Token '.$authKey));
                  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                  curl_setopt($ch, CURLOPT_POST, 1);
                  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
                  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                  $response = curl_exec($ch);
                  curl_close($ch);
              }
      }
  }
}