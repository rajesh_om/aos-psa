<?php
namespace OM\ImportOrder\Block\Adminhtml\Order\View;
class Ordercustomattribute extends \Magento\Backend\Block\Template
{
	protected $order;
	
	public function __construct(
	\Magento\Framework\Registry $registry,
	\Magento\Backend\Block\Template\Context $context,
	$data=[]
	){
		$this->_coreRegistry = $registry;
		parent::__construct($context, $data);
	}
	/**
	 * Retrieve order model instance
	 *
	 * @return \Magento\Sales\Model\Order
	 */
	public function getOrder()
	{
	    return $this->_coreRegistry->registry('current_order');
	}
	/**
	 * Retrieve order model instance
	 *
	 * @return \Magento\Sales\Model\Order
	 */
	public function getOrderId()
	{
	    return $this->getOrder()->getEntityId();
	}

	/**
	 * Retrieve order increment id
	 *
	 * @return string
	 */
	public function getOrderIncrementId()
	{
	    return $this->getOrder()->getIncrementId();
	}

}