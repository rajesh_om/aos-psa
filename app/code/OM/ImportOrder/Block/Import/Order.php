<?php


namespace OM\ImportOrder\Block\Import;
use OM\ImportOrder\Helper\Data;

class Order extends \Magento\Framework\View\Element\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context, 
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
         \Magento\Framework\File\Csv $csvProcessor,
        Data $helper,
        array $data = []
    ) {
        $this->_helper = $helper;
        $this->directoryList = $directoryList;
        $this->csvProcessor = $csvProcessor;
        parent::__construct($context, $data);
    }

    public function changeDate()
    {
        $this->_helper->changeDate();
    }


    public function prepareDataCsv4_1()
    {   
        $fileName = "us_demo_v7.csv";
        $fileDirectoryPath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::ROOT)."/".$fileName;
        $csvData = $this->csvProcessor->getData($fileDirectoryPath);

         foreach ($csvData as $row => $data)
         {      
            $csvdata['order_id']              =$data[0] ;                                                 
            $csvdata['email']                 =$data[1] ;                                                 
            $csvdata['financial_status']      =$data[2] ;                                                 
            $csvdata['paid_at']               =$data[3] ;                                                
            $csvdata['fullfilment_status']    =$data[4] ;                                                
            $csvdata['fulfilled_at']          =$data[5] ;                                                
            $csvdata['accepts_marketing']     =$data[6] ;                                             
            $csvdata['subtotal']              =$data[7] ;                            
            $csvdata['shipping']              =$data[8] ;                                 
            $csvdata['taxes']                 =$data[9] ;                        
            $csvdata['total']                 =$data[10];                        
            $csvdata['discount_code']         =$data[11];                     
            $csvdata['discount_amount']       =$data[12];                     
            $csvdata['shipping_method']       =$data[13];                             
            $csvdata['created_at']            =$data[14];                               
            $csvdata['billing_firstname']     =$data[15];                               
            $csvdata['billing_lastname']      =$data[16];                          
            $csvdata['billing_street']        =$data[17]." ".$data[18];                                 
            $csvdata['billing_city']          =$data[19];                              
            $csvdata['billing_zip']           =$data[20];                               
            $csvdata['billing_province']      =$data[21];                            
            $csvdata['billing_country']       =$data[22];                           
            $csvdata['billing_phone']         =$data[23];                                
            $csvdata['shipping_firstname']    =$data[24];                               
            $csvdata['shipping_lastname']     =$data[25];                             
            $csvdata['shipping_street']       =$data[26]." ".$data[27];                                  
            $csvdata['shipping_city']         =$data[28];                               
            $csvdata['shipping_zip']          =$data[29];                                
            $csvdata['shipping_province']     =$data[30];                             
            $csvdata['shipping_country']      =$data[31];                            
            $csvdata['shipping_phone']        =$data[32];                                 
            $csvdata['notes']                 =$data[33];                                
            $csvdata['payment_method']        =$data[34];                              
            $csvdata['payment_reference']     =$data[35];                     
            $csvdata['refunded_amount']       =$data[36];                              
            // $csvdata['outstanding_balance']   =$data[37];                                 
            // $csvdata['id']                    =$data[38];                               
            $csvdata['tags']                  =$data[39];                                   
            $csvdata['risk_level']            =$data[40];                  
            $csvdata['source']                =$data[41];                    
            $csvdata['tax_1_name']            =$data[42];                          
            $csvdata['tax_1_value']           =$data[43];                      
            $csvdata['tax_2_name']            =$data[44];                          
            $csvdata['tax_2_value']           =$data[45];                           
            $csvdata['tax_3_name']            =$data[46];                          
            $csvdata['tax_3_value']           =$data[47];                           
            $csvdata['phone']                 =$data[48];                          
            $csvdata['product_data']          =$data[49];
            // $csvdata['cancelled_at']          =$data[49];
         
            $csvProductInformation = json_decode($csvdata['product_data'],TRUE);
            
            if(empty($csvdata['billing_phone']))
            {
                $csvdata['billing_phone'] = $csvdata['shipping_phone'];
            }    

            $orderData=[
                'old_order_id' => $csvdata['order_id'],
                'email' => $csvdata['email'],
                'shipping_address' => [
                    'firstname' => $csvdata['shipping_firstname'],
                    'lastname' => $csvdata['shipping_lastname'],
                    'street' => $csvdata['shipping_street'],
                    'city' => $csvdata['shipping_city'],
                    'country_id' => $csvdata['shipping_country'],
                    'region' => $csvdata['shipping_province'],
                    'postcode' => $csvdata['shipping_zip'],
                    'telephone' => $csvdata['shipping_phone']
                ],
                'billing_address' => [
                    'firstname' => $csvdata['billing_firstname'],
                    'lastname' => $csvdata['billing_lastname'],
                    'street' => $csvdata['billing_street'],
                    'city' => $csvdata['billing_city'],
                    'country_id' => $csvdata['billing_country'],
                    'region' => $csvdata['billing_province'],
                    'postcode' => $csvdata['billing_zip'],
                    'telephone' => $csvdata['billing_phone'] 
                ],
                
                'subtotal' => $csvdata['subtotal'],
                'financial_status' => $csvdata['financial_status'],
                'fullfilment_status' => $csvdata['fullfilment_status'],
                'accepts_marketing' => $csvdata['accepts_marketing'],
                'created_at' => $csvdata['created_at'],
                'shipping' => $csvdata['shipping'],
                'shipping_method' => $csvdata['shipping_method'],
                'taxes' => $csvdata['taxes'],
                'total' => $csvdata['total'],
                'discount_amount' => $csvdata['discount_amount'],
                'discount_code' => $csvdata['discount_code'],
                'items'=> $csvProductInformation,
                
                'tags'=> $csvdata['tags'],
                'risk_level'=> $csvdata['risk_level'],
                'source'=> $csvdata['source'],
                'payment_method'=> $csvdata['payment_method'],
                'payment_reference'=> $csvdata['payment_reference'],
                'paid_at'=> $csvdata['paid_at'],
                'fulfilled_at'=> $csvdata['fulfilled_at'],
                'notes'=> $csvdata['notes'],
                'tax_1_name'=> $csvdata['tax_1_name'],
                'tax_1_value'=> $csvdata['tax_1_value'],
                'tax_2_name'=> $csvdata['tax_2_name'],
                'tax_2_value'=> $csvdata['tax_2_value'],
                'tax_3_name'=> $csvdata['tax_3_name'],
                'tax_3_value'=> $csvdata['tax_3_value'],
                'phone'=> $csvdata['phone'],
                'refunded_amount'=> $csvdata['refunded_amount'],
            ];

            $this->_helper->createOrder_v1($orderData);
         }
    }

    public function changeOrderData()
    {
        // $this->_helper->changeOrderData(19);
        $this->_installData->install12();
    }
}
