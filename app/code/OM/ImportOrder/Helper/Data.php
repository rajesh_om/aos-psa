<?php 
 
namespace OM\ImportOrder\Helper; 
  
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
 
/**
* @param Magento\Framework\App\Helper\Context $context
* @param Magento\Store\Model\StoreManagerInterface $storeManager
* @param Magento\Catalog\Model\Product $product
* @param Magento\Framework\Data\Form\FormKey $formKey $formkey,
* @param Magento\Quote\Model\Quote $quote,
* @param Magento\Customer\Model\CustomerFactory $customerFactory,
* @param Magento\Sales\Model\Service\OrderService $orderService,
*/
 
public function __construct(
    \Magento\Framework\App\Helper\Context $context,
    \Magento\Store\Model\StoreManagerInterface $storeManager,
    \Magento\Catalog\Model\ProductFactory $productFactory,
    \Magento\Quote\Model\QuoteManagement $quoteManagement,
    \Magento\Customer\Model\CustomerFactory $customerFactory,
    \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
    \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
    \Magento\Sales\Model\Service\OrderService $orderService,
    \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
    \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
    \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
    \Magento\Directory\Model\CurrencyFactory $currencyFactory,
    \Magento\Quote\Model\Quote\Address\Rate $shippingRate
) {
    $this->_storeManager = $storeManager;
    $this->_productFactory = $productFactory;
    $this->quoteManagement = $quoteManagement;
    $this->customerFactory = $customerFactory;
    $this->customerRepository = $customerRepository;
    $this->orderRepository = $orderRepository;
    $this->productRepository = $productRepository;
    $this->currencyFactory = $currencyFactory;
    $this->orderService = $orderService;
    $this->cartRepositoryInterface = $cartRepositoryInterface;
    $this->cartManagementInterface = $cartManagementInterface;
    $this->shippingRate = $shippingRate;
}

public $currencyCode = 'SGD';
public function createOrder_v1($orderData) {
    //init the store id and website id @todo pass from array
  
    $store_id=1; //1 for singapor 3 for us
    $store = $this->_storeManager->getStore($store_id);
    $websiteId = $store->getWebsiteId();
    $this->_storeManager->setCurrentStore($store_id);
    
    $currency =  $this->currencyFactory->create()->load($this->currencyCode);
    $this->_storeManager->getStore($store_id)->setCurrentCurrency($currency);

    //init the customer
    $customer=$this->customerFactory->create();
    $customer->setWebsiteId($websiteId);
    $customer->loadByEmail($orderData['email']);// load customet by email address

    //check the customer
    if(!$customer->getEntityId()){
        //If not avilable then create this customer
        $customer->setWebsiteId($websiteId)
            ->setStore($store)
            ->setFirstname($orderData['shipping_address']['firstname'])
            ->setLastname($orderData['shipping_address']['lastname'])
            ->setEmail($orderData['email'])
            ->setPassword($orderData['email']);
        $customer->save(); 
    }
    //init the quote
    $cart_id = $this->cartManagementInterface->createEmptyCart();
    $cart = $this->cartRepositoryInterface->get($cart_id);
    $cart->setStore($store);
    // if you have already buyer id then you can load customer directly
    $customer= $this->customerRepository->getById($customer->getEntityId());
    $cart->setCurrency();
    $cart->assignCustomer($customer); //Assign quote to customer
    //add items in quote
    foreach($orderData['items'] as $key => $item){
        $product = $this->getProductObj($item);
        if(empty($product))
        {
            die("product not created !!!!");
        }

        $product->setPrice($item['price']);
        // $product->setDiscountAmount('10');
        $cart->addProduct(
            $product,
            intval($item['quantity'])
        );
        
    }
    //Set Address to quote @todo add section in order data for seperate billing and handle it
    $cart->getBillingAddress()->addData($orderData['billing_address']);
    $cart->getShippingAddress()->addData($orderData['shipping_address']);
    

    // Collect Rates and Set Shipping & Payment Method
    $this->shippingRate
        ->setCode('flatrate_flatrate')
        ->setPrice(floatval($orderData['shipping']));
    // echo "<pre>";
    // var_dump(get_class($cart->getShippingAddress()));
    // die("die here");
    $shippingAddress = $cart->getShippingAddress();
    //@todo set in order data
    $shippingAddress->setCollectShippingRates(true)
        ->collectShippingRates()
        ->setShippingMethod('flatrate_flatrate') //shipping method
        ->setShippingDescription('Description of order shipping');
    $cart->getShippingAddress()->addShippingRate($this->shippingRate);
    // $cart->setPaymentMethod('udhar'); //payment method
    $cart->setInventoryProcessed(false);
    // Set sales order payment
    $cart->getPayment()->importData(['method' => 'checkmo']);
    // Collect total and saeve
    $cart->collectTotals();
    $cart->setReservedOrderId($orderData['old_order_id']);
    // Submit the quote and create the order
    $cart->save();
    $cart = $this->cartRepositoryInterface->get($cart->getId());
    $order_id = $this->cartManagementInterface->placeOrder($cart->getId());
    $this->changeOrderData($order_id,$orderData);
    }



    public function changeOrderData($order_id,$orderData)
    {
        $order = $this->getOrder($order_id);
        $date=$orderData['created_at'].' +0800';
        $order->setCreatedAt($date); 
        // $order->setTaxAmount($orderData['taxes']);
        // $order->setBaseTaxAmount($orderData['taxes']);
        
        if($orderData['discount_amount'])
        {
            $order->setDiscountAmount(-$orderData['discount_amount']);
            $order->setBaseDiscountAmount(-$orderData['discount_amount']);

            $discount_percentage = ($orderData['discount_amount']/$order->getBaseSubtotal())*100;

            $items = $order->getAllVisibleItems();
            $item_ordered = count($items);
            foreach($items as $i):
                $tot_amount = $i->getQtyOrdered(10) * $i->getPrice(10);
                $discount_on_individual_product = ($discount_percentage*$tot_amount)/100;
                $i->setDiscountAmount($discount_on_individual_product);
                $i->setBaseDiscountAmount($discount_on_individual_product);
            endforeach;
            $order->setDiscountDescription($orderData['discount_code']);
        }

        if($orderData['taxes'])
        {
            $order->setTaxAmount($orderData['taxes']);
            $order->setBaseTaxAmount($orderData['taxes']);
            $subtotal_excluding_tax = (floatval($order->getSubtotal())-floatval($orderData['taxes']));

            $tax_percentage = ($orderData['taxes']/$order->getBaseSubtotal())*100;

            $items = $order->getAllVisibleItems();
            $item_ordered = count($items);
            foreach($items as $i):
                $tot_amount = $i->getQtyOrdered(10) * $i->getPrice(10);
                $tax_on_individual_product = ($tax_percentage*$tot_amount)/100;
                $i->setTaxAmount($tax_on_individual_product);
                $i->setBaseTaxAmount($tax_on_individual_product);
                $i->setTaxPercent($tax_percentage);
                $row_subtotal_excluding_tax = ($i->getRowTotal()*(100-$tax_percentage))/100;
                $i->setRowTotal($row_subtotal_excluding_tax);
                $i->setPrice($i->getPrice()*(100-$tax_percentage)/100);
            endforeach;
            $order->setData('subtotal',$subtotal_excluding_tax); 
        }

        $order->setGrandTotal(floatval($orderData['total'])); //Grand Total purchased
        $order->setBaseGrandTotal(floatval($orderData['total'])); 
        $order->setShippingDescription($orderData['shipping_method']); 
        $order->setBaseShippingAmount($orderData['shipping']); 
        $order->setShippingAmount($orderData['shipping']); 
        
        $order->setBaseCurrencyCode($this->currencyCode);
        // $order->setQuoteCurrencyCode('SGD');

        $order->setState('new');
        $order->setStatus('pending');
        $history = $order->addStatusHistoryComment($orderData['notes'], false);
        $history->setIsCustomerNotified(false);
        $order->setEmailSent(0);
        //additional info
        $order->setData('financial_status',$orderData['financial_status']); 
        $order->setData('fullfilment_status',$orderData['fullfilment_status']); 
        $order->setData('accepts_marketing',$orderData['accepts_marketing']); 
        $order->setData('paid_at',$orderData['paid_at']); 
        $order->setData('fulfilled_at',$orderData['fulfilled_at']); 
        $order->setData('old_payment_method',$orderData['payment_method']); 
        $order->setData('old_payment_reference',$orderData['payment_reference']); 
        $order->setData('tags',$orderData['tags']); 
        $order->setData('risk_level',$orderData['risk_level']); 
        $order->setData('source',$orderData['source']); 
        
        $order->setData('tax_1_name',$orderData['tax_1_name']); 
        $order->setData('tax_1_value',$orderData['tax_1_value']); 
        $order->setData('tax_2_name',$orderData['tax_2_name']); 
        $order->setData('tax_2_value',$orderData['tax_2_value']); 
        $order->setData('tax_3_name',$orderData['tax_3_name']); 
        $order->setData('tax_3_value',$orderData['tax_3_value']); 
        $order->setData('refunded_amount',$orderData['refunded_amount']); 
        $order->setData('old_phone',$orderData['phone']); 
        $order->save();
        echo $orderData['old_order_id']." order created $order_id."."\n";
        // echo $order_id." order created"."\n";

    }

    public function getOrder($order_id)
    {
        return $this->orderRepository->get($order_id);
    }

    public function getProductObj($item)
    {   
        try {
            $product = $this->productRepository->get($item['sku']);
            return $product;
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $product =  $this->_productFactory->create(); 
            $product->setSku($item['sku']); // Set your sku here
            $product->setName($item['name']); // Name of Product
            $product->setAttributeSetId(4); // Attribute set id
            $product->setStatus(1); // Status on product enabled/ disabled 1/0
            $product->setWeight(0); // weight of product
            $product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
            $product->setTaxClassId(2); // Tax class id
            $product->setTypeId('simple'); // type of product (simple/virtual/downloadable/configurable)
            $product->setPrice($item['price']); // price of product
            $product->setStockData(
                array(
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 1,
                    'is_in_stock' => 1,
                    'qty' => 99999
                )
            );
            $product->save();
            if($product->getId())
            {
                return $product;
            } else 
            {
                die('product cannot be created');
            } 
            die('die in catch section');       
        }
    }

    


}