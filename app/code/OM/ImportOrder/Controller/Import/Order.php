<?php 


namespace OM\ImportOrder\Controller\Import;
use OM\ImportOrder\Block\Import\Order AS OrderBlock;

class Order extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        OrderBlock $orderblock
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->orderblock = $orderblock;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {   
        $this->orderblock->prepareDataCsv4_1();
        // $this->orderblock->changeDate();
    }
}
