<?php
namespace OM\ImportOrder\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use OM\ImportOrder\Block\Import\Order AS OrderBlock;

class Import extends Command
{

  private $appState;

  public function __construct( 
    \Magento\Framework\App\State $appState
  ) {
      parent::__construct();
      $this->appState = $appState;
  }


   protected function configure()
   {
       $this->setName('om:importorder');
       $this->setDescription('need to import order from here');
       parent::configure();
   }
   protected function execute(InputInterface $input, OutputInterface $output)
   {  
      $this->appState->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $updateImages = $objectManager->create('OM\ImportOrder\Block\Import\Order')->prepareDataCsv4_1();
   }
}