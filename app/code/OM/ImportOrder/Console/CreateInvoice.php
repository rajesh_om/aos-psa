<?php
namespace OM\ImportOrder\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateInvoice extends Command
{

  private $appState;

  public function __construct( 
    \Magento\Framework\App\State $appState,
    \Magento\Sales\Api\OrderRepositoryInterface\Proxy $orderRepository,
    \Magento\Sales\Model\Service\InvoiceService\Proxy $invoiceService,
    \Magento\Framework\DB\Transaction\Proxy $transaction
  ) {
      parent::__construct();
      $this->state = $appState;
      $this->_orderRepository = $orderRepository;
      $this->_invoiceService = $invoiceService;
      $this->_transaction = $transaction;
  }
  protected function configure()
  {
    $this->setName('om:createinvoice');
    $this->setDescription('Create invoice for imported order');
    parent::configure();
  }
   protected function execute(InputInterface $input, OutputInterface $output)
   {  
      $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
      
      $orderids = [];

      foreach ($orderids as $orderId) {
        $order = $this->_orderRepository->get($orderId);
        $paid_at = $order->getData('paid_at').' +0800';
        if($order->canInvoice()) {
            try {
              $invoice = $this->_invoiceService->prepareInvoice($order);
              $invoice->setCreatedAt($paid_at);
              $invoice->register();
              $transactionSave = $this->_transaction->addObject(
                  $invoice
              )->addObject(
                  $invoice->getOrder()
              );
              $transactionSave->save();
              $order->setIsCustomerNotified(false)
              ->save();
              echo "invoice created for order : $orderId \n";
            } catch (Exception $e) {
              echo "Exception :$e->getMessage()";
            }
        }
        else {
              echo "invoice not created for order : $orderId \n";
        }
      }
   }
}