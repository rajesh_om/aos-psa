<?php
namespace OM\ImportOrder\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateShipment extends Command
{

  private $appState;

  public function __construct( 
    \Magento\Framework\App\State $appState,
    \Magento\Sales\Api\OrderRepositoryInterface\Proxy $orderRepository,
    \Magento\Sales\Model\Service\InvoiceService\Proxy $invoiceService,
    \Magento\Sales\Model\Convert\Order\Proxy $convertOrder,
    \Magento\Framework\DB\Transaction\Proxy $transaction
  ) {
      parent::__construct();
      $this->state = $appState;
      $this->_orderRepository = $orderRepository;
      $this->_invoiceService = $invoiceService;
      $this->_convertOrder = $convertOrder;
      $this->_transaction = $transaction;
  }
  protected function configure()
  {
    $this->setName('om:createshipment');
    $this->setDescription('Create shipment for imported order');
    parent::configure();
  }
   protected function execute(InputInterface $input, OutputInterface $output)
   {  
      $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_GLOBAL);
      $orderids = [5822,5831,5832,5833,5834,5835,5836,5837,5839,5840,5841,5842,5843,5844,5845,5846,5847,5848,5849,5850,5851,5852,5853,5855,5856,5857,5858,5859,5860,5861,5862,5863,5864,5865,5866,5867,5868,5869,5870,5871,5873,5874,5875,5876,5877,5878,5879,5880,5882,5883,5884,5885,5886,5887,5888,5889,5890,5891,5892,5893,5894,5895,5896,5897,5898,5899,5900,5901,5902,5903,5904,5905,5906,5908,5909,5910,5911,5912,5913,5914,5915,5916,5917,5918,5919,5920,5921,5922,5923,5924,5925,5926,5927,5928,5929,5930,5931,5932,5933,5934,5935,5936,5937,5938,5939,5940,5941,5942,5943,5944,5945,5946,5947,5948,5949,5950,5951,5952,5953,5954,5956,5957,5958,5959,5960,5961,5962,5963,5964,5965,5966,5967,5969,5970,5971,5972,5973,5974,5975,5976,5977,5978,5979,5980,5981,5982,5983,5984,5985,5986,5987,5988,5989,5990,5991,5992,5993,5994,5995,5996,5997,5998,5999,6000,6001,6002,6005,6006,6008,6009,6010,6011,6012,6013,6014,6015,6016,6017,6018,6019,6020,6021,6022,6023,6024,6025,6026,6027,6028,6029,6030,6031,6032,6033,6034,6035,6036,6037,6038,6039,6040,6041,6042,6043,6044,6045,6046,6047,6048,6049,6050,6051,6052,6053,6054,6055,6056,6057,6058,6059,6060,6061,6062,6063,6064,6065,6066,6067,6068,6069,6070,6071,6072,6073,6074,6075,6076,6077,6078];
      foreach ($orderids as $orderId) 
      {
        $order = $this->_orderRepository->get($orderId);
        $fulfilled_at = $order->getData('fulfilled_at').' +0800';
        if ($order->canShip()) {
          $orderShipment = $this->_convertOrder->toShipment($order);
          foreach ($order->getAllItems() AS $orderItem) {
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
               continue;
            }
            $qty = $orderItem->getQtyToShip();
            $shipmentItem = $this->_convertOrder->itemToShipmentItem($orderItem)->setQty($qty);
            $orderShipment->addItem($shipmentItem);
          }
          $orderShipment->setCreatedAt($fulfilled_at);
          $orderShipment->register();
          $orderShipment->getOrder()->setIsInProcess(true);
          try {
            $orderShipment->save();
            $orderShipment->getOrder()->save();
            echo "shipment created for order : $orderId \n";
          } catch (\Exception $e) {
              echo "Exception :$e->getMessage()";
          }
        }
        else {
              echo "shipment not created for order : $orderId \n";
        }
      }
   }
}