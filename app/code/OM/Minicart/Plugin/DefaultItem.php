<?php

namespace OM\Minicart\Plugin;

use Magento\Quote\Model\Quote\Item;

class DefaultItem
{
    public function aroundGetItemData($subject, \Closure $proceed, Item $item)
    {
        $data = $proceed($item);
        $product = $item->getProduct();

        $atts = [
            "product_size" => $product->getAttributeText('product_size'),
            "pre_order_text" => $product->getAttributeText('pre_order_text')
        ];

        return array_merge($data, $atts);
    }
}