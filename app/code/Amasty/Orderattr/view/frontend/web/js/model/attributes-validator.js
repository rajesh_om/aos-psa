define([
    'underscore',
    'uiRegistry',
    'mage/storage',
    'Magento_Checkout/js/model/quote',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/full-screen-loader',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/url-builder'
], function (_, registry, storage, quote, customer, fullScreenLoader, errorProcessor, urlBuilder) {
    'use strict';
    function prepareResultForApi(result, checkoutFormCode) {
        var apiResult = {
            'amastyCartId' : quote.getQuoteId(),
            'checkoutFormCode' : checkoutFormCode,
            'shippingMethod' : '',
            'entityData': {
                'custom_attributes': []
            }
        };

        if (!quote.isVirtual()) {
            var rate = quote.shippingMethod();

            if (rate.carrier_code && rate.method_code) {
                apiResult.shippingMethod = rate.carrier_code + '_' + rate.method_code;
            }
        }

        _.each(result, function(value, code) {
            if (_.isArray(value)) {
                value = value.join(',');
            }
            apiResult.entityData.custom_attributes.push(
                {
                    'attribute_code' : code,
                    'value' : value
                }
            );
        });

        return apiResult;
    }
    return {
        /**
         * Validate checkout agreements
         *
         * @returns {boolean}
         */
        validate: function() {
            window.orderAttributesPreSend = false;

            var amastyCheckoutProvider = registry.get('amastyCheckoutProvider'),
                focused = false,
                result = {},
                attributesTypes = [
                    'amastyShippingAttributes',
                    'amastyPaymentAttributes',
                    'amastySummaryAttributes',
                    'amastyShippingMethodAttributes',
                    'before-place-order.amastyPaymentMethodAttributes'
                ],
                checkoutFormCode = 'amasty_checkout';

            if (quote.isVirtual()) {
                attributesTypes = [
                    'amastyPaymentAttributes',
                    'before-place-order.amastyPaymentMethodAttributes',
                    'amastySummaryAttributes'
                ];
                checkoutFormCode = 'amasty_checkout_virtual';
            }

            for (var key in attributesTypes) {
                if (attributesTypes.hasOwnProperty(key)) {
                    result = _.extend(result, amastyCheckoutProvider.get(attributesTypes[key]));
                    amastyCheckoutProvider.set('params.invalid', false);

                    var customScope = attributesTypes[key];
                    if (customScope.indexOf('.') !== -1) {
                        customScope = customScope.substr(customScope.indexOf('.') + 1);
                    }
                    amastyCheckoutProvider.trigger(customScope + '.data.validate');

                    if (amastyCheckoutProvider.get('params.invalid') && !focused) {
                        var container = registry.filter("index = " + attributesTypes[key] + 'Container');
                        if (container.length) {
                            container[0].focusInvalidField();
                        }
                        focused = true;
                        amastyCheckoutProvider.set('params.invalid', false);
                    }
                }
            }

            if (focused) {
                amastyCheckoutProvider.set('params.invalid', true);
            }

            if (amastyCheckoutProvider.get('params.invalid')) {
                return false;
            }
            window.orderAttributesPreSend = true;
            return true;
        }
    }
});
