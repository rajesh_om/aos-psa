define([
    'underscore',
    'mageUtils',
    'Amasty_Orderattr/js/form/element/select'
], function (_, utils, Select) {
    'use strict';

    return Select.extend({
        isFieldInvalid: function () {
            return this.error() && this.error().length ? this : null;
        },

        /** Fix default first option selection **/
        normalizeData: function (value) {
            if (utils.isEmpty(value)) {
                return '';
            }
            var option = this.getOption(value);

            return option && option.value;
        },

        /**
         * Clears 'value' property.
         *
         * @returns {Abstract} Chainable.
         */
        clear: function () {
            this.value('');

            return this;
        }
    });
});
