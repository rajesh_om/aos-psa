<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_PromoBanners
 */


namespace Amasty\PromoBanners\Controller\Adminhtml\Banners;

use Magento\Backend\App\Action;
use Psr\Log\LoggerInterface;

class MassActivate extends \Magento\Backend\App\Action
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        Action\Context $context,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->logger = $logger;
    }

    public function execute()
    {
        $ids = $this->getRequest()->getParam('ids');
        $status = $this->getRequest()->getParam('activate');

        try {
            /**
             * @var \Amasty\PromoBanners\Model\Rule $ruleModel
             */
            $ruleModel = $this->_objectManager->get('Amasty\PromoBanners\Model\Rule');
            $ruleModel->massChangeStatus($ids, $status);
            $message = __('Record(s) have been updated.');
            $this->messageManager->addSuccessMessage($message);
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('We can\'t change status of rules(s) right now. Please review the log and try again. ') . $e->getMessage()
            );
            $this->logger->critical($e);
        }

        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Amasty_PromoBanners::ampromobanners');
    }
}
