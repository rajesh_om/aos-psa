<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_PromoBanners
 */


namespace Amasty\PromoBanners\Controller\Adminhtml\Banners;

use Magento\Backend\App\Action;
use Psr\Log\LoggerInterface;

class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        Action\Context $context,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->logger = $logger;
    }

    public function execute()
    {
        $ids = $this->getRequest()->getParam('ids');

        try {
            /**
             * @var $collection \Amasty\PromoBanners\Model\ResourceModel\Rule\Collection
             */
            $collection = $this->_objectManager->create('Amasty\PromoBanners\Model\ResourceModel\Rule\Collection');
            $collection->addFieldToFilter('id', ['in' => $ids]);
            $collection->walk('delete');
            $this->messageManager->addSuccessMessage(__('Rules(s) were successfully deleted'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(
                __('We can\'t delete rule(s) right now. Please review the log and try again. ') . $e->getMessage()
            );

            $this->logger->critical($e);
        }

        $this->_redirect('*/*/');
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Amasty_PromoBanners::ampromobanners');
    }
}
