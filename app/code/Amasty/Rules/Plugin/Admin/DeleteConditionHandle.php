<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Rules
 */


namespace Amasty\Rules\Plugin\Admin;

class DeleteConditionHandle
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    private $request;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    private $moduleManager;

    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Framework\Module\Manager $moduleManager
    ) {
        $this->request = $request;
        $this->moduleManager = $moduleManager;
    }

    /**
     * @param \Amasty\Conditions\Observer\Admin\AddNewConditionHandle $subject
     * @param callable $proceed
     * @param \Magento\Framework\Event\Observer $observer
     *
     * @return \Amasty\Conditions\Observer\Admin\AddNewConditionHandle
     */
    public function aroundExecute(
        \Amasty\Conditions\Observer\Admin\AddNewConditionHandle $subject,
        callable $proceed,
        \Magento\Framework\Event\Observer $observer
    ) {
        $moduleName = $this->request->getModuleName();
        if ($moduleName === 'sales_rule' && !$this->moduleManager->isEnabled('Amasty_Promo')) {
            return $subject;
        }

        return $proceed($observer);
    }
}
