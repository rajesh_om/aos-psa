<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Promo
 */


namespace Amasty\Promo\Helper;

use Amasty\Promo\Model\ItemRegistry\PromoItemRegistry;
use Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface;
use Magento\CatalogInventory\Model\Spi\StockStateProviderInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Exception\LocalizedException;

/**
 * Add promo items to cart, update total quantity of cart, save cart
 */
class Cart
{
    /**
     * @var \Magento\Checkout\Model\Cart
     */
    private $cart;

    /**
     * @var \Amasty\Promo\Model\Registry
     */
    private $promoRegistry;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    private $stockRegistry;

    /**
     * @var \Amasty\Promo\Helper\Messages
     */
    private $promoMessagesHelper;
    
    /**
     * @var StockStateProviderInterface
     */
    private $stockStateProvider;

    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    private $metadata;

    /**
     * @var \Amasty\Promo\Helper\Item
     */
    private $promoItemHelper;

    /**
     * @var PromoItemRegistry
     */
    private $promoItemRegistry;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    public function __construct(
        \Magento\Checkout\Model\Cart $cart,
        \Amasty\Promo\Model\Registry $promoRegistry,
        StockRegistryProviderInterface $stockRegistry,
        \Amasty\Promo\Helper\Messages $promoMessagesHelper,
        StockStateProviderInterface $stockStateProvider,
        \Magento\Framework\App\ProductMetadataInterface $metadata,
        \Amasty\Promo\Helper\Item $promoItemHelper,
        PromoItemRegistry $promoItemRegistry,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->cart = $cart;
        $this->promoRegistry = $promoRegistry;
        $this->stockRegistry = $stockRegistry;
        $this->promoMessagesHelper = $promoMessagesHelper;
        $this->stockStateProvider = $stockStateProvider;
        $this->metadata = $metadata;
        $this->promoItemHelper = $promoItemHelper;
        $this->promoItemRegistry = $promoItemRegistry;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param int $qty
     * @param \Amasty\Promo\Model\ItemRegistry\PromoItemData $promoItemData
     * @param array $requestParams
     * @param \Magento\Quote\Model\Quote|null $quote
     *
     * @return bool
     */
    public function addProduct(
        \Magento\Catalog\Model\Product $product,
        $qty,
        $promoItemData,
        $requestParams = [],
        \Magento\Quote\Model\Quote $quote = null
    ) {
        if ($product->getTypeId() == \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE) {
            $availableQty = $this->checkAvailableQty($product, $qty, $quote);

            if ($availableQty <= 0) {
                $this->promoMessagesHelper->addAvailabilityError($product);

                return false;
            } else {
                if ($availableQty < $qty) {
                    $this->promoMessagesHelper->showMessage(
                        __(
                            "We apologize, but requested quantity of free gift <strong>%1</strong> is not available at the moment",
                            $product->getName()
                        ),
                        false,
                        true
                    );
                }
            }

            $qty = $availableQty;
        }
        if ($qty == 0) {
            return false;
        }

        $ruleId = $promoItemData->getRuleId();
        //TODO ST-1949 process not free items with custom_price
        $requestParams['qty'] = $qty;
        $requestParams['options']['ampromo_rule_id'] = $ruleId;
        $requestParams['options']['discount'] = $promoItemData->getDiscountArray();

        try {
            if ($quote instanceof \Magento\Quote\Model\Quote
                && !$this->cart->hasData('quote')
            ) {
                $this->cart->setQuote($quote); //prevent quote afterload event in cart::addProduct()
            }
            $cartQuote = $this->cart->getQuote();
            $item = $cartQuote->addProduct($product, new \Magento\Framework\DataObject($requestParams));

            if ($item instanceof \Magento\Quote\Model\Quote\Item) {
                $item->setData('ampromo_rule_id', $ruleId);
                $this->collectTotals($item, $cartQuote);
            } else {
                throw new LocalizedException(__($item));
            }

            //qty for promoItemData will be reserved later
            $promoItemData->isDeleted(false);
            if (isset($quote) && !$quote->hasData('is_copy')) {
                $this->promoMessagesHelper->showMessage(
                    __(
                        "Free gift <strong>%1</strong> was added to your shopping cart",
                        $product->getName()
                    ),
                    false,
                    true
                );
            }

            return true;
        } catch (\Exception $e) {
            $this->promoMessagesHelper->showMessage(
                $e->getMessage(),
                true,
                true
            );
        }

        return false;
    }

    /**
     * Set flags for recollect totals later. Totals will be recollected on cart save
     *
     * @param \Magento\Quote\Model\Quote\Item $item
     * @param \Magento\Quote\Model\Quote $cartQuote
     */
    private function collectTotals(\Magento\Quote\Model\Quote\Item $item, \Magento\Quote\Model\Quote $cartQuote)
    {
        if ($item->getProductType() !== Configurable::TYPE_CODE) {
            $cartQuote->setTotalsCollectedFlag(false);
            $cartQuote->getShippingAddress()->setCollectShippingRates(true);
        }
    }

    /**
     * @param \Magento\Quote\Model\Quote|null $quote
     */
    public function saveCart(
        \Magento\Quote\Model\Quote $quote = null
    ) {
        if ($quote) {
            $this->cart->setQuote($quote);
        } else {
            $quote = $this->cart->getQuote();
        }
        if (!$quote->getId()) {
            $this->updateTotalQty($quote);
        } else {
            try {
                $this->cart->save();
            } catch (\Exception $e) {
                $this->promoMessagesHelper->showMessage(
                    __(
                        "Something went wrong. Please try again later."
                    ),
                    true,
                    true
                );
                $this->logger->critical($e->getTraceAsString());
            }
        }
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     */
    public function updateTotalQty($quote)
    {
        $quote->setItemsCount(0);
        $quote->setItemsQty(0);
        $quote->setVirtualItemsQty(0);

        $items = $quote->getAllVisibleItems();
        foreach ($items as $item) {
            if ($item->getParentItem()) {
                continue;
            }

            $children = $item->getChildren();
            if ($children && $item->isShipSeparately()) {
                foreach ($children as $child) {
                    if ($child->getProduct()->getIsVirtual()) {
                        $qty = $quote->getVirtualItemsQty() + $child->getQty() * $item->getQty();
                        $quote->setVirtualItemsQty($qty);
                    }
                }
            }

            if ($item->getProduct()->getIsVirtual()) {
                $quote->setVirtualItemsQty($quote->getVirtualItemsQty() + $item->getQty());
            }
            $quote->setItemsCount($quote->getItemsCount() + 1);
            $quote->setItemsQty((float)$quote->getItemsQty() + $item->getQty());
        }
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param int $qtyRequested
     * @param \Magento\Quote\Model\Quote|null $quote
     *
     * @return float|int
     */
    public function checkAvailableQty(
        \Magento\Catalog\Model\Product $product,
        $qtyRequested,
        $quote = null
    ) {
        $stockItem = $this->stockRegistry->getStockItem(
            $product->getId(),
            $product->getStore()->getWebsiteId()
        );

        $qtyAdded = 0;
        if ($quote instanceof \Magento\Quote\Model\Quote) {
            $items = $quote->getItemsCollection();
        } else {
            $items =  $this->cart->getItems();
        }
        foreach ($items as $item) {
            if ($item->getProductId() == $product->getId()) {
                $qtyAdded += $item->getQty();
            }
        }

        $totalQty = $qtyRequested + $qtyAdded;

        $checkResult = $this->stockStateProvider->checkQuoteItemQty(
            $stockItem,
            $qtyRequested,
            $totalQty,
            $qtyRequested
        );

        if ($checkResult->getData('has_error')) {
            if (!$this->stockStateProvider->checkQty($stockItem, $totalQty)) {
                return $stockItem->getQty() - $qtyAdded;
            }

            if ($stockItem->getBackorders()) {
                return $stockItem->getMaxSaleQty() - $qtyAdded;
            }

            return 0;
        } else {
            return $qtyRequested;
        }
    }
}
