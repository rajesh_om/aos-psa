<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Promo
 */


namespace Amasty\Promo\Observer;

use Amasty\Promo\Model\ItemRegistry\PromoItemRegistry;
use Magento\Framework\Event\ObserverInterface;

/**
 * Remove all not allowed items
 */
class CollectTotalsAfterObserver implements ObserverInterface
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    private $_coreRegistry;

    /**
     * @var \Amasty\Promo\Helper\Cart
     */
    private $promoCartHelper;

    /**
     * @var \Amasty\Promo\Helper\Item
     */
    private $promoItemHelper;

    /**
     * @var \Amasty\Promo\Model\Registry
     */
    private $promoRegistry;

    /**
     * @var \Amasty\Promo\Model\Config
     */
    private $config;

    /**
     * @var \Magento\Framework\Event\ManagerInterface
     */
    private $eventManager;

    /**
     * @var \Amasty\Promo\Model\ItemRegistry\PromoItemRegistry
     */
    private $promoItemRegistry;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    private $productRepository;

    /**
     * flag for save cart and recollect totals
     * @var bool
     */
    protected $recollectTotals = false;

    public function __construct(
        \Magento\Framework\Registry $registry,
        \Amasty\Promo\Helper\Cart $promoCartHelper,
        \Amasty\Promo\Helper\Item $promoItemHelper,
        \Amasty\Promo\Model\Registry $promoRegistry,
        \Amasty\Promo\Model\Config $config,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Amasty\Promo\Model\ItemRegistry\PromoItemRegistry $promoItemRegistry,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ) {
        $this->_coreRegistry = $registry;
        $this->promoCartHelper = $promoCartHelper;
        $this->promoItemHelper = $promoItemHelper;
        $this->promoRegistry = $promoRegistry;
        $this->config = $config;
        $this->eventManager = $eventManager;
        $this->promoItemRegistry = $promoItemRegistry;
        $this->productRepository = $productRepository;
    }

    /**
     * Add product automatically
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->recollectTotals = false;
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $observer->getQuote();

        $this->updateQuoteItems($quote);
        $this->addProductsAutomatically($quote);

        $this->promoItemRegistry->save();

        if ($this->recollectTotals) {
            $this->promoCartHelper->saveCart($quote);
        }
    }

    /**
     * If applicable, add products to cart automatically
     *
     * @param \Magento\Quote\Model\Quote $quote
     */
    public function addProductsAutomatically($quote = null)
    {
        foreach ($this->promoItemRegistry->getItemsForAutoAdd() as $promoItem) {
            $isAdded = $this->promoCartHelper->addProduct(
                $this->productRepository->get($promoItem->getSku(), false, null, true),
                $promoItem->getQtyToProcess(),
                $promoItem,
                [],
                $quote
            );

            if (!$this->recollectTotals && $isAdded) {
                $this->recollectTotals = true;
            }
        }
    }

    /**
     * Update Quote Items quantity added by Free Gift
     *
     * @param \Magento\Quote\Model\Quote $quote
     */
    public function updateQuoteItems($quote)
    {
        $this->promoItemRegistry->resetQtyReserve();
        /** @var \Magento\Quote\Model\Quote\Item $item */
        foreach ($quote->getAllVisibleItems() as $item) {
            if (!$item->getParentItem() && $this->promoItemHelper->isPromoItem($item)) {
                $sku = $item->getProduct()->getData('sku');

                $ruleId = $this->promoItemHelper->getRuleId($item);
                $item->setQuote($quote);
                $promoData = $this->promoItemRegistry->getItemBySkuAndRuleId($sku, $ruleId);
                if (!$promoData || (float)$promoData->getQtyToProcess() <= 0.00001) {
                    $this->removeGift($item);
                    $this->recollectTotals = true;
                    continue;
                }
                if ((float)$item->getQty() > (float)$promoData->getQtyToProcess()) {
                    $item->setQty($promoData->getQtyToProcess());
                    $this->recollectTotals = true;
                }
                $this->promoItemRegistry->assignQtyToItem(
                    $item->getQty(),
                    $promoData,
                    PromoItemRegistry::QTY_ACTION_RESERVE
                );
            }
        }
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $item
     */
    private function removeGift($item)
    {
        $quote = $item->getQuote();
        if ($item->getId()) {
            $quote->removeItem($item->getId());
        } else {
            $item->isDeleted(true);
            if ($item->getHasChildren()) {
                foreach ($item->getChildren() as $child) {
                    $child->isDeleted(true);
                }
            }

            $parent = $item->getParentItem();
            if ($parent) {
                $parent->isDeleted(true);
            }
            $this->eventManager->dispatch('sales_quote_remove_item', ['quote_item' => $item]);

            //reassemble collection items, otherwise 'deleted' items without ID will be saved
            $collection = $quote->getItemsCollection();
            $items = $collection->getItems();
            $collection->removeAllItems();

            /** @var \Magento\Quote\Model\Quote\Item $row */
            foreach ($items as $row) {
                if ($row->getId() || !$row->isDeleted()) {
                    $collection->addItem($row);
                }
            }
        }
    }
}
