<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Xnotif
 */


namespace Amasty\Xnotif\Model\ResourceModel\Stock\Subscription;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactoryInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\ProductAlert\Model\ResourceModel\Stock\Collection as StockCollection;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Model\Product;
use Psr\Log\LoggerInterface;

class Collection extends StockCollection
{
    /**
     * @var AttributeRepositoryInterface
     */
    private $attributeRepository;

    /**
     * @var ProductMetadataInterface
     */
    private $productMetadata;

    public function __construct(
        EntityFactoryInterface $entityFactory,
        LoggerInterface $logger,
        FetchStrategyInterface $fetchStrategy,
        ManagerInterface $eventManager,
        AttributeRepositoryInterface $attributeRepository,
        ProductMetadataInterface $productMetadata,
        AdapterInterface $connection = null,
        AbstractDb $resource = null
    ) {
        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
        $this->attributeRepository = $attributeRepository;
        $this->productMetadata = $productMetadata;
    }

    /**
     * @return string
     */
    public function getIdFieldName()
    {
        $idFieldName = parent::getIdFieldName();
        if (!$idFieldName) {
            $idFieldName = 'alert_stock_id';
            $this->_setIdFieldName($idFieldName);
        }

        return $idFieldName;
    }

    /**
     * @return $this
     */
    public function _renderFiltersBefore()
    {
       $nameAttribute = $this->attributeRepository->get(Product::ENTITY, 'name');
       $productIdRow = $this->productMetadata->getEdition() != 'Community' ? 'row_id' : 'entity_id';
       $productVarcharTable = $this->getTable('catalog_product_entity_varchar');

       $this->getSelect()
           ->join(
               ['product' => $this->getTable('catalog_product_entity')],
               'product.entity_id = main_table.product_id',
               ['product_sku' => 'sku']
           )
           ->joinLeft(
               ['customer' => $this->getTable('customer_entity')],
               'customer.entity_id = main_table.customer_id',
               ['last_name' => 'lastname', 'first_name' => 'firstname']
           )
           ->joinLeft(
               ['product_name_by_store' => $productVarcharTable],
               'product.entity_id = product_name_by_store.' . $productIdRow . ' AND product_name_by_store.attribute_id = '
               . $nameAttribute->getAttributeId() . ' AND ' . $this->getStoreIdColumn() . ' = product_name_by_store.store_id'
           )
           ->joinLeft(
               ['product_name_default' => $productVarcharTable],
               'product.entity_id = product_name_default.' . $productIdRow . ' AND product_name_default.attribute_id = '
               . $nameAttribute->getAttributeId() . ' AND product_name_default.store_id = 0'
           )
       ;

       $this->updateCustomerFields();

       parent::_renderFiltersBefore();

       return $this;
    }

    /**
     * Check if field exist in alert table; else get from customer table
     */
    private function updateCustomerFields()
   {
       $columnsPart = $this->getSelect()->getPart('columns');

       $email = new \Zend_Db_Expr('IF (main_table.email IS NOT NULL, main_table.email, customer.email)');
       $productName = new \Zend_Db_Expr('IF (product_name_by_store.value IS NOT NULL, product_name_by_store.value, product_name_default.value)');

       $columnsPart[] = [
           'main_table',
           $this->getStoreIdColumn(),
           'result_store_id'
       ];
       $columnsPart[] = [
           'main_table',
           $email,
           'email'
       ];
       $columnsPart[] = [
           'main_table',
           $productName,
           'product_name'
       ];

       $this->getSelect()->setPart('columns', $columnsPart);
   }

    /**
     * @return \Zend_Db_Expr
     */
    private function getStoreIdColumn()
   {
       return new \Zend_Db_Expr('IF (main_table.store_id IS NOT NULL, main_table.store_id, customer.store_id)');
   }
}
