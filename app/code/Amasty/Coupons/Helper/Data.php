<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Helper;

use Amasty\Coupons\Model\Config;
use Magento\Framework\App\Area;
use Magento\Quote\Model\Quote\Address;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\SalesRule\Model\Coupon;
use Magento\SalesRule\Model\ResourceModel\Coupon as CouponResource;
use Amasty\Coupons\Model\CouponRenderer;
use Magento\Backend\Model\Session\Quote;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Amasty\Coupons\Api\RuleRepositoryInterface;

/**
 * Class Data
 */
class Data
{
    /**
     * @var Coupon
     */
    private $couponModel;

    /**
     * @var CheckoutSession
     */
    private $session;

    /**
     * @var CouponResource
     */
    private $couponResource;

    /**
     * @var CouponRenderer
     */
    private $couponRenderer;

    /**
     * @var Quote
     */
    private $backendSession;

    /**
     * @var State
     */
    private $state;

    /**
     * @var RuleRepositoryInterface
     */
    private $ruleRepository;

    /**
     * @var Config
     */
    private $config;

    public function __construct(
        CheckoutSession $session,
        Coupon $couponModel,
        CouponResource $couponResourceModel,
        CouponRenderer $couponRenderer,
        Quote $backendSession,
        State $state,
        RuleRepositoryInterface $ruleRepository,
        Config $config
    ) {
        $this->couponModel = $couponModel;
        $this->session = $session;
        $this->couponResource = $couponResourceModel;
        $this->couponRenderer = $couponRenderer;
        $this->backendSession = $backendSession;
        $this->state = $state;
        $this->ruleRepository = $ruleRepository;
        $this->config = $config;
    }

    /**
     * @param bool $isRuleApplied
     * @param Address|null $address
     *
     * @return array|bool
     * @throws LocalizedException
     */
    public function getRealAppliedCodes($isRuleApplied = false, $address = null)
    {
        if ($address) {
            $quote = $address->getQuote();
        } else {
            $quote = $this->state->getAreaCode() === Area::AREA_ADMINHTML
                ? $this->backendSession->getQuote()
                : $this->session->getQuote();
        }

        if (!$quote->getCouponCode()) {
            return [];
        }

        $coupons = $this->couponRenderer->parseCoupon($quote->getCouponCode());
        $appliedRules = array_map('trim', explode(',', $quote->getAppliedRuleIds()));

        if (!$appliedRules) {
            return false;
        }

        $rulesIds = [];

        foreach ($coupons as $key => $coupon) {
            /** @var Coupon $rule */
            $rule = $this->couponModel->loadByCode($coupon);

            try {
                /** @var \Amasty\Coupons\Model\Rule $customRule */
                $customRule = $this->ruleRepository->getById($rule->getRuleId());
                $isAllowCouponsSameRule = $customRule->getAllowCouponsSameRule();
            } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
                $isAllowCouponsSameRule = $this->config->isAllowCouponsSameRule();
            }

            if (($rule->getType() && !$isAllowCouponsSameRule)
                && (isset($customRule)
                    && ((!$customRule->getUseConfigValue() && !$customRule->getAllowCouponsSameRule())
                        || ($customRule->getUseConfigValue() && !$this->config->isAllowCouponsSameRule())))
                || (!isset($customRule) && !$this->config->isAllowCouponsSameRule())
            ) {
                $rulesIds[] = $rule->getRuleId();
            }

            if (!$this->couponResource->exists($coupon)
                || (!$isRuleApplied && !in_array($rule->getRuleId(), $appliedRules))
                || in_array($rule->getRuleId(), $rulesIds) && array_count_values($rulesIds)[$rule->getRuleId()] > 1
            ) {
                unset($coupons[$key]);
            }
        }

        $coupons = array_unique($coupons);

        return $coupons;
    }
}
