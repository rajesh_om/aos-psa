<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Block;

use Magento\Checkout\Block\Cart\AbstractCart;
use Amasty\Coupons\Helper\Data;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Coupon
 */
class Coupon extends AbstractCart
{
    /**
     * @var Data
     */
    protected $_couponHelper;

    /**
     * @var FormKey
     */
    private $formKey;

    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        CheckoutSession $checkoutSession,
        Data $couponHelper,
        FormKey $formKey,
        array $data = []
    ) {
        parent::__construct($context, $customerSession, $checkoutSession, $data);
        $this->_isScopePrivate = true;
        $this->_couponHelper = $couponHelper;
        $this->formKey = $formKey;
    }

    /**
     * @return array|bool
     * @throws LocalizedException
     */
    public function getCouponsCode()
    {
        return $this->_couponHelper->getRealAppliedCodes();
    }

    /**
     * @return string
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
}
