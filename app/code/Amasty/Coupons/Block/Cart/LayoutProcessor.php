<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Block\Cart;

use Magento\Checkout\Block\Checkout\LayoutProcessorInterface;
use Amasty\Coupons\Model\DiscountCollector;

/**
 * Class LayoutProcessor
 */
class LayoutProcessor implements LayoutProcessorInterface
{
    /**
     * @var DiscountCollector
     */
    protected $discountCollector;

    public function __construct(
        DiscountCollector $discountCollector
    ) {
        $this->discountCollector = $discountCollector;
    }

    /**
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        $couponsWithDiscount = $this->discountCollector->getRulesWithAmount();
        $jsLayout['components']['block-totals']['children']['before_grandtotal']
        ['children']['coupon']['config']['amount'] = $couponsWithDiscount;

        return $jsLayout;
    }
}
