<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Block\Adminhtml\Order\Create\Coupons;

use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\Session\Quote;
use Magento\Sales\Model\AdminOrder\Create;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Amasty\Coupons\Helper\Data;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Form
 */
class Form extends \Magento\Sales\Block\Adminhtml\Order\Create\Coupons\Form
{
    /**
     * @var Data
     */
    private $couponHelper;

    public function __construct(
        Context $context,
        Quote $sessionQuote,
        Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        Data $couponHelper,
        array $data = []
    ) {
        parent::__construct($context, $sessionQuote, $orderCreate, $priceCurrency, $data);
        $this->couponHelper = $couponHelper;
    }

    /**
     * @return array|bool
     * @throws LocalizedException
     */
    public function getCouponsCodes()
    {
        return $this->couponHelper->getRealAppliedCodes();
    }

    /**
     * return onclick button js code
     *
     * @return string
     */
    public function getCouponJs()
    {
        $couponsString = '';
        $couponsCodes = implode(',', $this->couponHelper->getRealAppliedCodes());

        if ($couponsCodes) {
            $couponsString = '\'' . $couponsCodes . ',\' + ';
        }
        $couponsString .= '$F(\'coupons:code\')';

        return 'order.applyCoupon(' . $couponsString . ')';
    }
}
