/**
 * Copyright © 2015 Amasty. All rights reserved.
 */
define(
    [
        'jquery',
        'ko',
        'underscore',
        'mage/translate',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Magento_Ui/js/model/messageList',
        'Amasty_Coupons/js/action/set-coupon-code',
        'Magento_SalesRule/js/action/cancel-coupon'
    ],
    function ($, ko, _, $t, Component, quote, messageList, setCouponCodeAction, cancelCouponAction) {
        'use strict';

        var totals = quote.getTotals(),
            couponCode = ko.observable(''),
            fakeCouponCode = ko.observable('');

        if (totals()['coupon_code']) {
            couponCode(totals()['coupon_code']);
        }
        var isApplied = ko.observable(couponCode() != null);
        var isLoading = ko.observable(false);
        var message = $t('Your coupon was successfully applied'),
            messageError = $t('Coupon code is not valid'),
            messageDelete = $t('Coupon code was removed');
        return Component.extend({
            defaults: {
                template: 'Amasty_Coupons/payment/discount'
            },
            couponCode: couponCode,
            fakeCouponCode: fakeCouponCode,

            /**
             * Applied flag
             */
            isApplied: isApplied,
            isLoading: isLoading,

            removeSelected : function (coupon) {
                var currentCodeList = this.couponCode().split(',');
                var index = currentCodeList.indexOf(coupon);
                if (index > -1) {
                    currentCodeList.splice(index, 1);
                }

                isLoading(true);
                if( currentCodeList.length > 0 ){
                    setCouponCodeAction(currentCodeList.join(','), isApplied, true).done(function (response) {
                        $('.totals.discount .title').removeClass('negative');
                        this.couponCode(response);
                        messageList.addSuccessMessage({'message': messageDelete});
                        this.fakeCouponCode('');
                    }.bind(this)).always(function(){
                        isLoading(false);
                    });
                }else{
                    this.couponCode('');
                    cancelCouponAction(isApplied, isLoading).always(function(){
                        isLoading(false);
                    });
                }
            },

            /**
             * Coupon code application procedure
             */
            apply: function() {
                if (this.validate()) {
                    isLoading(true);
                    var newDiscountCode =  this.fakeCouponCode();
                    var code = [];
                    code = this.couponCode().split(',');
                    code.push(newDiscountCode);
                    code = code.filter(function(n){ return n != '' });
                    code = code.join(',');
                    setCouponCodeAction(code, isApplied).done(function (response) {
                        $('.totals.discount .title').removeClass('negative');
                        var codeList =  response.split(',');
                        this.couponCode(response);

                        var newCode = this.fakeCouponCode().split(',');
                        if (_.difference(newCode, codeList).length) {
                            messageList.addErrorMessage({'message': messageError});
                        } else{
                            messageList.addSuccessMessage({'message': message});
                        }
                        this.fakeCouponCode('');
                    }.bind(this)).always(function(){
                        isLoading(false);
                    });
                }
            },

            /**
             * Cancel using coupon
             */
            cancel: function() {
                if (this.validate()) {
                    isLoading(true);
                    couponCode('');
                    cancelCouponAction(isApplied, isLoading);
                }
            },

            /**
             * Coupon form validation
             *
             * @returns {boolean}
             */
            validate: function() {
                var form = '#discount-form';
                return $(form).validation() && $(form).validation('isValid');
            }
        });
    }
);
