define([
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/totals',
    'jquery'
], function (Component, totals, $) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Amasty_Coupons/summary/coupon'
        },

        initialize: function () {
            $(document).on('click', '.totals.discount', function () {
                $(".total_coupons").toggle();
                $(this).find('.title').toggleClass('negative');
            });

            this._super();
        },

        getCoupons: function () {
            var segment = totals.getSegment('amasty_coupon_amount');
            if (!segment) {
                return [];
            }

            //Magento 2.1.9 <= value type
            if ($.type(segment.value[0]) !== 'undefined' && $.type(segment.value[0]) === 'string') {
                segment.value = $.map(segment.value, function(value) {
                    return JSON.parse(value);
                });
            }

            return segment.value;
        }
    });
});
