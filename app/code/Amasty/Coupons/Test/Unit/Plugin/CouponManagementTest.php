<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Test\Unit\Plugin;

use Amasty\Coupons\Plugin\CouponManagement;
use Amasty\Coupons\Test\Unit\Traits;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

/**
 * Class CouponManagementTest
 *
 * @see CouponManagement
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @codingStandardsIgnoreFile
 */
class CouponManagementTest extends \PHPUnit\Framework\TestCase
{
    use Traits\ObjectManagerTrait;
    use Traits\ReflectionTrait;

    const APPLIED_COUPON_CODE = 'test';
    const COUPONE_CODE = 'code';

    /**
     * @var CouponManagement|MockObject
     */
    private $plugin;

    /**
     * @var \Magento\Quote\Model\CouponManagement
     */
    private $subject;

    public function setUp()
    {
        $this->subject = $this->createMock(\Magento\Quote\Model\CouponManagement::class);
        $this->plugin = $this->createPartialMock(CouponManagement::class, ['getRealAppliedCodes']);
    }

    /**
     * @covers CouponManagement::afterGet
     *
     * @dataProvider afterGetDataProvider
     *
     * @throws \ReflectionException
     */
    public function testAfterGet($value, $expectedResult = true)
    {
        $helper = $this->createMock(\Amasty\Coupons\Helper\Data::class);
        $helper->expects($this->any())->method('getRealAppliedCodes')->willReturn($value);
        $this->setProperty($this->plugin, 'amHelper', $helper, CouponManagement::class);

        $result = $this->plugin->afterGet($this->subject, true);

        $this->assertEquals($result, $expectedResult);
    }

    /**
     * @covers CouponManagement::beforeSet
     *
     * @dataProvider beforeSetDataProvider
     *
     * @throws \ReflectionException
     */
    public function testBeforeSet($value, $cartId, $couponCode, $expectedResult = null)
    {
        $couponRenderer = $this->createMock(\Amasty\Coupons\Model\CouponRenderer::class);
        $couponRenderer->expects($this->any())->method('render')->willReturn($value);
        $this->setProperty($this->plugin, 'couponRenderer', $couponRenderer, CouponManagement::class);

        $result = $this->plugin->beforeSet($this->subject, $cartId, $couponCode);

        $this->assertEquals($result, $expectedResult);
    }

    /**
     * Data provider for afterGet test
     * @return array
     */
    public function afterGetDataProvider()
    {
        return [
            [false],
            [
                [self::APPLIED_COUPON_CODE], self::APPLIED_COUPON_CODE
            ],
            [self::APPLIED_COUPON_CODE]
        ];
    }

    /**
     * Data provider for afterGet test
     * @return array
     */
    public function beforeSetDataProvider()
    {
        return [
            [self::APPLIED_COUPON_CODE, 1, self::COUPONE_CODE, [1, self::APPLIED_COUPON_CODE]],
            [
                [self::APPLIED_COUPON_CODE], 1, self::COUPONE_CODE
            ],
            [123, 1, self::COUPONE_CODE],
            [true, 1, self::COUPONE_CODE]
        ];
    }
}
