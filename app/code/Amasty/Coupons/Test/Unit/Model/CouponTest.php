<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Test\Unit\Model;

use Amasty\Coupons\Model\Coupon;
use Amasty\Coupons\Test\Unit\Traits;
use PHPUnit_Framework_MockObject_MockObject as MockObject;

/**
 * Class CouponTest
 *
 * @see Coupon
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @codingStandardsIgnoreFile
 */
class CouponTest extends \PHPUnit\Framework\TestCase
{
    use Traits\ObjectManagerTrait;
    use Traits\ReflectionTrait;

    const AMOUNT_RULE = [
        'ruleCode' => 100,
    ];

    /**
     * @var Coupon
     */
    private $model;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $session;

    public function setUp()
    {
        $store = $this->createPartialMock(\Magento\Store\Model\Store::class, [
            'getBaseCurrency',
            'getCurrentCurrency',
            'format'
        ]);
        $store->expects($this->any())->method('getBaseCurrency')->willReturn($store);
        $store->expects($this->any())->method('getCurrentCurrency')->willReturn($store);
        $store->expects($this->any())->method('format')->willReturn(100);

        $storeManager = $this->createMock(\Magento\Store\Model\StoreManager::class);
        $storeManager->expects($this->any())->method('getStore')->willReturn($store);
        $this->session = $this->getMockBuilder(\Magento\Checkout\Model\Session::class)
            ->disableOriginalConstructor()
            ->setMethods(['setCouponAmount', 'setCouponsCode'])
            ->getMock();
        $discountCollector = $this->getObjectManager()->getObject(\Amasty\Coupons\Model\DiscountCollector::class, [
            'storeManager' => $storeManager
        ]);
        $this->setProperty($discountCollector, 'amount', self::AMOUNT_RULE);

        $this->model = $this->getObjectManager()->getObject(Coupon::class, [
            'session' => $this->session,
            'discountCollector' => $discountCollector,
        ]);
    }

    /**
     * @covers Coupon::setCouponsValues
     *
     * @throws \ReflectionException
     */
    public function testSetCouponsValues()
    {
        $this->assertInstanceOf('Amasty\Coupons\Model\Coupon', $this->model->setCouponsValues());
        $this->session->expects($this->never())->method('setCouponAmount');
    }

    /**
     * @covers Coupon::setCouponsCode
     *
     * @throws \ReflectionException
     */
    public function testSetCouponsCode()
    {
        $this->session->expects($this->never())->method('setCouponsCode');
    }
}
