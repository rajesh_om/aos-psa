<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Test\Unit\Helper;

use Amasty\Coupons\Helper\Data;
use Amasty\Coupons\Model\CouponRenderer;
use Amasty\Coupons\Test\Unit\Traits;
use PHPUnit_Framework_MockObject_MockObject as MockObject;
use Magento\SalesRule\Model\Coupon as CouponModel;

/**
 * Class DataTest
 *
 * @see Data
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @codingStandardsIgnoreFile
 */
class DataTest extends \PHPUnit\Framework\TestCase
{
    use Traits\ObjectManagerTrait;
    use Traits\ReflectionTrait;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var \Magento\Quote\Model\Quote\Address|MockObject
     */
    private $address;

    /**
     * @var CouponModel|MockObject
     */
    private $couponModel;

    public function setUp()
    {
        $coupon = $this->createMock(\Magento\SalesRule\Model\ResourceModel\Coupon::class);
        $coupon->expects($this->any())->method('exists')->willReturn(true);

        $this->couponModel = $this->createMock(CouponModel::class);
        $this->couponModel->expects($this->any())->method('loadByCode')->willReturn($this->couponModel);

        $couponRenderer = $this->getObjectManager()->getObject(CouponRenderer::class);

        $this->helper = $this->getObjectManager()->getObject(Data::class, [
            'couponRenderer' => $couponRenderer,
            'couponResource' => $coupon,
            'couponModel' => $this->couponModel
        ]);

        $this->address = $this->getMockBuilder(\Magento\Quote\Model\Quote\Address::class)
            ->disableOriginalConstructor()
            ->setMethods(['getQuote'])
            ->getMock();
    }

    /**
     * @covers Data::getRealAppliedCodes
     *
     * @throws \ReflectionException
     */
    public function testGetRealAppliedCodes()
    {
        $quote = $this->getMockBuilder(\Magento\Quote\Model\Quote::class)
            ->disableOriginalConstructor()
            ->setMethods(['getCouponCode', 'getAppliedRuleIds'])
            ->getMock();
        $this->address->expects($this->any())->method('getQuote')->willReturn($quote);

        $result = $this->helper->getRealAppliedCodes(true, $this->address);
        $this->assertEmpty($result);

        $quote->expects($this->any())->method('getCouponCode')->willReturn('test1');
        $quote->expects($this->any())->method('getAppliedRuleIds')->willReturn('1');

        $ruleId = 1;
        $this->couponModel->expects($this->exactly(2))->method('getRuleId')->willReturnReference($ruleId);
        $result = $this->helper->getRealAppliedCodes(false, $this->address);
        $this->assertEquals(['test1'], $result);

        $ruleId = 2;
        $result = $this->helper->getRealAppliedCodes(false, $this->address);
        $this->assertEmpty($result);
    }
}
