<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Model;

use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Amasty\Coupons\Model\DiscountCollector;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Request\Http;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Coupon
 */
class Coupon extends AbstractTotal
{
    const DISCOUNT_CODE = 'amasty_coupon_amount';

    /**
     * @var DiscountCollector
     */
    private $discountCollector;

    /**
     * @var CheckoutSession
     */
    private $session;

    /**
     * @var Http
     */
    private $http;

    public function __construct(
        DiscountCollector $discountCollector,
        CheckoutSession $session,
        Http $http
    ) {
        $this->discountCollector = $discountCollector;
        $this->session = $session;
        $this->http = $http;
    }

    /**
     * Collect discount
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     *
     * @throws NoSuchEntityException
     */
    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);

        $this->setCouponsValues();
        $this->setCouponsCode($total);
    }

    /**
     * Assign discount amount
     *
     * @param Quote $quote
     * @param Total $total
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fetch(Quote $quote, Total $total)
    {
        $result = [];

        if (stripos($this->http->getOriginalPathInfo(), 'paypal/express') === false
            && stripos($this->http->getOriginalPathInfo(), 'braintree/paypal') === false
            && stripos($this->http->getOriginalPathInfo(), 'sales/order_create') === false
            && stripos($this->http->getOriginalPathInfo(), 'googlepay/review') === false) {
            $result =
                [
                    'code' => self::DISCOUNT_CODE,
                    'title' => __('AmastyCoupon'),
                    'value' => $this->session->getCouponAmount()
                ];
        }

        return $result;
    }

    /**
     * Set coupons values into session
     *
     * @throws NoSuchEntityException
     * @return $this
     */
    public function setCouponsValues()
    {
        $this->session->setCouponAmount($this->discountCollector->getRulesWithAmount());

        return $this;
    }

    /**
     * @param Total $total
     */
    public function setCouponsCode($total)
    {
        $total->setDiscountDescription($this->discountCollector->getCouponCode());
    }
}
