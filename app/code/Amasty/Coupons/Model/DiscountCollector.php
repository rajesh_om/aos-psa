<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Model;

use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class DiscountCollector
 */
class DiscountCollector
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var array
     */
    protected $amount = [];

    public function __construct(
        StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * Collect amount of discount for each rule
     *
     * @param string $ruleCode
     * @param float|int $amount
     */
    public function applyRuleAmount($ruleCode, $amount)
    {
        if (!isset($this->amount[$ruleCode])) {
            $this->amount[$ruleCode] = 0;
        }

        $this->amount[$ruleCode] += $amount;
    }

    /**
     * Return amount of discount for each rule
     *
     * @throws NoSuchEntityException
     * @return array
     */
    public function getRulesWithAmount()
    {
        $totalAmount = [];
        foreach ($this->amount as $ruleCode => $ruleAmount) {
            $totalAmount[] = [
                'coupon_code'   => $ruleCode,
                'coupon_amount' =>
                    '-' .$this->storeManager->getStore()->getCurrentCurrency()->format($ruleAmount, [], false)
            ];
        }

        return $totalAmount;
    }

    public function flushAmount()
    {
        $this->amount = [];
    }

    /**
     * @return string
     */
    public function getCouponCode()
    {
        $codes = array_keys($this->amount);

        return implode(',', $codes);
    }
}
