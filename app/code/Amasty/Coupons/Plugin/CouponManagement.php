<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Plugin;

use Amasty\Coupons\Helper\Data;
use Amasty\Coupons\Model\CouponRenderer;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class CouponManagement
 */
class CouponManagement
{
    /**
     * @var Data
     */
    private $amHelper;

    /**
     * @var CouponRenderer
     */
    private $couponRenderer;

    public function __construct(
        Data $helper,
        CouponRenderer $couponRenderer
    ) {
        $this->amHelper = $helper;
        $this->couponRenderer = $couponRenderer;
    }

    /**
     * @param \Magento\Quote\Model\CouponManagement $subject
     * @param string $result
     *
     * @throws LocalizedException
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGet($subject, $result)
    {
        $appliedCoupons = $this->amHelper->getRealAppliedCodes();
        if (is_array($appliedCoupons)) {
            return implode(',', $appliedCoupons);
        } else {
            return $result;
        }
    }

    /**
     * @param \Magento\Quote\Model\CouponManagement $subject
     * @param int $cartId The cart ID.
     * @param string $couponCode The coupon code data.
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeSet($subject, $cartId, $couponCode)
    {
        $renderedCode = $this->couponRenderer->render($couponCode);
        if (is_string($renderedCode)) {
            return [$cartId, $renderedCode];
        }

        return null;
    }

    /**
     * Temporary fix for checkout compatibility
     *
     * @param \Magento\Quote\Model\CouponManagement $subject
     * @param bool $result
     *
     * @throws LocalizedException
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSet($subject, $result)
    {
        return $this->afterGet($subject, $result);
    }
}
