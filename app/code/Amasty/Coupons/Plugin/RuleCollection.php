<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Plugin;

use Amasty\Coupons\Model\CouponRenderer;
use Magento\SalesRule\Model\ResourceModel\Rule\Collection;
use Magento\Quote\Model\Quote\Address;
use Magento\SalesRule\Model\Coupon;

/**
 * Class RuleCollection
 */
class RuleCollection
{
    /**
     * @var CouponRenderer
     */
    private $couponRenderer;

    /**
     * @var string
     */
    private $couponCode;

    /**
     * @var bool
     */
    private $isNeedToReplace = false;

    /**
     * @var Coupon
     */
    private $coupon;

    public function __construct(
        CouponRenderer $couponRenderer,
        Coupon $coupon
    ) {
        $this->couponRenderer = $couponRenderer;
        $this->coupon = $coupon;
    }

    /**
     * @param Collection $subject
     * @param int $websiteId
     * @param int $customerGroupId
     * @param string $couponCode
     * @param string|null $now
     * @param Address $address
     *
     * @return array|null
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function beforeSetValidationFilter(
        Collection $subject,
        $websiteId,
        $customerGroupId,
        $couponCode = '',
        $now = null,
        $address = null
    ) {
        $this->isNeedToReplace = false;
        if (!is_string($couponCode) || strpos($couponCode, ',') === false) {
            return null;
        }
        $this->couponCode = $couponCode;
        $couponCode = $this->couponRenderer->render($couponCode);
        if (is_string($couponCode)) {
            return [$websiteId, $customerGroupId, $couponCode, $now, $address];
        }
        $this->isNeedToReplace = true;

        return null;
    }

    /**
     * @param Collection $subject
     * @param Collection $result
     *
     * @return Collection
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSetValidationFilter(Collection $subject, $result)
    {
        if ($this->isNeedToReplace === false) {
            return $result;
        }

        $ruleIds = [];

        $coupons = $this->couponRenderer->parseCoupon($this->couponCode);

        $connection = $result->getConnection();
        $select = $result->getSelect();

        $searchCode = $connection->quoteInto(
            'code = ?',
            $this->couponCode
        );
        $replaceCodeIn = $connection->quoteInto(
            'code IN (?)',
            $coupons
        );

        $searchRuleId = 'rule_id IN (NULL)';

        $wherePart = empty($select->getPart(\Zend_Db_Select::WHERE))
            ? $select->getPart(\Zend_Db_Select::FROM)['t']['tableName']
                  ->getPart(\Zend_Db_Select::UNION)[1][0]
                  ->getPart(\Zend_Db_Select::FROM)['rule_coupons']
            : $select->getPart(\Zend_Db_Select::WHERE);

        foreach ($wherePart as &$where) {
            if (strpos($where, $searchCode) !== false) {
                $where = str_ireplace($searchCode, $replaceCodeIn, $where);
            } elseif (strpos($where, $searchRuleId) !== false) {
                foreach ($coupons as $coupon) {
                    $ruleIds[] = $this->coupon->loadByCode($coupon)->getRuleId();
                }
                $replaceRuleIdIN = $connection->quoteInto(
                    'rule_id IN (?)',
                    $ruleIds
                );
                $where = str_ireplace($searchRuleId, $replaceRuleIdIN, $where);
            }
        }

        if (empty($select->getPart(\Zend_Db_Select::WHERE))) {
            $unionPart =
                $select->getPart(\Zend_Db_Select::FROM)['t']['tableName']->getPart(\Zend_Db_Select::UNION)[1][0];
            $tmp = $unionPart->getPart(\Zend_Db_Select::FROM);
            $tmp['rule_coupons'] = array_merge($tmp['rule_coupons'], $wherePart);
            $unionPart->setPart(\Zend_Db_Select::FROM, $tmp);
        } else {
            $select->setPart(\Zend_Db_Select::WHERE, $wherePart);
        }

        $select->group('rule_id');

        return $result;
    }
}
