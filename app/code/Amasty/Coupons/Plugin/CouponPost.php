<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Plugin;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Message\Collection;
use Magento\Framework\Message\ManagerInterface;
use Amasty\Coupons\Helper\Data;
use Magento\Framework\Escaper;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class CouponPost
 */
class CouponPost
{
    const ONE_COUPON = 1;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @var Data
     */
    private $amHelper;

    /**
     * @var Escaper
     */
    private $escaper;

    public function __construct(
        RequestInterface $request,
        ManagerInterface $messageManager,
        Data $helper,
        Escaper $escaper
    ) {
        $this->request = $request;
        $this->messageManager = $messageManager;
        $this->amHelper = $helper;
        $this->escaper = $escaper;
    }

    /**
     * @param \Magento\Checkout\Controller\Cart\CouponPost $subject
     * @param \Magento\Framework\Controller\Result\Redirect $back
     *
     * @return mixed
     * @throws LocalizedException
     */
    public function afterExecute($subject, $back)
    {
        $appliedCodes = $this->amHelper->getRealAppliedCodes();
        $messages = $this->messageManager->getMessages();

        if (is_array($appliedCodes) && !empty($appliedCodes)) {
            /** @var Collection $messages */
            $messages->clear();
            $message = $this->messageManager->createMessage('success', 'amCoupons');
            $lastCode = trim($this->request->getParam('last_code'));
            $lastCodeArray = explode(',', $lastCode);
            $isRemoved = $this->request->getParam('remove_coupon');
            $messageText = $this->escaper->escapeHtml(__("You used coupon code \"%1\"", implode(",", $appliedCodes)));
            $message->setText($messageText);
            $this->messageManager->getMessages()->addMessage($message);

            if (count($lastCodeArray) === self::ONE_COUPON && !in_array($lastCode, $appliedCodes)) {
                $messages->deleteMessageByIdentifier('amCoupons');
                if ($isRemoved) {
                    $this->messageManager->addSuccessMessage(
                        $this->escaper->escapeHtml(__('You canceled the coupon code "%1".', $lastCode))
                    );
                } else {
                    $this->messageManager->addErrorMessage(
                        $this->escaper->escapeHtml(__('The coupon code "%1" is not valid.', $lastCode))
                    );
                }
            } elseif (count($lastCodeArray) > self::ONE_COUPON) {
                $couponsDiff = array_diff($lastCodeArray, $appliedCodes);
                if (count($couponsDiff) !== 0) {
                    $this->messageManager->addErrorMessage(
                        $this->escaper->escapeHtml(__('Coupon code(s) not valid: %1', implode(',', $couponsDiff)))
                    );
                }
            }
        } elseif ($appliedCodes === false || empty($appliedCodes)) {
            $this->processErrors($messages);
        }

        return $back;
    }

    /**
     * @param Collection $messages
     */
    private function processErrors($messages)
    {
        $errors = $messages->getErrors();
        $messages->clear();

        foreach ($errors as $error) {
            preg_match('/"[^\"]*"/', $error->getText(), $couponCodes);
            if ($couponCodes) {
                $this->messageManager->addErrorMessage(
                    $this->escaper->escapeHtml(__(
                        'Coupon code(s) not valid: %1',
                        str_replace('"', '', $couponCodes[0])
                    ))
                );
            } else {
                $this->messageManager->addErrorMessage($error->getText());
            }
        }
    }
}
