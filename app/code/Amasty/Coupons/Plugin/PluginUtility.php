<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Plugin;

use Amasty\Coupons\Helper\Data;
use Amasty\Coupons\Model\CouponRenderer;
use Magento\Backend\App\Area\FrontNameResolver;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\State;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote\Address;
use Magento\SalesRule\Model\Coupon;
use Magento\SalesRule\Model\ResourceModel\Coupon\UsageFactory;
use Magento\SalesRule\Model\Rule;
use Magento\SalesRule\Model\Utility;

/**
 * Class PluginUtility
 */
class PluginUtility
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var State
     */
    private $appState;

    /**
     * @var Data
     */
    private $helper;

    /**
     * @var CouponRenderer
     */
    private $couponRenderer;

    /**
     * @var Coupon
     */
    private $couponModel;

    /**
     * @var DataObjectFactory
     */
    private $objectFactory;

    /**
     * @var UsageFactory
     */
    private $usageFactory;

    public function __construct(
        RequestInterface $request,
        State $appState,
        Data $helper,
        CouponRenderer $couponRenderer,
        Coupon $couponModel,
        DataObjectFactory $objectFactory,
        UsageFactory $usageFactory
    ) {
        $this->request = $request;
        $this->appState = $appState;
        $this->helper = $helper;
        $this->couponRenderer = $couponRenderer;
        $this->couponModel = $couponModel;
        $this->objectFactory = $objectFactory;
        $this->usageFactory = $usageFactory;
    }

    /**
     * Check if rule can be applied for specific address/quote/customer
     * @param Utility $subject
     * @param \Closure $proceed
     * @param Rule $rule
     * @param Address $address
     *
     * @throws LocalizedException
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundCanProcessRule(Utility $subject, \Closure $proceed, $rule, $address)
    {
        $coupons = $this->helper->getRealAppliedCodes(true, $address);
        $couponEntity = $this->couponModel->loadPrimaryByRule($rule);
        $userKey = $this->couponRenderer->findCouponInArray($rule->getCode(), $coupons);
        $userKeyPerCustomer = $this->couponRenderer->findCouponInArray($couponEntity->getCode(), $coupons);

        if ($userKey !== false) {
            $originalCouponCode = $address->getQuote()->getCouponCode();
            
            if ($this->couponRenderer->isCouponUnique($rule->getCode())) {
                $address->getQuote()->setCouponCode($coupons[$userKey]);
            } else {
                $address->getQuote()->setCouponCode(implode(',', $coupons));
            }

            if ($proceed($rule, $address)) {
                //restore original coupon
                if ($this->appState->getAreaCode() == FrontNameResolver::AREA_CODE) {
                    $postParams = $this->request->getPost()->toArray();
                    $postParams['order']['coupon']['code'] = $address->getQuote()->getCouponCode();
                    $this->request->setPost(new \Zend\Stdlib\Parameters($postParams));
                }

                return true;
            } else {
                //restore original coupon
                $address->getQuote()->setCouponCode($originalCouponCode);
                
                return false;
            }
        } elseif ($userKeyPerCustomer !== false) {
            // check entire usage limit
            if ($couponEntity->getUsageLimit() && $couponEntity->getTimesUsed() >= $couponEntity->getUsageLimit()) {
                $rule->setIsValidForAddress($address, false);
                
                return false;
            }

            if (!$this->checkUsesPerCustomer($address, $couponEntity)) {
                $rule->setIsValidForAddress($address, false);
                
                return false;
            }
        }

        $quote = $address->getQuote();
        $quote->setCouponCode(implode(',', $coupons));

        return $proceed($rule, $address);
    }

    /**
     * @param Address $address
     * @param Coupon $couponEntity
     * @return bool
     */
    private function checkUsesPerCustomer($address, $couponEntity)
    {
        $customerId = $address->getQuote()->getCustomerId();

        if ($customerId && $couponEntity->getUsagePerCustomer()) {
            /** \Magento\Framework\DataObject $couponUsage */
            $couponUsage = $this->objectFactory->create();
            $this->usageFactory->create()->loadByCustomerCoupon(
                $couponUsage,
                $customerId,
                $couponEntity->getId()
            );
            
            if ($couponUsage->getCouponId() &&
                $couponUsage->getTimesUsed() >= $couponEntity->getUsagePerCustomer()
            ) {
                return false;
            }
        }

        return true;
    }
}
