<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Setup\Operation;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\Setup\SchemaSetupInterface;
use Amasty\Coupons\Model\ResourceModel\Rule;
use Amasty\Coupons\Api\Data\RuleInterface;
use Magento\SalesRule\Api\Data\RuleInterface as SalesRuleInterface;

/**
 * Class UpgradeTo110
 */
class UpgradeTo110
{
    /**
     * @var MetadataPool
     */
    private $metadata;

    public function __construct(MetadataPool $metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @param SchemaSetupInterface $setup
     */
    public function execute(SchemaSetupInterface $setup)
    {
        $connection = $setup->getConnection();
        $salesruleLinkField = $this->metadata->getMetadata(SalesRuleInterface::class)
            ->getLinkField();

        $table = $connection->newTable(
            $setup->getTable(RULE::TABLE_NAME)
        )->addColumn(
            RuleInterface::ENTITY_ID,
            Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Entity ID'
        )
            ->addColumn(
                RuleInterface::KEY_SALESRULE_ID,
                Table::TYPE_INTEGER,
                null,
                ['unsigned' => true, 'nullable' => false],
                'SalesRule ID'
            )
            ->addColumn(
                RuleInterface::ALLOW_COUPONS_SAME_RULE,
                Table::TYPE_BOOLEAN,
                1,
                ['nullable' => false, 'default' => 0],
                'Same Rule'
            )
            ->addColumn(
                RuleInterface::USE_CONFIG_VALUE,
                Table::TYPE_BOOLEAN,
                1,
                ['nullable' => false, 'default' => 0],
                'Use Config Value'
            )
            ->addForeignKey(
                $setup->getFkName(
                    RULE::TABLE_NAME,
                    RuleInterface::KEY_SALESRULE_ID,
                    'salesrule',
                    $salesruleLinkField
                ),
                RuleInterface::KEY_SALESRULE_ID,
                $setup->getTable('salesrule'),
                $salesruleLinkField,
                Table::ACTION_CASCADE
            )
            ->setComment('Amasty Coupons Rules Table');

        $connection->createTable($table);
    }
}
