<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Coupons
 */


namespace Amasty\Coupons\Observer;

use Amasty\Coupons\Api\RuleRepositoryInterface;
use Amasty\Coupons\Model\Config;
use Amasty\Coupons\Model\CouponRenderer;
use Amasty\Coupons\Model\DiscountCollector;
use Magento\SalesRule\Model\Coupon;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\SalesRule\Model\ResourceModel\Coupon as CouponResource;

/**
 * Class CouponObserver for salesrule_validator_process event
 */
class CouponObserver implements ObserverInterface
{
    const STR_TO_LOWER = 'strtolower';

    /**
     * @var DiscountCollector
     */
    protected $discountCollector;

    /**
     * @var string
     */
    private $codeKey = 0;

    /**
     * @var RuleRepositoryInterface
     */
    private $ruleRepository;

    /**
     * @var CouponResource
     */
    private $couponResource;

    /**
     * @var CouponRenderer
     */
    private $couponRenderer;

    /**
     * @var Coupon
     */
    private $couponModel;

    /**
     * @var Config
     */
    private $config;

    public function __construct(
        DiscountCollector $discountCollector,
        RuleRepositoryInterface $ruleRepository,
        CouponResource $couponResource,
        CouponRenderer $couponRenderer,
        Coupon $couponModel,
        Config $config
    ) {
        $this->discountCollector = $discountCollector;
        $this->ruleRepository = $ruleRepository;
        $this->couponResource = $couponResource;
        $this->couponRenderer = $couponRenderer;
        $this->couponModel = $couponModel;
        $this->config = $config;
    }

    /**
     * event salesrule_validator_process
     *
     * @param Observer $observer
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $appliedCodes = [];

        if ($observer->getData('quote')->getCouponCode()) {
            $appliedCodes = explode(',', $observer->getData('quote')->getCouponCode());
        }
        try {
            /** @var \Amasty\Coupons\Model\Rule $customRule */
            $rule = $this->ruleRepository->getById($observer->getEvent()->getRule()->getId());
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            $rule = $observer->getEvent()->getRule();
        }
        /** @var \Amasty\Coupons\Model\Rule $rule */
        if ($observer->getRule()->getCouponType() != \Magento\SalesRule\Model\Rule::COUPON_TYPE_NO_COUPON
            && (($rule->getAllowCouponsSameRule() && !$rule->getUseConfigValue())
                || ($rule->getAllowCouponsSameRule() === null && $this->config->isAllowCouponsSameRule())
                || ($rule->getUseConfigValue() && $this->config->isAllowCouponsSameRule()))
        ) {
            $discount = null;
            $baseAmount = $observer->getData('result')->getAmount();
            $couponCodes = $observer->getEvent()->getQuote()->getCouponCode();
            $couponCodesArray = $this->couponRenderer->parseCoupon($couponCodes);
            foreach ($couponCodesArray as $coupon) {
                /** @var Coupon $couponRule */
                $couponRule = $this->couponModel->loadByCode($coupon);
                if ($this->couponResource->exists($coupon) && $rule->getRuleId() == $couponRule->getRuleId()) {
                    $this->discountCollector->applyRuleAmount($coupon, $baseAmount);
                    $discount += $baseAmount;
                    $this->codeKey++;
                }
            }

            $address = $observer->getAddress();
            $availableShippingDiscountAmount = $discount - $address->getSubtotal();

            if ($availableShippingDiscountAmount > 0) {
                $cartRules = $address->getCartFixedRules();
                $cartRules[$rule->getRuleId()] = $availableShippingDiscountAmount;
                $address->setCartFixedRules($cartRules);
            }

            $observer->getData('result')->setAmount($discount);

        } elseif ($observer->getData('rule')->getCouponCode() != null) {
            $key = array_search(
                strtolower($observer->getData('rule')->getCouponCode()),
                array_map(self::STR_TO_LOWER, $appliedCodes)
            );
            if ($key !== false) {
                $this->discountCollector->applyRuleAmount(
                    $appliedCodes[$key],
                    $observer->getData('result')->getAmount()
                );
            }
        } elseif ($observer->getData('rule')->getUseAutoGeneration() && isset($appliedCodes[$this->codeKey])) {
            $this->discountCollector->applyRuleAmount(
                $appliedCodes[$this->codeKey],
                $observer->getData('result')->getAmount()
            );
            $this->codeKey++;
        }

        if ($this->codeKey >= count($appliedCodes)) {
            $this->codeKey = 0;
        }
    }
}
