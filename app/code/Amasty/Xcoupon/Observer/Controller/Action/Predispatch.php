<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Xcoupon
 */


namespace Amasty\Xcoupon\Observer\Controller\Action;

use Magento\Framework\Event\Observer;

class Predispatch implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    private $request;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Magento\Checkout\Model\Session $customerSession
    ) {
        $this->request = $request;
        $this->checkoutSession = $customerSession;
    }

    /**
     * @param Observer $observer
     *
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $coupon = $this->request->getParam('remove') ? "" : $this->request->getParam('coupon');

        if ($coupon === null) {
            $coupon = $this->request->getParam('coupon_code') ?
                $this->request->getParam('coupon_code') :
                $this->checkoutSession->getCoupon();
        }

        if ($coupon !== null) {
            if ($quote = $this->checkoutSession->getQuote()) {
                $this->checkoutSession->setCoupon($coupon);
                $quote->setCouponCode($coupon)
                    ->collectTotals()
                    ->save();
                $quote->setTotalsCollectedFlag(false);
            }
        }
    }
}