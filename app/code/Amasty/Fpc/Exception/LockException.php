<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Fpc
 */


namespace Amasty\Fpc\Exception;

use Magento\Framework\Exception\LocalizedException;

class LockException extends LocalizedException
{
}
