<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Authentication\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Module\Manager;

class TestModule extends \Magento\Framework\App\Action\Action
{
    public $request;
    public $jsonFactory;
    public $storeManager;
    public $moduleManager;
    public $mobileassistantHelper;

    /**
     * @param Context                              $context
     * @param Http                                 $request
     * @param JsonFactory                          $jsonFactory
     * @param StoreManagerInterface                $storeManager
     * @param Manager                              $moduleManager
     * @param \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
     */
    public function __construct(
        Context $context,
        Http $request,
        JsonFactory $jsonFactory,
        StoreManagerInterface $storeManager,
        Manager $moduleManager,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
    ) {
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_moduleManager = $moduleManager;
        $this->_storeManager = $storeManager;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        return parent::__construct($context);
    }
    /**
     * This function is used for the test the magento url is authenticate or not
     * @return Bool
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $postData = $this->_request->getParams();
        if (array_key_exists('magento_url', $postData)) {
            $magnetoUrl = $postData['magento_url'];
        } else {
            $errorArray = [];
            $errorArray['error'] = "Magneto url is missing";
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }
        
        $urlInfo = parse_url($magnetoUrl);

        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant')) {
            $isSecure = $this->_request->isSecure();
            $validateUrl = false;
            if ($isSecure) {
                if ($this->_storeManager->getStore()
                    ->getConfig('web/secure/base_url') == $this->_storeManager->getStore()
                    ->getConfig('web/secure/base_link_url')
                ) {
                    $validateUrl = true;
                }
            } else {
                if ($this->_storeManager->getStore()
                    ->getConfig('web/unsecure/base_url') == $this->_storeManager->getStore()
                    ->getConfig('web/unsecure/base_link_url')) {
                    $validateUrl = true;
                }
            }
            if ($validateUrl) {
                $isIndex = $this->_storeManager->getStore()->getConfig('web/seo/use_rewrites');
                if (!$isIndex && basename($magnetoUrl) != 'index.php') {
                    $errrorArray = [];
                    $errrorArray['error'] = 'Please add "index.php" after your url.';
                    $jsonResult->setData($errrorArray);
                    return $jsonResult;
                }
                $successArray['success'] = 'Hurray! The connection with the Magento
                 Site worked out fine & you can start using the App.';
                $successArray['packageVersion'] = $this->_mobileassistantHelper->getExtensionVersion();
                $jsonResult->setData($successArray);
            } else {
                $magentoArray['error'] = 'There seems some difference between the Based URL 
                & Magento Based URL(on the store). Please check & if issue persists, Contact 
                our Support Team.';
                $jsonResult->setData($magentoArray);
            }
        } else {
            $activateArray['error'] = 'Please activate the Mobile Assistant Extension on the Magento Store.';
            $jsonResult->setData($activateArray);
            return $jsonResult;
        }

        return $jsonResult;
    }
}
