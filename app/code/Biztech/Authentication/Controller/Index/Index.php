<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Authentication\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Request\Http;
use Magento\User\Model\User;
use Magento\Backend\App\ConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Backend\Model\Auth;
use Biztech\Mobileassistant\Helper\Data;
use Magento\Backend\Model\Url;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $moduleManager;
    protected $storeManager;
    protected $request;
    protected $userModel;
    protected $config;
    protected $encryptor;
    protected $jsonFactory;
    protected $eventManager;
    protected $auth;
    protected $mobileassistantHelper;
    protected $backendmodel;
    protected $backendSession;
    protected $authSession;
    protected $aclBuilder;

    /**
     * @param Context                             $context
     * @param Manager                             $moduleManager
     * @param StoreManagerInterface               $storeManager
     * @param Http                                $request
     * @param ConfigInterface                     $configInterface
     * @param User                                $userModel
     * @param EncryptorInterface                  $encryptor
     * @param JsonFactory                         $jsonFactory
     * @param Auth                                $auth
     * @param Data                                $mobileassistantHelper
     * @param Url                                 $backendmodel
     * @param \Magento\Backend\Model\Session      $backendSession
     * @param \Magento\Backend\Model\Auth\Session $authSession
     * @param \Magento\Framework\Acl\Builder      $aclBuilder
     */
    public function __construct(
        Context $context,
        Manager $moduleManager,
        StoreManagerInterface $storeManager,
        Http $request,
        ConfigInterface $configInterface,
        User $userModel,
        EncryptorInterface $encryptor,
        JsonFactory $jsonFactory,
        Auth $auth,
        Data $mobileassistantHelper,
        Url $backendmodel,
        \Magento\Backend\Model\Session $backendSession,
        \Magento\Backend\Model\Auth\Session $authSession,
        \Magento\Framework\Acl\Builder $aclBuilder
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_userModel = $userModel;
        $this->_config = $configInterface;
        $this->_encryptor = $encryptor;
        $this->_jsonFactory = $jsonFactory;
        $this->_eventManager = $context->getEventManager();
        $this->_auth = $auth;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_backendmodel = $backendmodel;
        $this->_backendSession = $backendSession;
        $this->_authSession = $authSession;
        $this->_aclBuilder = $aclBuilder;
        return parent::__construct($context);
    }
    
    /**
     * This function is used for login in the app.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant')) {
            $data = $this->_mobileassistantHelper->getHeaders();
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            
            $isSecure = $this->_request->isSecure();
            $validateUrl = false;
            if ($isSecure) {
                if ($this->_storeManager->getStore()->getConfig('web/secure/base_url') == $this->_storeManager->getStore()->getConfig('web/secure/base_link_url')) {
                    $validateUrl = true;
                }
            } else {
                if ($this->_storeManager->getStore()->getConfig('web/unsecure/base_url') == $this->_storeManager->getStore()->getConfig('web/unsecure/base_link_url')) {
                    $validateUrl = true;
                }
            }

            if ($validateUrl) {
                $postData = $this->_request->getParams();
                $username = $postData['userapi'];
                $password = $postData['keyapi'];
                $deviceToken = $postData['token'];
                $notificationFlag = $postData['notification_flag'];
                $deviceType = $postData['device_type'];

                $config = $this->_config->isSetFlag('admin/security/use_case_sensitive_login');
                $adminSession = $this->_backendSession;
                $adminSession->isValidForPath("adminhtml");

                $user = $this->_userModel;
                $user->loadByUsername($username);
                $sensitive = $config ? $username == $user->getUsername() : true;

                if ($sensitive && $user->getId() && $this->_encryptor->validateHash($password, $user->getPassword())) {
                    if ($user->getIsActive() != '1') {
                        $inactiveArray = array('error' => 'This account is inactive.');
                        $jsonResult->setData($inactiveArray);
                    }
                    if (!$user->hasAssigned2Role($user->getId())) {
                        $accessArray = array('error' => 'Access denied.');
                        $jsonResult->setData($accessArray);
                    }
                } else {
                    $invalidArray = array('error' => 'Invalid User Name or Password.');
                    $jsonResult->setData($invalidArray);
                    return $jsonResult;
                }

                if ($this->_backendmodel->useSecretKey()) {
                    $this->_backendmodel->renewSecretUrls();
                }

                $sessionManager = $this->_authSession;
                $sessionManager->setIsFirstVisit(true);
                $sessionManager->setUser($user);
                $aclBuilder = $this->_aclBuilder;
                $sessionManager->setAcl($aclBuilder->getAcl());


                $sessionId = $user->getId() . '_' . md5($username);
                $eventArray = array('user' => $this->_auth->getCredentialStorage());
                $this->_eventManager->dispatch(
                    'backend_auth_user_login_success',
                    $eventArray
                );

                if ($sessionId) {
                    $data = array('username' => $username, 'password' => $user->getPassword(), 'devicetoken' => $deviceToken, 'session_id' => $sessionId, 'notification_flag' => $notificationFlag, 'device_type' => $deviceType, 'is_logout' => 0);

                    $jsonResult = $this->_mobileassistantHelper->create($data);
                    return $jsonResult;
                }
            } else {
                $magentoUrlArray = array('error' => 'There seems some difference between the Based URL & Magento Based URL(on the store). Please check & if issue persists, Contact our Support Team.');
                $jsonResult->setData($magentoUrlArray);
            }
        } else {
            $connectionArray = array('error' => 'Please activate the Mobile Assistant Extension on the Magento Store.');
            $jsonResult->setData($connectionArray);
            return $jsonResult;
        }

        return $jsonResult;
    }
}
