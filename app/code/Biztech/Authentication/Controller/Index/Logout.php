<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Authentication\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;

class Logout extends \Magento\Framework\App\Action\Action
{
    protected $request;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    protected $mobileassistantModel;

    /**
     * @param Context                                        $context
     * @param Http                                           $request
     * @param JsonFactory                                    $jsonFactory
     * @param \Biztech\Mobileassistant\Helper\Data           $helper
     * @param \Biztech\Mobileassistant\Model\Mobileassistant $mobileassistantModel
     */
    public function __construct(
        Context $context,
        Http $request,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        \Biztech\Mobileassistant\Model\Mobileassistant $mobileassistantModel
    ) {
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_mobileassistantModel = $mobileassistantModel;
        return parent::__construct($context);
    }
    
    /**
     * This function is used for the logout from the app.
     * @return Bool
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();

        if (!$this->_mobileassistantHelper->getHeaders()) {
            $errorResult = array('error' => 'Please make sure your app is proper authenticated');
            $jsonResult->setData($errorResult);
            return $jsonResult;
        }

        $postData = $this->_request->getParams();
        if (array_key_exists('userapi', $postData)) {
            $userApi = $postData['userapi'];
        } else {
            $errorArray = array('error' => "userapi key is missing");
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }
        if (array_key_exists('token', $postData)) {
            $deviceToken = $postData['token'];
        } else {
            $errorArray = array('error' => "token key is missing");
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }

        $deviceCollections = $this->_mobileassistantModel->getCollection()
        ->addFieldToFilter('device_token', array('eq' => $deviceToken));
        $deviceCollectionCount = count($deviceCollections);

        if ($deviceCollectionCount < 1) {
            $errorArray = array('error' => 'No match found for this device_token.');
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }

        foreach ($deviceCollections as $deviceDetail) {
            $userId = $deviceDetail->getUserId();
            try {
                $model = $this->_mobileassistantModel;
                $model->load($userId)
                        ->setIsLogout(1)
                        ->save();
            } catch (\Exception $e) {
                return $e->getMessage();
            }
        }

        $sucessArray = array('success_msg' => 'User logout successfully.');
        $jsonResult->setData($sucessArray);
        return $jsonResult;
    }
}
