<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Authentication\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;

class ChangeSettings extends \Magento\Framework\App\Action\Action
{
    private $request;
    private $jsonFactory;
    private $mobileassistantModel;

    /**
     * @param Context                                        $context
     * @param Http                                           $request
     * @param JsonFactory                                    $jsonFactory
     * @param \Biztech\Mobileassistant\Model\Mobileassistant $mobileassistantModel
     */
    public function __construct(
        Context $context,
        Http $request,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Model\Mobileassistant $mobileassistantModel
    ) {
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantModel = $mobileassistantModel;
        return parent::__construct($context);
    }

    /**
     * Change setting in app for configuration
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $postData = $this->_request->getParams();

        if (array_key_exists('userapi', $postData)) {
            $userApi = $postData['userapi'];
        } else {
            $errorArray = array('error' => "userapi key is missing");
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }
        if (array_key_exists('token', $postData)) {
            $deviceToken = $postData['token'];
        } else {
            $errorArray = array('error' => "token key is missing");
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }
        if (array_key_exists('notification_flag', $postData)) {
            $notificationFlag = $postData['notification_flag'];
        } else {
            $errorArray = array('error' => "notification_flag key is missing");
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }
        $deviceCollections = $this->_mobileassistantModel->getCollection()
        ->addFieldToFilter('username', array('eq' => $userApi))
        ->addFieldToFilter('device_token', array('eq' => $deviceToken));
        $deviceCollectionCount = count($deviceCollections);
        if ($deviceCollectionCount < 1) {
            $errorArray = array('error' => 'No match found for this userapi and token');
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }
        foreach ($deviceCollections as $deviceDetail) {
            $userId = $deviceDetail->getUserId();
        }

        if ($deviceCollectionCount == 1) {
            try {
                $model = $this->_mobileassistantModel;
                $model->load($userId)
                        ->setNotificationFlag($notificationFlag)
                        ->save();

                $sucessArray = array('success_msg' => 'Settings updated sucessfully.');
                $jsonResult->setData($sucessArray);
                return $jsonResult;
            } catch (\Exception $e) {
                $errorArray = array('error' => $e->getMessage());
                $jsonResult->setData($sucessArray);
                return $jsonResult;
            }
        }
    }
}
