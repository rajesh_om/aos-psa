<?php

namespace Biztech\Mobileassistant\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface {
    
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('mobileassistant');
        
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            
            $table = $installer->getConnection()
                    ->newTable($tableName)
                    ->addColumn(
                        'user_id', Table::TYPE_INTEGER, null, array(
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                        ), 'User Id'
                    )
                    ->addColumn(
                            'username', Table::TYPE_TEXT, 255, array('nullable' => false, 'default' => ''), 'Username'
                    )
                    ->addColumn(
                            'apikey', Table::TYPE_TEXT, 50, array('nullable' => false, 'default' => ''), 'Apikey'
                    )
                    ->addColumn(
                            'device_token', Table::TYPE_TEXT, 255, array('nullable' => false, 'default' => ''), 'Device Token'
                    )
                    ->addColumn(
                            'notification_flag', Table::TYPE_SMALLINT, 11, array('nullable' => false, 'default' => '1'), 'Notification Flag'
                    )
                    ->addColumn(
                            'device_type', Table::TYPE_TEXT, 255, array('nullable' => false, 'default' => ''), 'Device Type'
                    )
                    ->addColumn(
                            'is_logout', Table::TYPE_SMALLINT, 11, array('nullable' => false, 'default' => '0'), 'Is Logout'
                    )
                    ->addColumn(
                            'password', Table::TYPE_TEXT, 255, array('nullable' => false, 'default' => ''), 'Password'
                    )
                    ->setComment('News Table')
                    ->setOption('type', 'InnoDB')
                    ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }

}
