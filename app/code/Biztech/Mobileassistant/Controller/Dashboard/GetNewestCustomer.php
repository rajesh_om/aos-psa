<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Dashboard;

use Magento\Directory\Helper\Data;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Locale\CurrencyInterface;

class GetNewestCustomer extends \Magento\Framework\App\Action\Action
{
    protected $directoryHelper;
    protected $moduleManager;
    public $request;
    protected $jsonFactory;
    protected $localeCurrency;
    protected $mobileassistantHelper;
    protected $customerReportCollection;

    /**
     * @param Context                                                  $context
     * @param PageFactory                                              $pageFactory
     * @param Data                                                     $directoryHelper
     * @param Manager                                                  $moduleManager
     * @param Http                                                     $request
     * @param JsonFactory                                              $jsonFactory
     * @param CurrencyInterface                                        $localeCurrency
     * @param \Biztech\Mobileassistant\Helper\Data                     $mobileassistantHelper
     * @param \Magento\Reports\Model\ResourceModel\Customer\Collection $customerReportCollection
     */
    public function __construct(
        Context $context,
        Data $directoryHelper,
        Manager $moduleManager,
        Http $request,
        JsonFactory $jsonFactory,
        CurrencyInterface $localeCurrency,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        \Magento\Reports\Model\ResourceModel\Customer\Collection $customerReportCollection
    ) {
        $this->_directoryHelper = $directoryHelper;
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_localeCurrency = $localeCurrency;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->customerReportCollection = $customerReportCollection;
        return parent::__construct($context);
    }
    /**
     * This function is used for get newest customer list.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();
            $sessionId = '';
            $storeId = '';
            if (array_key_exists('session', $postData)) {
                $sessionId = $postData['session'];
            } else {
                $errorArray = array('error' => "Session key is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }

            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }

            if (array_key_exists('storeid', $postData)) {
                $storeId = $postData['storeid'];
            } else {
                $storeId = 1;
            }

            $baseCurrencyCode = $this->_directoryHelper->getBaseCurrencyCode();
            $collection = $this->_customerReportCollection;
            $collection->addCustomerName();
            $storeFilter = 0;
            if ($storeId) {
                $collection->addAttributeToFilter('store_id', $storeId);
                $storeFilter = 1;
            }
            $collection->addOrdersStatistics($storeFilter)->orderByCustomerRegistration();
            $newestCustomer = array();
            foreach ($collection as $collection) {
                $newestCustomer[] = array(
                    'name' => $collection->getName(),
                    'email' => $collection->getEmail(),
                    'orders_count' => $collection->getOrdersCount(),
                    'average_order_amount' => $this->_localeCurrency->getCurrency($baseCurrencyCode)->toCurrency($this->_mobileassistantHelper->getPriceFormat($collection->getOrdersAvgAmount())),
                    'total_order_amount' => $this->_localeCurrency->getCurrency($baseCurrencyCode)->toCurrency($this->_mobileassistantHelper->getPriceFormat($collection->getOrdersSumAmount()))
                );
            }

            $jsonResult->setData($newestCustomer);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
