<?php

namespace Biztech\Mobileassistant\Controller\Dashboard;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\Timezone;
use Magento\Reports\Model\ResourceModel\Order\Collection;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Directory\Helper\Data;
use Magento\Framework\Locale\CurrencyInterface;

class Dashboard extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;
    protected $moduleManager;
    public $request;
    public $date;
    public $timezone;
    public $reportCollection;
    protected $jsonFactory;
    protected $directoryHelper;
    protected $localeCurrency;
    protected $mobileassistantHelper;
    protected $storeManagerInterface;
    protected $dirCurrency;
    protected $orderResourceCollection;
    protected $objectManager;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Manager $moduleManager,
        Http $request,
        DateTime $date,
        Timezone $timezone,
        Collection $reportCollection,
        JsonFactory $jsonFactory,
        Data $directoryHelper,
        CurrencyInterface $localeCurrency,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Directory\Model\Currency $dirCurrency,
        \Magento\Sales\Model\ResourceModel\Order\Collection $orderResourceCollection,
        \Magento\Framework\ObjectManagerInterface $objectManager
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_date = $date;
        $this->_timezone = $timezone;
        $this->_reportCollection = $reportCollection;
        $this->_jsonFactory = $jsonFactory;
        $this->_directoryHelper = $directoryHelper;
        $this->_localeCurrency = $localeCurrency;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_dirCurrency = $dirCurrency;
        $this->_orderResourceCollection = $orderResourceCollection;
        $this->_objectManager = $objectManager;
        return parent::__construct($context);
    }
    /**
     * [execute use for the get the dashboard details]
     * @return [type] [description]
     */
    public function execute()
    {
        $result = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $result->setData($errorResult);
                return $result;
            }
            $postData = $this->_request->getParams();
            $sessionId = '';
            $storeId = '';
            $typeId = '';
            if (isset($postData['session'])) {
                $sessionId = $postData['session'];
            }

            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $result->setData($sessionExpire);
                return $result;
            }

            if (isset($postData['storeid'])) {
                $storeId = $postData['storeid'];
            }
            if (isset($postData['days_for_dashboard'])) {
                $typeId = $postData['days_for_dashboard'];
            }

            $storeManager = $this->_storeManagerInterface;
            $currency = $this->_dirCurrency;
            $store = $storeManager->getStore();
            $currentCurrencySymbol = $currency->getCurrencySymbol();
            $currentCurrencyCode = $store->getCurrentCurrencyCode();
            if ($currentCurrencySymbol == "") {
                $currentCurrencySymbol = $currentCurrencyCode;
            }
            $now = $this->_date->timestamp();
            $endDate = date('Y-m-d 23:59:59', $now);
            $startDate = '';

            $orderCollection = $this->_orderResourceCollection;
            $orderCollection->addFieldToFilter('store_id', array('eq' => $storeId))->addFieldToFilter('status', array('in' => array('complete', 'processing')))->setOrder('entity_id', 'desc');

            if ($typeId == 7) {
                $startDate = date('Y-m-d 00:00:00', strtotime('-6 days'));
            } elseif ($typeId == 30) {
                $startDate = date('Y-m-d 00:00:00', strtotime('-29 days'));
            } elseif ($typeId == 90) {
                $startDate = date('Y-m-d 00:00:00', strtotime('-89 days'));
            } elseif ($typeId == 24) {
                $endDate = date("Y-m-d H:m:s");
                $startDate = date("Y-m-d H:m:s", strtotime('-24 hours', time()));

                $timezoneLocal = $this->_timezone->getConfigTimezone();

                list($dateStart, $dateEnd) = $this->_reportCollection->getDateRange('24h', '', '', true);

                $dateStart->setTimezone(new \DateTimeZone($timezoneLocal));
                $dateEnd->setTimezone(new \DateTimeZone($timezoneLocal));
                $dateEnd->modify('-1 hour');

                $dates = array();

                while ($dateStart <= $dateEnd) {
                    $d = $dateStart->format('Y-m-d H:i:s');
                    $dateStart->modify('+1 hour');
                    $dates[] = $d;
                }
                $startDate = $dates[0];
                $endDate = $dates[count($dates) - 1];

                $orderCollection->addAttributeToFilter('created_at', array('from' => $startDate, 'to' => $endDate));
                $totalCount = count($orderCollection);
            }

            if ($typeId != 'year') {
                if ($typeId == 'month') {
                    $endDate = date("Y-m-d H:m:s");
                    $startDate = date('Y-m-01 H:m:s');
                }

                if ($typeId != 24) {
                    $orderCollection->addAttributeToFilter('created_at', array('from' => $startDate, 'to' => $endDate));
                    $totalCount = count($orderCollection);
                    $dates = $this->GetDatesFromRange($startDate, $endDate);
                }
                $count = 0;
                foreach ($dates as $date) {
                    if ($typeId == 24) {
                        if ($count == 23) {
                            continue;
                        }
                        $dateStart = $dates[$count];
                        $dateEnd = $dates[$count + 1];
                    } else {
                        $dateStart = date('Y-m-d 00:00:00', strtotime($date));
                        $dateEnd = date('Y-m-d 23:59:59', strtotime($date));
                    }

                    // $orderCollectionByDate = $this->orderResourceCollection;
                    $orderCollectionByDate = $this->_objectManager->create('Magento\Sales\Model\ResourceModel\Order\Collection');
                    $orderCollectionByDate->addFieldToFilter('store_id', array('eq' => $storeId))->addFieldToFilter('status', array('in' => array('complete', 'processing')))->setOrder('entity_id', 'desc');

                    $orderCollectionByDate->addAttributeToFilter('created_at', array('from' => $dateStart, 'to' => $dateEnd));
                    $orderCollectionByDate->getSelect()->columns('SUM(grand_total) AS grand_total_sum');
                    $orderCollectionByDate->getSelect()->group(array('store_id'));
                    $orderData = $orderCollectionByDate->getData();

                    if (count($orderData) == 0) {
                        if ($typeId == 24) {
                            $orderTotalByDate[date("Y-m-d H:i", strtotime($date))] = 0;
                        } elseif ($typeId == 'month') {
                            $orderTotalByDate[date('d', strtotime($date))] = 0;
                        } else {
                            $orderTotalByDate[$date] = 0;
                        }
                    } else {
                        if ($typeId == 24) {
                            $ordersByDate[date("Y-m-d H:i", strtotime($date))][] = $orderData[0]['grand_total_sum'];
                            $orderTotalByDate[date("Y-m-d H:i", strtotime($date))] = array_sum($ordersByDate[date("Y-m-d H:i", strtotime($date))]);
                        } elseif ($typeId == 'month') {
                            $ordersByDate[date('d', strtotime($date))][] = $orderData[0]['grand_total_sum'];
                            $orderTotalByDate[date('d', strtotime($date))] = array_sum($ordersByDate[date('d', strtotime($date))]);
                        } else {
                            $ordersByDate[$date][] = $orderData[0]['grand_total_sum'];
                            $orderTotalByDate[$date] = array_sum($ordersByDate[$date]);
                        }
                    }
                    $count++;
                }
            } else {
                $endDate = date('Y-m-d');
                $startDate = date('Y-01-01');
                $orderCollection->addAttributeToFilter('created_at', array('from' => $startDate, 'to' => $endDate));
                $totalCount = count($orderCollection);
                $months = $this->GetMonths($startDate, $endDate);
                $currentYear = date("Y");
                foreach ($months as $month) {
                    $FirstDay = $this->FirstDay($month, $currentYear);
                    $ordersByDate = array();
                    if ($month == date('F')) {
                        $lastDay = date('Y-m-d');
                    } else {
                        $lastDay = $this->LastDay($month, $currentYear);
                    }

                    $dates = $this->GetDatesFromRange($FirstDay, $lastDay);

                    foreach ($dates as $date) {
                        $dateStart = date('Y-m-d 00:00:00', strtotime($date));
                        $dateEnd = date('Y-m-d 23:59:59', strtotime($date));

                        // $orderCollectionByDate = $this->orderResourceCollection;
                        $orderCollectionByDate = $this->_objectManager->create('Magento\Sales\Model\ResourceModel\Order\Collection');
                        $orderCollectionByDate->addFieldToFilter('store_id', array('eq' => $storeId))->addFieldToFilter('status', array('in' => array('complete', 'processing')))->setOrder('entity_id', 'desc');

                        $orderCollectionByDate->addAttributeToFilter('created_at', array('from' => $dateStart, 'to' => $dateEnd));
                        $orderCollectionByDate->getSelect()->columns('SUM(grand_total) AS grand_total_sum');
                        $orderCollectionByDate->getSelect()->group(array('store_id'));
                        $orderData = $orderCollectionByDate->getData();
                        if (count($orderData) != 0) {
                            $ordersByDate[] = $orderData[0]['grand_total_sum'];
                        }
                    }

                    $orderTotalByDate[$month] = array_sum($ordersByDate);
                }
            }

            $baseCurrencyCode = $this->_directoryHelper->getBaseCurrencyCode();

            $orderGrandTotal = strip_tags(round(array_sum($orderTotalByDate), 2));

            $lifeTimeSales = strip_tags(round($this->_reportCollection->calculateSales()->load()->getFirstItem()->getLifetime(), 2));
            $averageOrder = strip_tags(round($this->_reportCollection->calculateSales()->load()->getFirstItem()->getAverage(), 2));

            if ($typeId == 7) {
                $avgOrderCount = $totalCount / 7;
                $avgOrderGrandTotal = (array_sum($orderTotalByDate)) / 7;
            } elseif ($typeId == 30) {
                $avgOrderCount = $totalCount / 30;
                $avgOrderGrandTotal = (array_sum($orderTotalByDate)) / 30;
            } elseif ($typeId == 90) {
                $avgOrderCount = $totalCount / 90;
                $avgOrderGrandTotal = (array_sum($orderTotalByDate)) / 90;
            } elseif ($typeId == 24) {
                $avgOrderCount = $totalCount / 24;
                $avgOrderGrandTotal = (array_sum($orderTotalByDate)) / 24;
            } elseif ($typeId == 'month') {
                $avgOrderCount = $totalCount / count($dates);
                $avgOrderGrandTotal = (array_sum($orderTotalByDate)) / count($dates);
            } elseif ($typeId == 'year') {
                $avgOrderCount = $totalCount / count($months);
                $avgOrderGrandTotal = (array_sum($orderTotalByDate)) / count($months);
            }

            $avgOrderCount = number_format($avgOrderCount, 2, '.', ',');
            $avgOrderGrandTotal = strip_tags(round($avgOrderGrandTotal, 2));

            $orderTotalResultArr = array('dashboard_result' => array('ordertotalbydate' => $orderTotalByDate, 'ordergrandtotal' => $currentCurrencySymbol.$this->_mobileassistantHelper->getPriceFormat($orderGrandTotal), 'totalordercount' => $totalCount, 'lifetimesales' => $currentCurrencySymbol.$this->_mobileassistantHelper->getPriceFormat($lifeTimeSales), 'averageorder' => $currentCurrencySymbol.$this->_mobileassistantHelper->getPriceFormat($averageOrder), 'avg_order_count' => $this->_mobileassistantHelper->getPriceFormat($avgOrderCount), 'avg_order_grandtotal' => $currentCurrencySymbol.$this->_mobileassistantHelper->getPriceFormat($avgOrderGrandTotal)));
            $orderTotalResultArr['packageVersion'] = $this->_mobileassistantHelper->getExtensionVersion();
            $result->setData($orderTotalResultArr);
            return $result;
        } else {
            $returnExtensionArray = array('enable' => false);
            $result->setData($returnExtensionArray);
            return $result;
        }
    }

    /**
     * [GetDatesFromRange use for the get the range of the between dates]
     * @param [type] $startDate [start date]
     * @param [type] $endDate   [end date]
     */
    
    public function GetDatesFromRange($startDate, $endDate)
    {
        $dateFrom = strtotime(date('Y-m-d', strtotime($startDate)));
        $dateTo = strtotime(date('Y-m-d', strtotime($endDate)));

        for ($i = $dateFrom; $i <= $dateTo; $i+=86400) {
            $dates[] = date("Y-m-d", $i);
        }
        return $dates;
    }

    /**
     * [GetMonths use for the get the months based on the given dates]
     * @param [type] $date1 [date1]
     * @param [type] $date2 [date2]
     */
    
    public function GetMonths($date1, $date2)
    {
        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        $my = date('mY', $time2);
        $months = array();
        $f = '';

        while ($time1 < $time2) {
            $time1 = strtotime((date('Y-m-d', $time1) . ' +15days'));

            if (date('m', $time1) != $f) {
                $f = date('m', $time1);

                if (date('mY', $time1) != $my && ($time1 < $time2)) {
                    $months[] = date('m', $time1);
                }
            }
        }

        $months[] = date('m', $time2);
        return $months;
    }

    /**
     * [FirstDay use for the get the first day of the month or year]
     * @param string $month [month]
     * @param string $year  [year]
     */
    public function FirstDay($month = '', $year = '')
    {
        if (empty($month)) {
            $month = date('m');
        }
        if (empty($year)) {
            $year = date('Y');
        }
        $result = strtotime("{$year}-{$month}-01");
        return date('Y-m-d', $result);
    }

    /**
     * [LastDay use for the get the last day of the month or year]
     * @param string $month [month]
     * @param string $year  [year]
     */
    
    public function LastDay($month = '', $year = '')
    {
        if (empty($month)) {
            $month = date('m');
        }
        if (empty($year)) {
            $year = date('Y');
        }
        $result = strtotime("{$year}-{$month}-01");
        $result = strtotime('-1 day', strtotime('+1 month', $result));
        return date('Y-m-d', $result);
    }
}
