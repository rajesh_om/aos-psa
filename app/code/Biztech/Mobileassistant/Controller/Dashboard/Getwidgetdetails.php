<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Dashboard;

use Magento\Directory\Helper\Data;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\Timezone;
use Magento\Reports\Model\ResourceModel\Order\Collection;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Locale\CurrencyInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManager;
use Magento\Catalog\Helper\Image;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class Getwidgetdetails extends \Magento\Framework\App\Action\Action
{
    protected $directoryHelper;
    protected $moduleManager;
    public $request;
    public $date;
    public $timezone;
    public $reportCollection;
    protected $jsonFactory;
    protected $localeCurrency;
    protected $mobileassistantHelper;
    protected $productFactory;
    protected $storeManager;
    protected $imageHelper;
    protected $orderResourceCollection;
    protected $orderModel;
    protected $customerReportcollection;
    protected $stockRegistry;

    public function __construct(
        Context $context,
        Data $directoryHelper,
        Manager $moduleManager,
        Http $request,
        DateTime $date,
        Timezone $timezone,
        Collection $reportCollection,
        JsonFactory $jsonFactory,
        CurrencyInterface $localeCurrency,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        ProductFactory $productFactory,
        StoreManager $storeManager,
        Image $imageHelper,
        \Magento\Sales\Model\ResourceModel\Order\Collection $orderResourceCollection,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Reports\Model\ResourceModel\Customer\Collection $customerReportcollection,
        StockRegistryInterface $stockRegistry
    ) {
        $this->_directoryHelper = $directoryHelper;
        $this->_moduleManager = $moduleManager;
        $this->_request = $Request;
        $this->_date = $date;
        $this->_timezone = $timezone;
        $this->_reportCollection = $reportCollection;
        $this->_jsonFactory = $jsonFactory;
        $this->localeCurrency = $localeCurrency;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_productFactory = $productFactory;
        $this->_storeManager = $storeManager;
        $this->_imageHelper = $imageHelper;
        $this->_orderResourceCollection = $orderResourceCollection;
        $this->_orderModel = $orderModel;
        $this->_stockRegistry = $stockRegistry;
        return parent::__construct($context);
    }
    /**
     * [execute use for the get the widget details]
     * @return [type] [description]
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();

            $sessionId = '';
            $storeId = '';
            $typeId = '';
            $tab = '';
            $limit = '';
            $newestCustomer = array();
            if (isset($postData['session'])) {
                $sessionId = $postData['session'];
            }
            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }

            if (isset($postData['storeid'])) {
                $storeId = $postData['storeid'];
            }
            if (isset($postData['days_for_dashboard'])) {
                $typeId = $postData['days_for_dashboard'];
            }
            if (isset($postData['tab'])) {
                $tab = $postData['tab'];
            }
            if (isset($postData['limit'])) {
                $limit = $postData['limit'];
            }
            $source = 'widget';
            $baseCurrencyCode = $this->_directoryHelper->getBaseCurrencyCode();

            /* order detail */
            $orderDetails = $this->GetOrderdetails($storeId, $typeId, $tab, $source, $limit);
            $orderTotalByDate = $orderDetails['ordertotalbydate'];
            $orderGrandTotal = strip_tags($this->localeCurrency->getCurrency($baseCurrencyCode)->toCurrency(array_sum($orderTotalByDate)));

            $widgetResultArr['widget_order'] = array('ordergrandtotal' => $orderGrandTotal, 'totalordercount' => $orderDetails['ordertotalcount'], 'ordercollection' => null);
            if ($tab == 'order') {
                $widgetResultArr['widget_order']['ordercollection'] = $orderDetails['ordercollection'];
            }

            /* customer detail */
            $customerDetails = $this->GetCustomerDetails($storeId, $typeId, $limit, $tab);
            $customertotal = $customerDetails['customertotal'];

            if ($tab == 'customer') {
                $collection = $customerDetails['customercollection'];

                foreach ($collection as $collection) {
                    $newestCustomer[] = array(
                        'entity_id' => $collection->getEntityId(),
                        'name' => $collection->getName(),
                        'email' => $collection->getEmail(),
                    );
                }
            }

            $widgetResultArr['widget_customer'] = array('totalcustomer' => $customertotal, 'customers_detail' => $newestCustomer);

            /* products detail */
            $productDetails = $this->GetLowStockProducts($tab, $storeId, $limit);
            $totalProduct = $productDetails['totalproducts'];
            $productData = $productDetails['productlistdata'];

            $widgetResultArr['widget_product'] = array('totalproduct' => $totalProduct, 'product_detail' => $productData);

            $jsonResult->setData($widgetResultArr);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }

    /**
     * [GetOrderdetails use for the get the order details]
     * @param [type] $storeId [store id]
     * @param [type] $typeId  [type id]
     * @param [type] $tab     [tab for mobile]
     * @param [type] $source  [source]
     * @param [type] $limit   [limit]
     */
    protected function GetOrderdetails($storeId, $typeId, $tab = null, $source = null, $limit = null)
    {
        $orderTotalByDate = array();
        $now = $this->_date->timestamp();
        $endDate = date('Y-m-d 23:59:59', $now);
        $startDate = '';
        $orderListData = array();

        
        if ($source == 'widget') {
            $orderCollection = $this->_orderResourceCollection;
            $orderCollection->addFieldToFilter('store_id', array('eq' => $storeId))->setOrder('entity_id', 'desc');
        } else {
            $orderCollection = $this->_orderResourceCollection;
            $orderCollection->addFieldToFilter('store_id', array('eq' => $storeId))->addFieldToFilter('status', array('eq' => array('complete')))->setOrder('entity_id', 'desc');
        }

        if ($typeId == 7) {
            $startDate = date('Y-m-d 00:00:00', strtotime('-6 days'));
        } elseif ($typeId == 30) {
            $startDate = date('Y-m-d 00:00:00', strtotime('-29 days'));
        } elseif ($typeId == 90) {
            $startDate = date('Y-m-d 00:00:00', strtotime('-89 days'));
        } elseif ($typeId == 24) {
            $endDate = date("Y-m-d H:m:s");
            $startDate = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
            $timezoneLocal = $this->_timezone->getConfigTimezone();

            list($dateStart, $dateEnd) = $this->_reportCollection->getDateRange('24h', '', '', true);

            $dateStart->setTimezone(new \DateTimeZone($timezoneLocal));
            $dateEnd->setTimezone(new \DateTimeZone($timezoneLocal));

            $dates = array();

            while ($dateStart <= $dateEnd) {
                $d = $dateStart->format('Y-m-d H:i:s');
                $dateStart->modify('+1 hour');
                $dates[] = $d;
            }

            $startDate = $dates[0];
            $endDate = $dates[count($dates) - 1];

            $orderCollection->addAttributeToFilter('created_at', array('from' => $startDate, 'to' => $endDate));
            $totalCount = count($orderCollection);
        }

        if ($typeId != 'year') {
            if ($typeId == 'month') {
                $endDate = date("Y-m-d H:m:s");
                $startDate = date('Y-m-01 H:m:s');
            }

            if ($typeId != 24) {
                $orderCollection->addAttributeToFilter('created_at', array('from' => $startDate, 'to' => $endDate));
                $totalCount = count($orderCollection);
                $dates = $this->GetDatesFromRange($startDate, $endDate);
            }
            $count = 0;
            foreach ($dates as $date) {
                if ($typeId == 24) {
                    if ($count == 23) {
                        continue;
                    }
                    $dateStart = $dates[$count];
                    $dateEnd = $dates[$count + 1];
                } else {
                    $dateStart = date('Y-m-d 00:00:00', strtotime($date));
                    $dateEnd = date('Y-m-d 23:59:59', strtotime($date));
                }

                if ($source == 'widget') {
                    $orderCollectionByDate = $this->orderResourceCollection;
                    $orderCollectionByDate->addFieldToFilter('store_id', array('eq' => $storeId))->setOrder('entity_id', 'desc');
                } else {
                    $orderCollectionByDate = $this->orderResourceCollection;
                    $orderCollectionByDate->addFieldToFilter('store_id', array('eq' => $storeId))->addFieldToFilter('status', array('eq' => array('complete')))->setOrder('entity_id', 'desc');
                }

                $orderCollectionByDate->addAttributeToFilter('created_at', array('from' => $dateStart, 'to' => $dateEnd));
                $orderCollectionByDate->getSelect()->columns('SUM(grand_total) AS grand_total_sum');
                $orderCollectionByDate->getSelect()->group(array('store_id'));
                $orderdata = $orderCollectionByDate->getData();
                if (count($orderdata) == 0) {
                    if ($typeId == 24) {
                        $orderTotalByDate[date("Y-m-d H:i", strtotime($date))] = 0;
                    } elseif ($typeId == 'month') {
                        $orderTotalByDate[date('d', strtotime($date))] = 0;
                    } else {
                        $orderTotalByDate[$date] = 0;
                    }
                } else {
                    if ($typeId == 24) {
                        $ordersByDate[date("Y-m-d H:i", strtotime($date))][] = $orderdata[0]['grand_total_sum'];
                        $orderTotalByDate[date("Y-m-d H:i", strtotime($date))] = array_sum($ordersByDate[date("Y-m-d H:i", strtotime($date))]);
                    } elseif ($typeId == 'month') {
                        $ordersByDate[date('d', strtotime($date))][] = $orderdata[0]['grand_total_sum'];
                        $orderTotalByDate[date('d', strtotime($date))] = array_sum($ordersByDate[date('d', strtotime($date))]);
                    } else {
                        $ordersByDate[$date][] = $orderdata[0]['grand_total_sum'];
                        $orderTotalByDate[$date] = array_sum($ordersByDate[$date]);
                    }
                }

                $count++;
            }
        } else {
            $endDate = date('Y-m-d');
            $startDate = date('Y-01-01');
            $orderCollection->addAttributeToFilter('created_at', array('from' => $startDate, 'to' => $endDate));
            $totalCount = count($orderCollection);
            $months = $this->GetMonths($startDate, $endDate);
            $currentYear = date("Y");
            foreach ($months as $month) {
                $FirstDay = $this->FirstDay($month, $currentYear);
                $ordersByDate = array();
                if ($month == date('F')) {
                    $lastDay = date('Y-m-d');
                } else {
                    $lastDay = $this->Lastday($month, $currentYear);
                }

                $dates = $this->GetDatesFromRange($FirstDay, $lastDay);

                foreach ($dates as $date) {
                    $dateStart = date('Y-m-d 00:00:00', strtotime($date));
                    $dateEnd = date('Y-m-d 23:59:59', strtotime($date));

                    if ($source == 'widget') {
                        $orderCollectionByDate = $this->_orderResourceCollection;
                        $orderCollectionByDate->addFieldToFilter('store_id', array('eq' => $storeId))->setOrder('entity_id', 'desc');
                    } else {
                        $orderCollectionByDate = $this->_orderResourceCollection;
                        $orderCollectionByDate->addFieldToFilter('store_id', array('eq' => $storeId))->addFieldToFilter('status', array('eq' => array('complete')))->setOrder('entity_id', 'desc');
                    }

                    $orderCollectionByDate->addAttributeToFilter('created_at', array('from' => $dateStart, 'to' => $dateEnd));
                    $orderCollectionByDate->getSelect()->columns('SUM(grand_total) AS grand_total_sum');
                    $orderCollectionByDate->getSelect()->group(array('store_id'));
                    $orderdata = $orderCollectionByDate->getData();
                    if (count($orderdata) != 0) {
                        $ordersByDate[] = $orderdata[0]['grand_total_sum'];
                    }
                }

                $orderTotalByDate[$month] = array_sum($ordersByDate);
            }
        }


        $jsonResult = array('ordertotalbydate' => $orderTotalByDate, 'ordertotalcount' => $totalCount);

        $orderCollection->getSelect()->limit($limit);

        if ($tab == 'order') {
            foreach ($orderCollection->getData() as $order) {
                $grandTotal = $this->Helper->getPrice($order['grand_total'], $order['store_id'], $order['order_currency_code']);

                $orderListData[] = array(
                    'entity_id' => $order['entity_id'],
                    'increment_id' => $order['increment_id'],
                    'store_id' => $order['store_id'],
                    'customer_name' => $order['customer_firstname'] . ' ' . $order['customer_lastname'],
                    'status' => $order['status'],
                    'order_date' => $order['created_at'],
                    'grand_total' => $grandTotal,
                    'toal_qty' => $this->orderModel->load($order['entity_id'])->getTotalQtyOrdered(),
                    'total_items' => $order['total_item_count']
                );
            }

            $jsonResult['ordercollection'] = $orderListData;
        }

        return $jsonResult;
    }

    /**
     * [GetCustomerDetails use for the get the customer details]
     * @param [type] $storeId [store id]
     * @param [type] $typeId  [type id]
     * @param [type] $limit   [limit]
     * @param [type] $tab     [tab for the mobile]
     */
    protected function GetCustomerDetails($storeId, $typeId, $limit = null, $tab = null)
    {
        $collection = $this->_customerReportcollection;
        $collection->addCustomerName();
        $storeFilter = 0;
        if ($storeId) {
            $collection->addAttributeToFilter('store_id', $storeId);
            $storeFilter = 1;
        }
        $collection->addOrdersStatistics($storeFilter)->orderByCustomerRegistration();
        $total = $collection->getSize();
        $now = $this->_date->timestamp();
        $endDate = date('Y-m-d 23:59:59', $now);
        if (isset($typeId) && $typeId != null) {
            if ($typeId == 7) {
                $startDate = date('Y-m-d 00:00:00', strtotime('-6 days'));
            } elseif ($typeId == 30) {
                $startDate = date('Y-m-d 00:00:00', strtotime('-29 days'));
            } elseif ($typeId == 90) {
                $startDate = date('Y-m-d 00:00:00', strtotime('-89 days'));
            } elseif ($typeId == 24) {
                $endDate = date("Y-m-d H:m:s");
                $startDate = date("Y-m-d H:m:s", strtotime('-24 hours', time()));
                $timezoneLocal = $this->_timezone->getConfigTimezone();

                list($dateStart, $dateEnd) = $this->_reportCollection->getDateRange('24h', '', '', true);

                $dateStart->setTimezone(new \DateTimeZone($timezoneLocal));
                $dateEnd->setTimezone(new \DateTimeZone($timezoneLocal));

                $dates = array();

                while ($dateStart <= $dateEnd) {
                    $d = $dateStart->format('Y-m-d H:i:s');
                    $dateStart->modify('+1 hour');
                    $dates[] = $d;
                }

                $startDate = $dates[0];
                $endDate = $dates[count($dates) - 1];

                $collection->addAttributeToFilter('created_at', array('from' => $startDate, 'to' => $endDate));
                $totalCount = count($collection);
            }

            if ($typeId != 'year') {
                if ($typeId == 'month') {
                    $endDate = date("Y-m-d H:m:s");
                    $startDate = date('Y-m-01 H:m:s');
                }

                if ($typeId != 24) {
                    $collection->addAttributeToFilter('created_at', array('from' => $startDate, 'to' => $endDate));
                    $totalCount = count($collection);
                    $dates = $this->GetDatesFromRange($startDate, $endDate);
                }
            } else {
                $endDate = date('Y-m-d');
                $startDate = date('Y-01-01');
                $collection->addAttributeToFilter('created_at', array('from' => $startDate, 'to' => $endDate));
                $totalCount = count($collection);
            }
        }
        if ($tab == 'customer' && isset($limit)) {
            $collection->clear();
            $collection->getSelect()->limit($limit);
        }
        $jsonResult = array('customercollection' => $collection, 'customertotal' => $totalCount);
        return $jsonResult;
    }

    /**
     * [GetLowStockProducts use for get the low stock product on the widget]
     * @param [type] $tab     [tab for mobile]
     * @param [type] $storeId [store id]
     * @param [type] $limit   [limit]
     */
    protected function GetLowStockProducts($tab, $storeId, $limit)
    {
        $productList = array();
        $products = $this->_productFactory->create()->getCollection();
        $products->addStoreFilter($storeId)->setOrder('entity_id', 'desc')
                ->joinField(
                    'is_in_stock',
                    'cataloginventory_stock_item',
                    'is_in_stock',
                    'product_id=entity_id',
                    '{{table}}.stock_id=1',
                    'left'
                )->addAttributeToFilter('is_in_stock', array('eq' => 0));

        $productResultArr['totalproducts'] = $products->getSize();

        $products->getSelect()->limit($limit);
        if ($tab == 'product') {
            foreach ($products as $product) {
                $productData = $this->_productFactory->create()->load($product->getId());
                $status = $productData->getStatus();
                $stockItem = $this->_stockRegistry->getStockItem($productData->getId(), $productData->getStore()->getWebsiteId());
                $qty = $stockItem->getQty();
                if ($status == 1) {
                    $status = 'Enabled';
                } else {
                    $status = 'Disabled';
                }

                $productList[] = array(
                    'id' => $product->getId(),
                    'sku' => $productData->getSku(),
                    'name' => $productData->getName(),
                    'status' => $status,
                    'qty' => $qty,
                    'price' => $this->_mobileassistantHelper->getPrice($productData->getPrice(), $storeId, $this->_storeManager->getStore()->getCurrentCurrencyCode()),
                    'image' => ($productData->getImage()) ? $this->_imageHelper->init($productData, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl() : 'N/A',
                    'type' => $product->getTypeId()
                );
            }
        }

        $productResultArr['productlistdata'] = $productList;

        return $productResultArr;
    }

    /**
     * [GetDatesFromRange use for get the dates range]
     * @param [type] $startDate [start date]
     * @param [type] $endDate   [end date]
     */
    public function GetDatesFromRange($startDate, $endDate)
    {
        $dateFrom = strtotime(date('Y-m-d', strtotime($startDate)));
        $dateTo = strtotime(date('Y-m-d', strtotime($endDate)));

        for ($i = $dateFrom; $i <= $dateTo; $i+=86400) {
            $dates[] = date("Y-m-d", $i);
        }
        return $dates;
    }

    /**
     * [GetMonths use for get months]
     * @param [type] $date1 [date1]
     * @param [type] $date2 [date2]
     */
    public function GetMonths($date1, $date2)
    {
        $time1 = strtotime($date1);
        $time2 = strtotime($date2);
        $my = date('mY', $time2);
        $months = array();
        $f = '';

        while ($time1 < $time2) {
            $time1 = strtotime((date('Y-m-d', $time1) . ' +15days'));

            if (date('m', $time1) != $f) {
                $f = date('m', $time1);

                if (date('mY', $time1) != $my && ($time1 < $time2)) {
                    $months[] = date('m', $time1);
                }
            }
        }

        $months[] = date('m', $time2);
        return $months;
    }

    /**
     * [FirstDay use for get the first day of the month or year]
     * @param string $month [month]
     * @param string $year  [year]
     */
    public function FirstDay($month = '', $year = '')
    {
        if (empty($month)) {
            $month = date('m');
        }
        if (empty($year)) {
            $year = date('Y');
        }
        $result = strtotime("{$year}-{$month}-01");
        return date('Y-m-d', $result);
    }

    /**
     * [Lastday use for get the last day of the month or year]
     * @param string $month [month]
     * @param string $year  [year]
     */
    public function Lastday($month = '', $year = '')
    {
        if (empty($month)) {
            $month = date('m');
        }
        if (empty($year)) {
            $year = date('Y');
        }
        $result = strtotime("{$year}-{$month}-01");
        $result = strtotime('-1 day', strtotime('+1 month', $result));
        return date('Y-m-d', $result);
    }
}
