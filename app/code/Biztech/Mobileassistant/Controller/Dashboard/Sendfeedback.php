<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Dashboard;

use Magento\Framework\App\Action\Context;
use Magento\Store\Model\StoreManager;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Controller\Result\JsonFactory;

class Sendfeedback extends \Magento\Framework\App\Action\Action
{
    protected $moduleManager;
    protected $storeManager;
    public $request;
    protected $jsonFactory;
    protected $scopeConfig;
    protected $mobileassistantHelper;

    /**
     * @param Context                              $context
     * @param Manager                              $moduleManager
     * @param StoreManager                         $storeManager
     * @param Http                                 $request
     * @param ScopeConfigInterface                 $scopeConfig
     * @param JsonFactory                          $jsonFactory
     * @param \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
     */
    public function __construct(
        Context $context,
        Manager $moduleManager,
        StoreManager $storeManager,
        Http $request,
        ScopeConfigInterface $scopeConfig,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_storeManager = $storeManager;
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        return parent::__construct($context);
    }
    /**
     * This function used for send feedback
     * @return Void
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();
            $sessionId = '';
            if (isset($postData['session'])) {
                $sessionId = $postData['session'];
            }
            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }

            $to = "support@biztechconsultancy.com";
            $subject = "Feedback For MobileAssistant Magento2";

            $message = "<div>
                <p>Hi there. This is just test email.</p>
                <p>Below is ratings given by user: " . $postData['nickname'] . "</p>
                <p>Ratings: " . $postData['rating'] . "</p>" . " \r\n" . "
                <p>Comments: " . $postData['comment'] . "</p>" . " \r\n" . "
                <p>Please find application link:</p>" . " \r\n" . "
                <a href='" . $this->_storeManager->getStore()->getBaseUrl() . "'>Click here</a>" . " \r\n" . "
                </div>";

            $header = "From:" . $this->_scopeConfig->getValue('trans_email/ident_general/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE) . " \r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-type: text/html\r\n";
            $retval = mail($to, $subject, $message, $header);
            if ($retval == true) {
                $NewestCustomerResult = array('status' => true, 'message' => 'Message sent successfully');
            } else {
                $NewestCustomerResult = array('status' => false, 'message' => 'Message could not be sent');
            }

            $jsonResult->setData($NewestCustomerResult);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
