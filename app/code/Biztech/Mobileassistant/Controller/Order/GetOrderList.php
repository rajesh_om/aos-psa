<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Order;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Controller\Result\JsonFactory;

class GetOrderList extends \Magento\Framework\App\Action\Action
{
    protected $moduleManager;
    public $request;
    public $date;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    protected $timezone;
    protected $dirCurrency;
    protected $orderModel;
    protected $orderGridCollection;

    /**
     * @param Context                                                  $context
     * @param Manager                                                  $moduleManager
     * @param Http                                                     $request
     * @param DateTime                                                 $date
     * @param JsonFactory                                              $jsonFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface     $timezone
     * @param \Biztech\Mobileassistant\Helper\Data                     $mobileassistantHelper
     * @param \Magento\Directory\Model\CurrencyFactory                 $dirCurrency
     * @param \Magento\Sales\Model\Order                               $orderModel
     * @param \Magento\Sales\Model\ResourceModel\Order\Grid\Collection $orderGridCollection
     */
    public function __construct(
        Context $context,
        Manager $moduleManager,
        Http $request,
        DateTime $date,
        JsonFactory $jsonFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        \Magento\Directory\Model\CurrencyFactory $dirCurrency,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Sales\Model\ResourceModel\Order\Grid\Collection $orderGridCollection
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_date = $date;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_timezone = $timezone;
        $this->_dirCurrency = $dirCurrency;
        $this->_orderModel = $orderModel;
        $this->_orderGridCollection = $orderGridCollection;
        return parent::__construct($context);
    }

    /**
     * This function is used for the get the order list
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();
            if (array_key_exists('session', $postData)) {
                $sessionId = $postData['session'];
            } else {
                $errorArray = array('error' => "Session key is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }

            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }

            if (array_key_exists('limit', $postData)) {
                $limit = $postData['limit'];
            } else {
                $limit = null;
            }
            if (array_key_exists('offset', $postData)) {
                $offset = $postData['offset'];
            } else {
                $offset = null;
            }
            if (array_key_exists('storeid', $postData)) {
                $storeId = $postData['storeid'];
            } else {
                $storeId = 1;
            }
            if (array_key_exists('is_refresh', $postData)) {
                $isRefresh = $postData['is_refresh'];
            } else {
                $isRefresh = 0;
            }
            if (array_key_exists('source', $postData)) {
                $source = $postData['source'];
            } else {
                $source = '';
            }
            if (array_key_exists('last_fetch_order', $postData)) {
                $lastFetchOrder = $postData['last_fetch_order'];
            } else {
                $lastFetchOrder = '';
            }
            if (array_key_exists('min_fetch_order', $postData)) {
                $minFetchOrder = $postData['min_fetch_order'];
            } else {
                $minFetchOrder = '';
            }
            if (array_key_exists('last_updated', $postData)) {
                $lastUpdated = $postData['last_updated'];
            } else {
                $lastUpdated = '';
            }
            $orderCollection = $this->_orderGridCollection;
            $orderCollection->addFieldToFilter('store_id', array('eq' => $storeId))->setOrder('entity_id', 'desc');
            if ($offset != null) {
                $orderCollection->addFieldToFilter('entity_id', array('lt' => $offset));
            }
            if ($isRefresh == 1) {
                if (isset($postData['last_fetch_order'])) {
                    $lastFetchOrder = $postData['last_fetch_order'];
                }
                if (isset($postData['min_fetch_order'])) {
                    $minFetchOrder = $postData['min_fetch_order'];
                }
                if (isset($postData['last_updated'])) {
                    $lastUpdated = $postData['last_updated'];
                }

                $orderCollection->getSelect()->where("(entity_id BETWEEN '" . $minFetchOrder . "'AND '" . $lastFetchOrder . "' AND updated_at > '" . $lastUpdated . "') OR entity_id >'" . $lastFetchOrder . "'");
            }

            $orderCollection->getSelect()->limit($limit);
            $orderListData = array();
            foreach ($orderCollection as $order) {
                if ($source == 'dashboard') {
                    $grandTotal = $this->_mobileassistantHelper->getPrice($order->getBaseGrandTotal(), $order->getStoreId(), $order->getBaseCurrencyCode());
                } else {
                    $grandTotal = $this->_mobileassistantHelper->getPrice($order->getGrandTotal(), $order->getStoreId(), $order->getOrderCurrencyCode());
                }
                $loadOrder = $this->_orderModel->load($order->getEntityId());
                
                $currency = $this->_dirCurrency->create()->load($order->getorderCurrencyCode());
                $orderCurrencySymbol = $currency->getCurrencySymbol();
                $formattedorderprice = $currency->formatTxt($order->getGrandTotal(), array());
                $currencySymbol = $currency->getCurrencySymbol();
                $curPosition = strpos($formattedorderprice, $currencySymbol);
                if ($curPosition == 0) {
                    $ordercurrancyprefix = 1;
                } else {
                    $ordercurrancyprefix = 0;
                }


                $currency = $this->_dirCurrency->create()->load($order->getbaseCurrencyCode());
                $baseCurrencySymbol = $currency->getCurrencySymbol();
                $formattedBasePrice = $currency->formatTxt($order->getbaseGrandTotal(), array());
                $currencySymbol = $currency->getCurrencySymbol();
                $curPosition = strpos($formattedBasePrice, $currencySymbol);
                if ($curPosition == 0) {
                    $baseCurrancyPrefix = 1;
                } else {
                    $baseCurrancyPrefix = 0;
                }

                $orderListData[] = array(
                    'entity_id' => $order->getEntityId(),
                    'increment_id' => $order->getIncrementId(),
                    'store_id' => $order->getStoreId(),
                    'customer_name' => $order->getBillingName(),
                    'status' => $order->getStatus(),
                    'order_date' => $this->_timezone->date(new \DateTime($order->getCreatedAt()))->format('Y-m-d H:i:s'),
                    'grand_total' => $this->_mobileassistantHelper->orderFormattedPrice($order->getGrandTotal(), $order->getOrderCurrencyCode()),
                    'toal_qty' => $loadOrder->getTotalQtyOrdered(),
                    'total_items' => count($loadOrder->getAllVisibleItems()),
                    'base_currency_code'=> $order->getbaseCurrencyCode(),
                    'base_currency_symbol'=> $baseCurrencySymbol,
                    'base_currency_prefix' => $baseCurrancyPrefix,
                    'base_grand_total' => $order->getbaseGrandTotal(),
                    'base_formatted_grand_total' => $formattedBasePrice,
                    'order_currency_code'=> $order->getorderCurrencyCode(),
                    'order_currency_symbol' => $orderCurrencySymbol,
                    'order_currency_prefix' => $ordercurrancyprefix,
                    'order_formatted_grand_total' => $formattedorderprice,
                    'order_grand_total' => $order->getgrandTotal(),
                );
            }

            $updatedTime = date("Y-m-d H:i:s", $this->_date->timestamp());
            $orderListResultArr = array('orderlistdata' => $orderListData, 'updated_time' => $updatedTime);
            $jsonResult->setData($orderListResultArr);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
