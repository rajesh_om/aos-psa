<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Order;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Catalog\Helper\Image;
use Magento\Downloadable\Model\Link\PurchasedFactory;
use Magento\Downloadable\Model\ResourceModel\Link\Purchased\Item\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Catalog\Model\ProductFactory;

class GetOrderDetail extends \Magento\Framework\App\Action\Action
{
    protected $moduleManager;
    public $request;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    protected $imageHelper;
    protected $purchasedFactory;
    protected $itemsFactory;
    protected $scopeConfig;
    protected $dirCurrency;
    protected $orderModel;
    protected $orderGridCollection;
    protected $dirCountry;
    protected $downloadableObject;
    protected $productFactory;
    protected $orderItemCollection;
    protected $orderCurrency;
    protected $productModel;
    protected $timeZone;

    /**
     * @param Context                                                                    $context
     * @param Manager                                                                    $moduleManager
     * @param Http                                                                       $request
     * @param JsonFactory                                                                $jsonFactory
     * @param \Biztech\Mobileassistant\Helper\Data                                       $mobileassistantHelper
     * @param Image                                                                      $imageHelper
     * @param PurchasedFactory                                                           $purchasedFactory
     * @param CollectionFactory                                                          $itemsFactory
     * @param ScopeConfigInterface                                                       $scopeConfig
     * @param \Magento\Directory\Model\CurrencyFactory                                   $dirCurrency
     * @param \Magento\Sales\Model\Order                                                 $orderModel
     * @param \Magento\Sales\Model\ResourceModel\Order\Grid\Collection                   $orderGridCollection
     * @param \Magento\Directory\Model\Country                                           $dirCountry
     * @param \Magento\Downloadable\Block\Adminhtml\Sales\Items\Column\Downloadable\Name $downloadableObject
     * @param ProductFactory                                                             $productFactory
     * @param \Magento\Sales\Model\ResourceModel\Order\Item\Collection                   $orderItemCollection
     */
    public function __construct(
        Context $context,
        Manager $moduleManager,
        Http $request,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        Image $imageHelper,
        PurchasedFactory $purchasedFactory,
        CollectionFactory $itemsFactory,
        ScopeConfigInterface $scopeConfig,
        \Magento\Directory\Model\CurrencyFactory $dirCurrency,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Sales\Model\ResourceModel\Order\Grid\Collection $orderGridCollection,
        \Magento\Directory\Model\Country $dirCountry,
        \Magento\Downloadable\Block\Adminhtml\Sales\Items\Column\Downloadable\Name $downloadableObject,
        ProductFactory $productFactory,
        \Magento\Sales\Model\ResourceModel\Order\Item\Collection $orderItemCollection,
        \Magento\Catalog\Model\Product $productModel,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timeZone
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_imageHelper = $imageHelper;
        $this->_purchasedFactory = $purchasedFactory;
        $this->_itemsFactory = $itemsFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_dirCurrency = $dirCurrency;
        $this->_orderModel = $orderModel;
        $this->_orderGridCollection = $orderGridCollection;
        $this->_dirCountry = $dirCountry;
        $this->_downloadableObject = $downloadableObject;
        $this->_productFactory = $productFactory;
        $this->_orderItemCollection = $orderItemCollection;
        $this->_productModel = $productModel;
        $this->_timeZone = $timeZone;
        return parent::__construct($context);
    }
    /**
     * This function is used for get the particular order details.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();
            $sessionId = '';
            $storeId = '';
            $orderId = '';
            $orderDetail = array();
            $customerDetail = array();
            $billingAddressData = array();
            $shippingAddressData = array();
            $paymentInfo = array();
            $shippingInfo = array();
            $productsDetail = array();

            if (array_key_exists('session', $postData)) {
                $sessionId = $postData['session'];
            } else {
                $errorArray = array('error' => "Session key is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }
            if (array_key_exists('storeid', $postData)) {
                $storeId = $postData['storeid'];
            } else {
                $storeId = 1;
            }

            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }
            if (array_key_exists('entity_id', $postData)) {
                $orderId = $postData['entity_id'];
            } else {
                $errorArray = array('error' => "Order Id is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }
            if (isset($postData['entity_id'])) {
                $order = $this->_orderModel->load($orderId);

                $orderCurrency = $this->_dirCurrency->create()->load($order->getorderCurrencyCode());
                $order_currencySymbol = $orderCurrency->getCurrencySymbol();
                if ($order_currencySymbol == "") {
                    $order_currencySymbol = $order->getOrderCurrencyCode();
                }
                $formattedOrderPrice = $orderCurrency->formatTxt($order->getGrandTotal(), array());
                $currencySymbol = $orderCurrency->getCurrencySymbol();
                $curPosition = strpos($formattedOrderPrice, $currencySymbol);
                if ($curPosition == 0) {
                    $orderCurrancyPrefix = 1;
                } else {
                    $orderCurrancyPrefix = 0;
                }
               
                $baseCurrency = $this->_dirCurrency->create()->load($order->getbaseCurrencyCode());
                $baseCurrencySymbol = $baseCurrency->getCurrencySymbol();
                
                $formattedbaseprice = $baseCurrency->formatTxt($order->getbaseGrandTotal(), array());
                $currencySymbol = $baseCurrency->getCurrencySymbol();
                $curPosition = strpos($formattedbaseprice, $currencySymbol);
                if ($curPosition == 0) {
                    $basecurrancyprefix = 1;
                } else {
                    $basecurrancyprefix = 0;
                }
                
                $orderDetail = array(
                    'entity_id' => $order->getEntityId(),
                    'increment_id' => $order->getIncrementId(),
                    'status' => $order->getStatus(),
                    // 'order_date' => $order->getCreatedAt(),
                    'order_date' => $this->_timeZone->date($order->getCreatedAt())->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT),
                    'total_qty' => $order->getTotalQtyOrdered(),
                    'total_items' => count($order->getAllVisibleItems()),
                    'grand_total' => $this->_mobileassistantHelper->orderFormattedPrice($order->getGrandTotal(), $order->getOrderCurrencyCode()),
                    'sub_total' => $this->_mobileassistantHelper->orderFormattedPrice($order->getSubtotal(), $order->getOrderCurrencyCode()),
                    'tax' => $this->_mobileassistantHelper->orderFormattedPrice($order->getTax(), $order->getOrderCurrencyCode()),
                    'discount' => $this->_mobileassistantHelper->orderFormattedPrice($order->getDiscountAmount(), $order->getOrderCurrencyCode()),
                    'base_formatted_grand_total' => $this->_mobileassistantHelper->orderFormattedPrice($order->getbaseGrandTotal(), $order->getOrderCurrencyCode()),
                    'base_grand_total' => $this->_mobileassistantHelper->orderFormattedPrice($order->getbaseGrandTotal(), $order->getOrderCurrencyCode()),
                    'base_sub_total' =>  $this->_mobileassistantHelper->orderFormattedPrice($order->getbaseSubTotal(), $order->getOrderCurrencyCode()),
                    'base_discount' => $this->_mobileassistantHelper->orderFormattedPrice($order->getbaseDiscountAmount(), $order->getOrderCurrencyCode()),
                    'base_currency_symbol' => $baseCurrencySymbol,
                    'base_currency_prefix' => $basecurrancyprefix,
                    'order_formatted_grand_total' => $formattedOrderPrice,
                    'order_grand_total' => $this->_mobileassistantHelper->orderFormattedPrice($order->getGrandTotal(), $order->getOrderCurrencyCode()),
                    'order_sub_total' => $orderCurrency->formatTxt($order->getSubtotal(), array()),
                    'order_discount' => $orderCurrency->formatTxt($order->getDiscountAmount(), array()),
                    'order_currency_symbol'=> $order_currencySymbol,
                    'order_currency_prefix' => $orderCurrancyPrefix,
                    'order_currency_tax_amount' => $orderCurrency->formatTxt($order->getTaxAmount(), array()),
                    'base_currency_tax_amount' => $this->_mobileassistantHelper->orderFormattedPrice($order->getBaseTaxAmount(), $order->getOrderCurrencyCode()),
                );

                $customerId = $order->getCustomerId();
                $customerName = $order->getCustomerFirstname() . " " . $order->getCustomerLastname();
                if ($customerId == null) {
                    $customerName = $order->getCustomerName();
                }
                $customerDetail = array(
                    'customer_id' => $customerId,
                    'customer_name' => $customerName,
                    'customer_email' => $order->getCustomerEmail()
                );

                $billingAddress = $order->getBillingAddress();
                $billingAddressData = array(
                    'name' => $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname(),
                    'street' => $billingAddress->getData('street'),
                    'city' => $billingAddress->getCity(),
                    'region' => $billingAddress->getRegion(),
                    'postcode' => $billingAddress->getPostcode(),
                    'country' => $this->_dirCountry->loadByCode($billingAddress->getCountryId())->getName(),
                    'telephone' => $billingAddress->getTelephone()
                );
                $shippingAddress = $order->getShippingAddress();
                if ($shippingAddress) {
                    $shippingAddressData = array(
                        'name' => $shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname(),
                        'street' => $shippingAddress->getData('street'),
                        'city' => $shippingAddress->getCity(),
                        'region' => $shippingAddress->getRegion(),
                        'postcode' => $shippingAddress->getPostcode(),
                        'country' => $this->_dirCountry->loadByCode($shippingAddress->getCountryId())->getName(),
                        'telephone' => $shippingAddress->getTelephone()
                    );
                }

                $paymentInfo = array(
                    'payment_method' => $order->getPayment()->getMethodInstance()->getTitle()
                );

                $shippingInfo = array(
                    'shipping_method' => $order->getShippingDescription(),
                    'shipping_charge' => $this->_mobileassistantHelper->orderFormattedPrice($order->getShippingAmount(), $order->getOrderCurrencyCode()),
                    'base_currency_shipping_charge' => $baseCurrency->formatTxt($order->getBaseShippingAmount(), array()),
                    'order_currency_shipping_charge' => $orderCurrency->formatTxt($order->getShippingAmount(), array())
                );

                $productsDetail = $this->orderedProductDetails($orderId);

                $fullOrderDetail = array(
                    'basic_order_detail' => $orderDetail,
                    'customer_detail' => $customerDetail,
                    'billing_address' => $billingAddressData,
                    'payment_info' => $paymentInfo,
                    'shipping_info' => $shippingInfo,
                    'product_detail' => $productsDetail,
                    'is_invoice' => $order->canInvoice(),
                    'is_cancel' => $order->canCancel(),
                    'is_email' => !$order->isCanceled()
                );
                if (isset($shippingAddressData)) {
                    $fullOrderDetail['shipping_address'] = $shippingAddressData;
                } else {
                    $fullOrderDetail['shipping_address'] = null;
                }
                $orderDetailResultArr = array('orderlistdata' => $fullOrderDetail);
                $jsonResult->setData($orderDetailResultArr);
                return $jsonResult;
            } else {
                $errorArray = array('error' => "Order Id is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }

    /**
     * This function is return the product list of ordered
     * @param  [Int] orderId
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    protected function OrderedProductDetails($orderId)
    {
        $order = $this->_orderModel->load($orderId);
        $orderCurrency = $this->_dirCurrency->create()->load($order->getorderCurrencyCode());
        $baseCurrency = $this->_dirCurrency->create()->load($order->getbaseCurrencyCode());
        $productsDetail = array();
        foreach ($order->getItemsCollection() as $item) {
            $options = $item->getProductOptions();

            if ($item->getProductType() == "downloadable") {
                $obj = $this->_downloadableObject;

                foreach ($options['links'] as $links) {
                    $purchasedObject = $this->_purchasedFactory->create()->load($orderId, 'order_id');
                    $purchasedItem = $this->_itemsFactory->create()->addFieldToFilter('order_item_id', $item->getId());
                    $purchasedObject->setPurchasedItems($purchasedItem);

                    foreach ($purchasedObject->getPurchasedItems() as $link) {
                        $linksValue[] = $link->getLinkTitle() . '(' . $link->getNumberOfDownloadsUsed() . ' / ' . ($link->getNumberOfDownloadsBought() ? $link->getNumberOfDownloadsBought() : __('U')) . ')';
                    }
                    $info = array(array(
                            'label' => $this->_scopeConfig->getValue(
                                \Magento\Downloadable\Model\Link::XML_PATH_LINKS_TITLE,
                                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                            ),
                            'value' => implode(',', $linksValue)
                    ));
                }
            } else {
                $result = array();
                if ($options = $item->getProductOptions()) {
                    if (isset($options['options'])) {
                        $result = array_merge($result, $options['options']);
                    }
                    if (isset($options['additional_options'])) {
                        $result = array_merge($result, $options['additional_options']);
                    }
                    if (!empty($options['attributes_info'])) {
                        $result = array_merge($options['attributes_info'], $result);
                    }
                }

                $info = array();
                if ($result) {
                    foreach ($result as $option) {
                        $info[] = array(
                            'label' => $option['label'],
                            'value' => $option['value']
                        );
                    }
                }
            }
            $skus = '';
            $product = $this->_productFactory->create()->load($item->getProductId());
            if ($item->getParentItem()) {
                continue;
            }
            if ($options = $this->getItemOptions($item)) {
                $skus = $options;
            }


            $productsDetail[] = array(
                'product_id' => $item->getProductId(),
                'name' => $item->getName(),
                'sku' => $item->getSku(),
                'unit_price' => $this->_mobileassistantHelper->orderFormattedPrice($item->getPrice(), $order->getOrderCurrencyCode()),
                'ordered_qty' => round($item->getQtyOrdered(), 2),
                'row_total' => $orderCurrency->formatTxt($item->getRowTotal(), array()),
                'order_row_total'=>$orderCurrency->formatTxt($item->getRowTotal(), array()),
                'base_row_total'=>$baseCurrency->formatTxt($item->getBaseRowTotal(), array()),
                'options' => $skus ? $skus : '',
                'image' => ($product->getImage()) ? $this->_imageHelper->init($product, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl() : 'N/A',
                'attribute_info' => $info ? $info : '',
                'order_original_price_unit_price'=> $orderCurrency->formatTxt($item->getOriginalPrice(), array()),
                'base_original_price_unit_price'=> $baseCurrency->formatTxt($item->getBaseOriginalPrice(), array())
            );
        }
        return $productsDetail;
    }

    /**
     * This function is used for get ordered product item options
     * @param [Object] item
     */
    private function GetItemOptions($item)
    {
        $skus = array();
        $id = array('id' => $item->getItemId());
        $orderItems = $this->_orderItemCollection->addFieldToFilter('parent_item_id', array('eq' => $id));
        foreach ($orderItems as $orderItem) {
            $productData = $this->_productModel->load($orderItem->getProductId());
            $skus[] = $productData->getSku();
        }
        return $skus;
    }
}
