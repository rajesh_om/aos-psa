<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Order;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;

class GetFilterOrderList extends \Magento\Framework\App\Action\Action
{
    protected $moduleManager;
    public $request;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    public $date;
    protected $dirCurrency;
    protected $orderGridCollection;
    protected $orderModel;

    /**
     * @param Context                                                  $context
     * @param Manager                                                  $moduleManager
     * @param Http                                                     $request
     * @param JsonFactory                                              $jsonFactory
     * @param \Biztech\Mobileassistant\Helper\Data                     $mobileassistantHelper
     * @param DateTime                                                 $date
     * @param \Magento\Sales\Model\ResourceModel\Order\Grid\Collection $orderGridCollection
     * @param \Magento\Sales\Model\Order                               $orderModel
     * @param \Magento\Directory\Model\CurrencyFactory                 $dirCurrency
     */
    public function __construct(
        Context $context,
        Manager $moduleManager,
        Http $request,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        DateTime $date,
        \Magento\Sales\Model\ResourceModel\Order\Grid\Collection $orderGridCollection,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Directory\Model\CurrencyFactory $dirCurrency
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_date = $date;
        $this->_dirCurrency = $dirCurrency;
        $this->_orderModel = $orderModel;
        $this->_orderGridCollection = $orderGridCollection;
        return parent::__construct($context);
    }
    /**
     * This function is used for filter the order list.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }

            $postData = $this->_request->getParams();
            $orderListData = array();
            if (isset($postData['storeid'])) {
                $storeId = $postData['storeid'];
            }
            if (array_key_exists('storeid', $postData)) {
                $storeId = $postData['storeid'];
            } else {
                $errorArray = array('error' => "Store ID is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }
            if (array_key_exists('session', $postData)) {
                $sessionId = $postData['session'];
            } else {
                $errorArray = array('error' => "Session key is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }

            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }
            if (array_key_exists('filter_by_date', $postData)) {
                $filterByDate = $postData['filter_by_date'];
            } else {
                $filterByDate = null;
            }
            if (array_key_exists('filter_by_status', $postData)) {
                $filterByStatus = $postData['filter_by_status'];
            } else {
                $filterByStatus = null;
            }
            if (array_key_exists('search_by_id', $postData)) {
                $searchById = $postData['search_by_id'];
            } else {
                $searchById = null;
            }
            if (array_key_exists('offset', $postData)) {
                $offset = $postData['offset'];
            } else {
                $offset = null;
            }
            if (array_key_exists('limit', $postData)) {
                $limit = $postData['limit'];
            } else {
                $limit = 10;
            }

            $now = $this->_date->timestamp();
            $orderCollection = $this->_orderGridCollection;
            $orderCollection->addFieldToFilter('store_id', array('eq' => $storeId))->setOrder('entity_id', 'desc');

            if ($filterByDate != null) {
                $dateEnd = date('Y-m-d 23:59:59', $now);
                if ($filterByDate == 1) {
                    $dateStart = date('Y-m-d 00:00:00', $now);
                    $orderCollection->addFieldToFilter('created_at', array('from' => $dateStart, 'to' => $dateEnd));
                } elseif ($filterByDate == 2) {
                    $dateStart = date('Y-m-d 00:00:00', strtotime('-6 days'));
                    $orderCollection->addFieldToFilter('created_at', array('from' => $dateStart, 'to' => $dateEnd));
                } elseif ($filterByDate == 3) {
                    $dateStart = date('Y-m-01 00:00:00');
                    $orderCollection->addFieldToFilter('created_at', array('from' => $dateStart, 'to' => $dateEnd));
                } elseif ($filterByDate == 4) {
                    if (array_key_exists('date_before', $postData)) {
                        $orderCollection->addFieldToFilter('created_at', array("lt" => $postData['date_before'] . " 00:00:00"));
                    }
                } elseif ($filterByDate == 5) {
                    $dateStart = $postData['start_date'] . " 00:00:00";
                    $dateEnd = $postData['end_date'] . " 23:59:59";
                    $orderCollection->addFieldToFilter('created_at', array('from' => $dateStart, 'to' => $dateEnd));
                } elseif ($filterByDate == 6) {
                    if (array_key_exists('date_after', $postData)) {
                        $orderCollection->addFieldToFilter('created_at', array("gt" => $postData['date_after'] . " 23:59:59"));
                    }
                }
            }
            if ($filterByStatus != null) {
                $orderCollection->addFieldToFilter('status', array('eq' => $filterByStatus));
            }

            if ($searchById != null) {
                $orderCollection->addFieldToFilter('increment_id', array('like' => '%' . $searchById . '%'));
            }
            if ($offset != null) {
                $orderCollection->addFieldToFilter('entity_id', array('lt' => $offset));
            }
            $orderCollection->getSelect()->limit($limit);
            $orderListData = null;
            foreach ($orderCollection as $order) {
                $loadOrder = $this->_orderModel->load($order->getEntityId());

                $currency = $this->_dirCurrency->create()->load($order->getorderCurrencyCode());
                $orderCurrencySymbol = $currency->getCurrencySymbol();
                $formattedorderprice = $currency->formatTxt($order->getGrandTotal(), array());
                $currencySymbol = $currency->getCurrencySymbol();
                $curPosition = strpos($formattedorderprice, $currencySymbol);
                if ($curPosition == 0) {
                    $ordercurrancyprefix = 1;
                } else {
                    $ordercurrancyprefix = 0;
                }


                $currency = $this->_dirCurrency->create()->load($order->getbaseCurrencyCode());
                $baseCurrencySymbol = $currency->getCurrencySymbol();
                $formattedbaseprice = $currency->formatTxt($order->getbaseGrandTotal(), array());
                $currencySymbol = $currency->getCurrencySymbol();
                $curPosition = strpos($formattedbaseprice, $currencySymbol);
                if ($curPosition == 0) {
                    $baseCurrancyPrefix = 1;
                } else {
                    $baseCurrancyPrefix = 0;
                }


                $orderListData[] = array(
                    'entity_id' => $order->getEntityId(),
                    'increment_id' => $order->getIncrementId(),
                    'store_id' => $order->getStoreId(),
                    'customer_name' => $order->getBillingName(),
                    'status' => $order->getStatus(),
                    'order_date' => $order->getCreatedAt(),
                    'grand_total' => $this->_mobileassistantHelper->orderFormattedPrice($order->getGrandTotal(), $order->getOrderCurrencyCode()),
                    'toal_qty' => $loadOrder->getTotalQtyOrdered(),
                    'total_items' => count($loadOrder->getAllVisibleItems()),
                    'base_currency_code'=> $order->getbaseCurrencyCode(),
                    'base_currency_symbol'=> $baseCurrencySymbol,
                    'base_currency_prefix' => $baseCurrancyPrefix,
                    'base_grand_total' => $order->getbaseGrandTotal(),
                    'base_formatted_grand_total' => $formattedbaseprice,
                    'order_currency_code'=> $order->getorderCurrencyCode(),
                    'order_currency_symbol' => $orderCurrencySymbol,
                    'order_currency_prefix' => $ordercurrancyprefix,
                    'order_formatted_grand_total' => $formattedorderprice,
                    'order_grand_total' => $order->getgrandTotal(),
                );
            }
            $orderListResultArr = array('orderlistdata' => $orderListData);

            $jsonResult->setData($orderListResultArr);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
