<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Customer;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Module\Manager;
use Magento\Framework\View\Result\PageFactory;

class GetFilterCustomerList extends \Magento\Framework\App\Action\Action
{
    protected $moduleManager;
    public $request;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    protected $customerReportCollection;
    protected $customerAddressModel;

    /**
     * @param Context                                                  $context
     * @param Manager                                                  $moduleManager
     * @param Http                                                     $request
     * @param JsonFactory                                              $jsonFactory
     * @param \Biztech\Mobileassistant\Helper\Data                     $mobileassistantHelper
     * @param \Magento\Reports\Model\ResourceModel\Customer\Collection $customerReportCollection
     * @param \Magento\Customer\Model\Address                          $customerAddressModel
     */
    public function __construct(
        Context $context,
        Manager $moduleManager,
        Http $request,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        \Magento\Reports\Model\ResourceModel\Customer\Collection $customerReportCollection,
        \Magento\Customer\Model\Address $customerAddressModel
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_customerReportCollection = $customerReportCollection;
        $this->_customerAddressModel = $customerAddressModel;
        return parent::__construct($context);
    }
    /**
     * This function is used for the filter the customer list.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();
            $sessionId = '';
            $search = '';
            $limit = '';
            $offset = '';

            if (array_key_exists('session', $postData)) {
                $sessionId = $postData['session'];
            } else {
                $errorArray = array('error' => "Session key is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }

            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }
            if (array_key_exists('limit', $postData)) {
                $limit = $postData['limit'];
            } else {
                $limit = 10;
            }
            if (array_key_exists('offset', $postData)) {
                $offset = $postData['offset'];
            } else {
                $offset = null;
            }
            if (array_key_exists('search_content', $postData)) {
                $searchContent = $postData['search_content'];
            } else {
                $searchContent = null;
            }
            $customers = $this->_customerReportCollection->addAttributeToSelect('*')->setOrder('entity_id', 'desc');
            if ($offset != null) {
                $customers->addAttributeToFilter('entity_id', array('lt' => $offset));
            }

            if ($searchContent != null) {
                $customers->addAttributeToFilter(array(
                    array(
                        'attribute' => 'firstname',
                        'like' => '%' . $searchContent . '%',
                    ),
                    array(
                        'attribute' => 'lastname',
                        'like' => '%' . $searchContent . '%',
                    ),
                    array(
                        'attribute' => 'email',
                        'like' => '%' . $searchContent . '%',
                    ),
                ));
            }
            $customers->getSelect()->limit($limit);
            $customerList = array();
            $customerAddressModel = $this->_customerAddressModel;
            foreach ($customers as $customer) {
                $billingAddress = $customerAddressModel->load($customer->getDefaultBilling());
                $shippingAddress = $customerAddressModel->load($customer->getDefaultShipping());

                $customerList[] = array(
                    'entity_id' => $customer->getEntityId(),
                    'firstname' => $customer->getFirstname(),
                    'lastname' => $customer->getLastname(),
                    'email_id' => $customer->getEmail(),
                    'telephone' => $billingAddress->getData('telephone'),
                    'billing_address_id' => $billingAddress->getId(),
                    'shipping_address_id' => $shippingAddress->getId(),
                );
            }
            $customerListResultArr = array('customerlistdata' => $customerList);
            $jsonResult->setData($customerListResultArr);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
