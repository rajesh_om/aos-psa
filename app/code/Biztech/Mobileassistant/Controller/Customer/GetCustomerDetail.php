<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Customer;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;

class GetCustomerDetail extends \Magento\Framework\App\Action\Action
{
    protected $moduleManager;
    public $request;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    protected $baseCurrency;
    protected $customerModel;
    protected $dirCountry;
    protected $dirCurrency;
    protected $orderModel;
    protected $orderGridCollection;

    /**
     * @param Context                                                  $context
     * @param Manager                                                  $moduleManager
     * @param Http                                                     $request
     * @param JsonFactory                                              $jsonFactory
     * @param \Biztech\Mobileassistant\Helper\Data                     $mobileassistantHelper
     * @param \Magento\Customer\Model\Customer                         $customerModel
     * @param \Magento\Directory\Model\Country                         $dirCountry
     * @param \Magento\Directory\Model\CurrencyFactory                 $dirCurrency
     * @param \Magento\Sales\Model\Order                               $orderModel
     * @param \Magento\Sales\Model\ResourceModel\Order\Grid\Collection $orderGridCollection
     */
    public function __construct(
        Context $context,
        Manager $moduleManager,
        Http $request,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        \Magento\Customer\Model\Customer $customerModel,
        \Magento\Directory\Model\Country $dirCountry,
        \Magento\Directory\Model\CurrencyFactory $dirCurrency,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Sales\Model\ResourceModel\Order\Grid\Collection $orderGridCollection
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_customerModel = $customerModel;
        $this->_dirCountry = $dirCountry;
        $this->_dirCurrency = $dirCurrency;
        $this->_orderModel = $orderModel;
        $this->_orderGridCollection = $orderGridCollection;
        return parent::__construct($context);
    }
    /**
     * This function is used for get the customer details with billing and shipping details
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();
            if (array_key_exists('session', $postData)) {
                $sessionId = $postData['session'];
            } else {
                $errorArray = array('error' => "Session key is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }

            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }
            if (array_key_exists('customer_id', $postData)) {
                $customerId = $postData['customer_id'];
            } else {
                $errorArray = array('error' => "Customer Id is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }
            $customerData = $this->_customerModel->load($customerId);


            $basicDetail = array(
                'entity_id' => $customerData->getEntityId(),
                'firstname' => $customerData->getFirstname(),
                'lastname' => $customerData->getLastname(),
                'email' => $customerData->getEmail(),
            );
            $billingAddressDetail = array();
            foreach ($customerData->getAddresses() as $address) {
                $billingType = 0;
                $shippingType = 0;
                $billingCountryName = null;

                if ($address->getCountryId()) {
                    $billingCountryName = $this->_dirCountry->loadByCode($address->getCountryId())->getName();
                }

                if ($address->getId() == $customerData->getDefaultBilling()) {
                    $billingType = 1;
                }

                if ($address->getId() == $customerData->getDefaultShipping()) {
                    $shippingType = 1;
                }

                $billingAddressDetail[] = array(
                    'firstname' => $address->getFirstname(),
                    'lastname' => $address->getLastname(),
                    'street' => $address->getData('street'),
                    'city' => $address->getCity(),
                    'region_id' => $address->getRegionId() ? $address->getRegionId() : '',
                    'region' => $address->getRegion(),
                    'postcode' => $address->getPostcode(),
                    'country' => $billingCountryName,
                    'country_id' => $address->getCountryId(),
                    'telephone' => $address->getTelephone(),
                    'address_id' => $address->getId(),
                    'billing_type' => $billingType,
                    'shipping_type' => $shippingType
                );
            }

            $customerDetail = array(
                'basic_details' => $basicDetail,
                'address' => $billingAddressDetail,
            );
            $orderDetail = $this->getCustomerOrderList($customerId);

            $customerDetailResultArr = array('customerDetails' => $customerDetail, 'customerOrderDetail' => $orderDetail);
            $jsonResult->setData($customerDetailResultArr);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }

    /**
     * This function is used for the get the current customer order details
     * @param [int] customerId
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    protected function getCustomerOrderList($customerId)
    {
        $orderListData = array();
        $orderCollection = $this->_orderGridCollection->addFieldToFilter('customer_id', array('eq' => $customerId))->setOrder('entity_id', 'desc');
        $limit = 5;
        $orderCollection->getSelect()->limit($limit);

        foreach ($orderCollection as $order) {
            $orderCurrency = $this->_dirCurrency->create()->load($order->getorderCurrencyCode());
            $order_currencySymbol = $orderCurrency->getCurrencySymbol();
            $formattedorderprice = $orderCurrency->formatTxt($order->getGrandTotal(), array());
            $curPosition = strpos($formattedorderprice, $order_currencySymbol);
            if ($curPosition == 0) {
                $ordercurrancyprefix = 1;
            } else {
                $ordercurrancyprefix = 0;
            }

            $baseCurrency = $this->_dirCurrency->create()->load($order->getbaseCurrencyCode());
            $baseCurrencySymbol = $baseCurrency->getCurrencySymbol();
            $formattedbaseprice = $baseCurrency->formatTxt($order->getbaseGrandTotal(), array());
            $curPosition = strpos($formattedbaseprice, $formattedbaseprice);
            if ($curPosition == 0) {
                $basecurrancyprefix = 1;
            } else {
                $basecurrancyprefix = 0;
            }

            $orderListData[] = array(
                'entity_id' => $order->getEntityId(),
                'increment_id' => $order->getIncrementId(),
                'store_id' => $order->getStoreId(),
                'customer_name' => $order->getBillingName(),
                'status' => $order->getStatus(),
                'order_date' => date('Y-m-d H:i:s', strtotime($order->getCreatedAt())),
                'grand_total' => $this->_mobileassistantHelper->orderFormattedPrice($order->getGrandTotal(), $order->getOrderCurrencyCode()),
                'toal_qty' => $this->_orderModel->load($order->getEntityId())->getTotalQtyOrdered(),
                'order_currency_symbol' => $order_currencySymbol,
                'order_currency_prefix' => $ordercurrancyprefix,
                'order_formatted_grand_total' => $formattedorderprice,
                'base_currency_symbol'=> $baseCurrencySymbol,
                'base_currency_prefix' => $basecurrancyprefix,
                'base_formatted_grand_total'=> $formattedbaseprice
            );
        }

        return $orderListData;
    }
}
