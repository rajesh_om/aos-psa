<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Customer;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Module\Manager;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\View\Result\PageFactory;

class GetCustomerList extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;
    protected $moduleManager;
    public $request;
    public $date;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    protected $customerReportCollection;
    protected $customerAddressModel;

    /**
     * @param Context                                                  $context
     * @param PageFactory                                              $pageFactory
     * @param Manager                                                  $moduleManager
     * @param Http                                                     $request
     * @param DateTime                                                 $date
     * @param JsonFactory                                              $jsonFactory
     * @param \Biztech\Mobileassistant\Helper\Data                     $mobileassistantHelper
     * @param \Magento\Reports\Model\ResourceModel\Customer\Collection $customerReportCollection
     * @param \Magento\Customer\Model\Address                          $customerAddressModel
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Manager $moduleManager,
        Http $request,
        DateTime $date,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        \Magento\Reports\Model\ResourceModel\Customer\Collection $customerReportCollection,
        \Magento\Customer\Model\Address $customerAddressModel
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_date = $date;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_customerReportCollection = $customerReportCollection;
        $this->_customerAddressModel = $customerAddressModel;
        return parent::__construct($context);
    }

    /**
     * This function is used for the get the all customer list.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();
            if (array_key_exists('session', $postData)) {
                $sessionId = $postData['session'];
            } else {
                $errorArray = array('error' => "Session key is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }

            if (!$sessionId || $sessionId == null) {
                $errorResult = array('error' => 'Session key is missing');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }

            if (array_key_exists('limit', $postData)) {
                $limit = $postData['limit'];
            } else {
                $limit = null;
            }
            if (array_key_exists('offset', $postData)) {
                $offset = $postData['offset'];
            } else {
                $offset = null;
            }
            
            if (array_key_exists('last_fetch_customer', $postData)) {
                $lastFetchCustomer = $postData['last_fetch_customer'];
            } else {
                $lastFetchCustomer = '';
            }
            if (array_key_exists('min_fetch_customer', $postData)) {
                $minFetchCustomer = $postData['min_fetch_customer'];
            } else {
                $minFetchCustomer = '';
            }
            if (array_key_exists('last_updated', $postData)) {
                $lastUpdated = $postData['last_updated'];
            } else {
                $lastUpdated = '';
            }
            if (array_key_exists('is_refresh', $postData)) {
                $isRefresh = $postData['is_refresh'];
            } else {
                $isRefresh = 0;
            }

            $customers = $this->_customerReportCollection;
            $customers->addAttributeToSelect('*')->setOrder('entity_id', 'desc');

            if ($offset != null) {
                $customers->addAttributeToFilter('entity_id', array('lt' => $offset));
            }

            if ($isRefresh == 1) {
                if (isset($postData['last_fetch_customer'])) {
                    $lastFetchCustomer = $postData['last_fetch_customer'];
                }
                if (isset($postData['min_fetch_customer'])) {
                    $minFetchCustomer = $postData['min_fetch_customer'];
                }
                $customers->getSelect()->where("(e.entity_id BETWEEN '" . $minFetchCustomer . "'AND '" . $lastFetchCustomer . "' AND updated_at > '" . $lastUpdated . "') OR e.entity_id >'" . $lastFetchCustomer . "'");
            }

            $customers->getSelect()->limit($limit);
            $customerList = array();
            $customerAddressModel = $this->_customerAddressModel;
            foreach ($customers as $customer) {
                $billingAddress = $customerAddressModel->load($customer->getDefaultBilling());
                $shippingAddress = $customerAddressModel->load($customer->getDefaultShipping());

                $customerList[] = array(
                    'entity_id' => $customer->getEntityId(),
                    'firstname' => $customer->getFirstname(),
                    'lastname' => $customer->getLastname(),
                    'email_id' => $customer->getEmail(),
                    'telephone' => $billingAddress->getData('telephone'),
                    'billing_address_id' => $billingAddress->getId(),
                    'shipping_address_id' => $shippingAddress->getId(),
                );
            }
            $updatedTime = date("Y-m-d H:i:s", $this->_date->timestamp());
            $customerListResultArr = array('customerlistdata' => $customerList, 'updated_time' => $updatedTime);
            $jsonResult->setData($customerListResultArr);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
