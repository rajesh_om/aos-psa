<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;

class GetLogoAndCurrency extends \Magento\Framework\App\Action\Action
{
    protected $request;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    protected $headerLogo;
    protected $dirCurrency;
    protected $storeManagerInterface;
    protected $scopeConfigInterface;
    protected $productMetadataInterface;

    /**
     * @param Context                                            $context
     * @param Http                                               $request
     * @param JsonFactory                                        $jsonFactory
     * @param \Biztech\Mobileassistant\Helper\Data               $helper
     * @param \Magento\Theme\Block\Html\Header\Logo              $headerLogo
     * @param \Magento\Directory\Model\Currency                  $dirCurrency
     * @param \Magento\Store\Model\StoreManagerInterface         $storeManagerInterface
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface
     * @param \Magento\Framework\App\ProductMetadataInterface    $productMetadataInterface
     */
    public function __construct(
        Context $context,
        Http $request,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        \Magento\Theme\Block\Html\Header\Logo $headerLogo,
        \Magento\Directory\Model\Currency $dirCurrency,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfigInterface,
        \Magento\Framework\App\ProductMetadataInterface $productMetadataInterface
    ) {
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_headerLogo = $headerLogo;
        $this->_dirCurrency = $dirCurrency;
        $this->_storeManagerInterface = $storeManagerInterface;
        $this->_scopeConfigInterface = $scopeConfigInterface;
        $this->_productMetadataInterface = $productMetadataInterface;
        return parent::__construct($context);
    }
    /**
     * This function is used for the get the logo and currency details.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();

        if (!$this->_mobileassistantHelper->getHeaders()) {
            $errorResult = array('error' => 'Please make sure your app is proper authenticated');
            $jsonResult->setData($errorResult);
            return $jsonResult;
        }

        $postData = $this->_request->getParams();
        if (array_key_exists('storeid', $postData)) {
            $storeId = $postData['storeid'];
        } else {
            $storeId = 1;
        }

        $block = $this->_headerLogo;
        $logo = $block->getLogoSrc();

        $currencySymbol = $this->_dirCurrency;
        $currencySymbol = $currencySymbol->getCurrencySymbol();

        $storeManagerInterface = $this->_storeManagerInterface;
                        
        $currencyCode = $this->_scopeConfigInterface->getValue('currency/options/default', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, 1);

        $currency = $this->_dirCurrency->load($currencyCode);
        $currencySymbol = $currency->getCurrencySymbol();
        if ($currencySymbol == null) {
            $currencySymbol = $currencyCode;
        }

        $isPos = 0;
        $mageVersion = $this->getMageVersion();
        $logoCurrencyArray = array('logo' => $logo,
            'currency_symbol' => $currencySymbol ,
            'is_pos' => $isPos,
            'is_inventory' => 0,
            'magento_version' => $mageVersion);

        $jsonResult->setData($logoCurrencyArray);
        return $jsonResult;
    }

    /**
     * Get the current magento instace version
     * @return String
     */
    private function getMageVersion()
    {
        $allowVersion = false;
        return $allowVersion;
    }
}
