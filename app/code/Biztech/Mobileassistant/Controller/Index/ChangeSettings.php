<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Index;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;

class ChangeSettings extends \Magento\Framework\App\Action\Action
{
    protected $request;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    protected $mobileassistantModel;

    public function __construct(
        Context $context,
        Http $Request,
        JsonFactory $JsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        \Biztech\Mobileassistant\Model\Mobileassistant $mobileassistantModel
    ) {
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_mobileassistantModel = $mobileassistantModel;
        return parent::__construct($context);
    }

    /**
     * This function is used for the change the settings in the app.
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        $postData = $this->_request->getParams();

        if (array_key_exists('userapi', $postData)) {
            $userApi = $postData['userapi'];
        } else {
            $errorArray = array('error' => "userapi key is missing");
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }
        if (array_key_exists('token', $postData)) {
            $deviceToken = $postData['token'];
        } else {
            $errorArray = array('error' => "token key is missing");
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }
        if (array_key_exists('notification_flag', $postData)) {
            $notificationFlag = $postData['notification_flag'];
        } else {
            $errorArray = array('error' => "notification_flag key is missing");
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }
        $deviceCollections = $this->_mobileassistantModel->getCollection()
        ->addFieldToFilter('username', array('eq' => $userApi))
        ->addFieldToFilter('device_token', array('eq' => $deviceToken));
        $deviceCollectionCount = count($deviceCollections);
        if ($deviceCollectionCount < 1) {
            $errorArray = array('error' => 'No match found for this userapi and token');
            $jsonResult->setData($errorArray);
            return $jsonResult;
        }
        foreach ($deviceCollections as $deviceDetail) {
            $userId = $deviceDetail->getUserId();
        }

        if ($deviceCollectionCount == 1) {
            try {
                $model = $this->_mobileassistantModel;
                $model->load($userId)
                        ->setNotificationFlag($notificationFlag)
                        ->save();

                $sucessArray = array('success_msg' => 'Settings updated sucessfully.');
                $jsonResult->setData($sucessArray);
                return $jsonResult;
            } catch (\Exception $e) {
                $errorArray = array('error' => $e->getMessage());
                $jsonResult->setData($sucessArray);
                return $jsonResult;
            }
        }
    }
}
