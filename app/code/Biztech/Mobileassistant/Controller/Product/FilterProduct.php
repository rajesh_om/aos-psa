<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Biztech\Mobileassistant\Controller\Product;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManager;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class FilterProduct extends \Magento\Framework\App\Action\Action
{
    protected $moduleManager;
    public $request;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    protected $storeManager;
    protected $imageHelper;
    protected $productFactory;
    protected $stockRegistry;

    /**
     * @param Context                              $context
     * @param Manager                              $moduleManager
     * @param Http                                 $request
     * @param JsonFactory                          $jsonFactory
     * @param \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
     * @param StoreManager                         $storeManager
     * @param Image                                $imageHelper
     * @param ProductFactory                       $productFactory
     * @param StockRegistryInterface               $stockRegistry
     */
    public function __construct(
        Context $context,
        Manager $moduleManager,
        Http $request,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        StoreManager $storeManager,
        Image $imageHelper,
        ProductFactory $productFactory,
        StockRegistryInterface $stockRegistry
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_storeManager = $storeManager;
        $this->_imageHelper = $imageHelper;
        $this->_productFactory = $productFactory;
        $this->_stockRegistry = $stockRegistry;
        return parent::__construct($context);
    }
    /**
     * This function is used for the filter the product list
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();
            
            if (array_key_exists('session', $postData)) {
                $sessionId = $postData['session'];
            } else {
                $errorArray = array('error' => "Session key is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }

            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }
            try {
                if (array_key_exists('storeid', $postData)) {
                    $storeId = $postData['storeid'];
                } else {
                    $storeId = 1;
                }
                if (array_key_exists('limit', $postData)) {
                    $limit = $postData['limit'];
                } else {
                    $limit = null;
                }
                if (array_key_exists('offset', $postData)) {
                    $offset = $postData['offset'];
                } else {
                    $offset = null;
                }
                if (array_key_exists('filter_by_name', $postData)) {
                    $filterByName = $postData['filter_by_name'];
                } else {
                    $filterByName = null;
                }
                if (array_key_exists('product_type', $postData)) {
                    $filterByType = $postData['product_type'];
                } else {
                    $filterByType = null;
                }
                if (array_key_exists('filter_by_qty', $postData)) {
                    $filterByQty = $postData['filter_by_qty'];
                } else {
                    $filterByQty = null;
                }

                $products = $this->_productFactory->create()->getCollection();
                $products->addStoreFilter($storeId)->setOrder('entity_id', 'desc');

                if ($filterByName != null) {
                    $products->addAttributeToFilter(array(
                        array(
                            'attribute' => 'name',
                            'like' => '%' . $filterByName . '%'
                        ),
                        array(
                            'attribute' => 'sku',
                            'like' => '%' . $filterByName . '%'
                        )
                    ));
                }

                if ($filterByType != null) {
                    $products->addFieldToFilter('type_id', array('eq' => $filterByType));
                }

                if ($filterByQty != null) {
                    $products->joinField('qty', 'cataloginventory_stock_item', 'qty', 'product_id=entity_id', '{{table}}.stock_id=1', 'left');
                    if ($filterByQty == 'gteq') {
                        $qty = $postData['qty'];
                        $products->addFieldToFilter('qty', array('gteq' => $qty));
                    } elseif ($filterByQty == 'lteq') {
                        $qty = $postData['qty'];
                        $products->addFieldToFilter('qty', array('lteq' => $qty));
                    } elseif ($filterByQty == 'btwn') {
                        $from_qty = $postData['from_qty'];
                        $to_qty = $postData['to_qty'];
                        $products->addFieldToFilter('qty', array('from' => $from_qty, 'to' => $to_qty));
                    }
                }
                if ($offset != null) {
                    $products->addAttributeToFilter('entity_id', array('lt' => $offset));
                }
                $products->getSelect()->limit($limit);
                $productList = null;
                $productList = null;
                $allProductsIds = array();
                foreach ($products->getData() as $value) {
                    $allProductsIds[] = $value['entity_id'];
                }
                $products = $allProductsIds;
                foreach ($products as $product) {
                    $productData = $this->_productFactory->create()->setStoreId($storeId)->load($product);

                    $status = $productData->getStatus();
                    $stockItem = $this->_stockRegistry->getStockItem($product, 1);
                    $qty = $stockItem->getQty();
                    if ($status == 1) {
                        $status = 'Enabled';
                    } else {
                        $status = 'Disabled';
                    }
                    if ($qty < 0 || $stockItem->getIsInStock() == 0 || $qty == null) {
                        $qty = 'Out of Stock';
                    }
                    $productList[] = array(
                        'id' => $product,
                        'sku' => $productData->getSku(),
                        'name' => $productData->getName(),
                        'status' => $status,
                        'qty' => $qty,
                        'price' => $this->_mobileassistantHelper->getformattedPrice($productData->getPrice()),
                        'image' => ($productData->getImage()) ? $this->_imageHelper->init($productData, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl() : 'N/A',
                        'type' => $productData->getTypeId()
                    );
                }

                $productListResultArr = array('productlistdata' => $productList);
                $jsonResult->setData($productListResultArr);
                return $jsonResult;
            } catch (\Exception $e) {
                $productDetails = array(
                    'status' => 'error',
                    'message' => $e->getMessage()
                );
                $jsonResult->setData($productDetails);
                return $jsonResult;
            }
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
