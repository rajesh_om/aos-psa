<?php
/**
 *
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Product;

use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Store\Model\StoreManager;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class GetProductDetail extends \Magento\Framework\App\Action\Action
{
    protected $pageFactory;
    protected $moduleManager;
    public $request;
    protected $jsonFactory;
    protected $mobileassistantHelper;
    protected $storeManager;
    protected $imageHelper;
    protected $productFactory;
    protected $stockRegistry;

    /**
     * @param Context                                                                                       $context
     * @param PageFactory                                                                                   $pageFactory
     * @param Manager                                                                                       $moduleManager
     * @param Http                                                                                          $request
     * @param JsonFactory                                                                                   $jsonFactory
     * @param \Biztech\Mobileassistant\Helper\Data                                                          $mobileassistantHelper
     * @param StoreManager                                                                                  $storeManager
     * @param Image                                                                                         $imageHelper
     * @param ProductFactory                                                                                $productFactory
     * @param StockRegistryInterface                                                                        $stockRegistry
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        Manager $moduleManager,
        Http $request,
        JsonFactory $jsonFactory,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        StoreManager $storeManager,
        Image $imageHelper,
        ProductFactory $productFactory,
        StockRegistryInterface $stockRegistry
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_storeManager = $storeManager;
        $this->_imageHelper = $imageHelper;
        $this->_productFactory = $productFactory;
        $this->_stockRegistry = $stockRegistry;
        return parent::__construct($context);
    }
    /**
     * Get the particular product details
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $result = $this->_jsonFactory->create();

        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $result->setData($errorResult);
                return $result;
            }

            $postData = $this->_request->getParams();
            $storeId = '';
            $productId = '';
            $productSku = '';
            $associatedProducts = array();
            $associatedProductsList = array();
            $associatedProductsDetails = array();
            $allImages = array();
            
            if (isset($postData['session'])) {
                $sessionId = $postData['session'];
            }

            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $result->setData($sessionExpire);
                return $result;
            }
            try {
                if (array_key_exists('storeid', $postData)) {
                    $storeId = $postData['storeid'];
                }
                if (array_key_exists('store_id', $postData)) {
                    $storeId = $postData['store_id'];
                } else {
                }
                if (array_key_exists('product_id', $postData)) {
                    $productId = $postData['product_id'];
                }
                if (array_key_exists('productid', $postData)) {
                    $productId = $postData['productid'];
                }
                if (array_key_exists('sku', $postData)) {
                    $productSku = $postData['sku'];
                }

                if (isset($productId) && $productId != "") {
                    $productData = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
                } else {
                    $product = $this->_productFactory->create()->getCollection();
                    $product->addAttributeToFilter('sku', $productSku);
                    foreach ($product->getData() as $key => $value) {
                        $productId = $value['entity_id'];
                    }
                    if ($product) {
                        $productData = $this->_productFactory->create()->setStoreId($storeId)->load($productId);
                    } else {
                        $errorArray = array('mesage' => 'No product found.');
                        $result->setData($errorArray);
                        return $result;
                    }
                }
                $proStatus = $productData->getStatus();
                $stockItem = $this->_stockRegistry->getStockItem($productData->getId(), $productData->getStore()->getWebsiteId());
                $pro_qty = $stockItem->getQty();

                if ($pro_qty < 0 || $stockItem->getIsInStock() == 0 || $pro_qty == null) {
                    $pro_qty = 'Out of Stock';
                }
                $allMediaGalleryImages = $productData->getMediaGalleryImages();
                if ($allMediaGalleryImages) {
                    foreach ($allMediaGalleryImages as $mediaImages) {
                        $allImages[] = $this->_imageHelper->init($productData, 'product_page_image_large')
                                ->setImageFile($mediaImages->getFile())->constrainOnly(false)->keepAspectRatio(true)->keepFrame(true)->resize(300, 330)
                                ->getUrl();
                    }
                }
                if ($proStatus == 1) {
                    $proStatus = 'Enabled';
                } else {
                    $proStatus = 'Disabled';
                }

                if ($productData->getTypeId() == 'grouped') {
                    $associatedProducts = $productData->getTypeInstance(true)->getAssociatedProducts($productData);
                } elseif ($productData->getTypeId() == 'configurable') {
                    $associatedProducts = $productData->getTypeInstance()->getUsedProducts($productData);
                } elseif ($productData->getTypeId() == 'bundle') {
                    $associatedProducts = $productData->getTypeInstance(true)->getSelectionsCollection($productData->getTypeInstance(true)->getOptionsIds($productData), $productData);
                }

                foreach ($associatedProducts as $associatedProduct) {
                    $status = $associatedProduct->getStatus();
                    $stockItems = $this->_stockRegistry->getStockItem($associatedProduct->getId(), $associatedProduct->getStore()->getWebsiteId());
                    $qty = $stockItems->getQty();
                    if ($status == 1) {
                        $status = 'Enabled';
                    } else {
                        $status = 'Disabled';
                    }
                    if ($qty == 0 || $stockItems->getIsInStock() == 0 || $qty == null) {
                        $qty = 'Out of Stock';
                    }
                    $associatedProductsDetails[] = array(
                        'id' => $associatedProduct->getId(),
                        'sku' => $associatedProduct->getSku()
                    );

                    if ($stockItems->getIsInStock() == true) {
                        $stockStatus = 1;
                    } else {
                        $stockStatus = 0;
                    }
                    $associatedProductsList[] = array(
                        'id' => $associatedProduct->getId(),
                        'sku' => $associatedProduct->getSku(),
                        'name' => $associatedProduct->getName(),
                        'status' => $status,
                        'qty' => $qty,
                        'is_in_stock' => $stockStatus,
                        'price' => $this->_mobileassistantHelper->getPrice($associatedProduct->getPrice(), $storeId, $this->_storeManager->getStore()->getCurrentCurrencyCode()),
                    );
                }

                if ($stockItem->getIsInStock() == true) {
                    $stock_status = 1;
                } else {
                    $stock_status = 0;
                }
                $specialPrice = $this->_mobileassistantHelper->getPrice($productData->getSpecialPrice(), $storeId, $this->_storeManager->getStore()->getCurrentCurrencyCode());
                $productSpecialPrice = $productData->getSpecialPrice();
                if (isset($productSpecialPrice)) {
                    $specialPrice = $this->_mobileassistantHelper->getformattedPrice($specialPrice);
                } else {
                    $specialPrice = "";
                }
                if ($productData->getWeight() != null) {
                    $weight = $productData->getWeight();
                } else {
                    $weight = "";
                }
                if ($productData->getVisibility() == 1) {
                    $visibility = "Not Visible Individually";
                } elseif ($productData->getVisibility() == 2) {
                    $visibility = "Catalog";
                } elseif ($productData->getVisibility() == 3) {
                    $visibility = "Search";
                } elseif ($productData->getVisibility() == 4) {
                    $visibility = "Catalog, Search";
                } else {
                    $visibility = "Not Visible Individually";
                }
                
                $productDetails[] = array(
                    'id' => $productData->getId(),
                    'sku' => $productData->getSku(),
                    'name' => $productData->getName(),
                    'status' => $proStatus,
                    'qty' => $pro_qty,
                    'is_in_stock' => $stock_status,
                    'price' => $this->_mobileassistantHelper->getformattedPrice($productData->getPrice()),
                    'visibility' => $visibility,
                    'visibility_id' => $productData->getVisibility(),
                    'desc' => $productData->getDescription(),
                    'type' => $productData->getTypeId(),
                    'special_price' => $specialPrice,
                    'weight' => $weight,
                    'image' => ($productData->getImage()) ? $this->_imageHelper->init($productData, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl() : 'N/A',
                    'associated_skus' => $associatedProductsDetails,
                    'all_images' => $allImages,
                );
                $productResultArr = array('productdata' => $productDetails, 'associated_products_list' => $associatedProductsList);
                $result->setData($productResultArr);
                return $result;
            } catch (\Exception $e) {
                $productDetails = array(
                    'status' => 'error',
                    'message' => $e->getMessage()
                );
                $result->setData($productDetails);
                return $result;
            }
        } else {
            $returnExtensionArray = array('enable' => false);
            $result->setData($returnExtensionArray);
            return $result;
        }
    }
}
