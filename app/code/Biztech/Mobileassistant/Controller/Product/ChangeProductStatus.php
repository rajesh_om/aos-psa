<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Product;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Module\Manager;

class ChangeProductStatus extends \Magento\Framework\App\Action\Action
{
    protected $moduleManager;
    public $request;
    protected $jsonFactory;
    protected $productRepository;
    protected $mobileassistantHelper;

    /**
     * @param Context                              $context
     * @param Manager                              $moduleManager
     * @param Http                                 $request
     * @param JsonFactory                          $jsonFactory
     * @param ProductRepositoryInterface           $productRepository
     * @param \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
     */
    public function __construct(
        Context $context,
        Manager $moduleManager,
        Http $request,
        JsonFactory $jsonFactory,
        ProductRepositoryInterface $productRepository,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_jsonFactory = $jsonFactory;
        $this->_productRepository = $productRepository;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        return parent::__construct($context);
    }
    /**
     * This function is used for change product status
     * @return Bool
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();

            if (array_key_exists('session', $postData)) {
                $sessionId = $postData['session'];
            } else {
                $errorArray = array('error' => "Session key is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }
            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }
            try {
                if (array_key_exists('storeid', $postData)) {
                    $storeId = $postData['storeid'];
                } else {
                    $storeId = 1;
                }
                if (array_key_exists('productid', $postData)) {
                    $productId = $postData['productid'];
                }
                if (array_key_exists('product_id', $postData)) {
                    $productId = $postData['product_id'];
                }
                if (array_key_exists('current_status', $postData)) {
                    $currentStatus = $postData['current_status'];
                }
                if (array_key_exists('new_status', $postData)) {
                    $newStatus = $postData['new_status'];
                }

                if ($currentStatus != $newStatus && $newStatus == 1) {
                    $product = $this->_productRepository->getById($productId);
                    $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                    $this->_productRepository->save($product);
                } elseif ($currentStatus != $newStatus && $newStatus == 2) {
                    $product = $this->_productRepository->getById($productId);
                    $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED);
                    $this->_productRepository->save($product);
                }

                $productResultArr = array('status' => 'true', 'message' => 'Product has been successfully updated.');
                $jsonResult->setData($productResultArr);
                return $jsonResult;
            } catch (\Exception $e) {
                $productDetails = array(
                    'status' => 'error',
                    'message' => $e->getMessage(),
                );
                $jsonResult->setData($productDetails);
                return $jsonResult;
            }
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
