<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Controller\Product;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Module\Manager;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Locale\CurrencyInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Store\Model\StoreManager;
use Magento\Catalog\Helper\Image;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class GetProductList extends \Magento\Framework\App\Action\Action
{
    protected $moduleManager;
    public $request;
    public $date;
    protected $jsonFactory;
    protected $localeCurrency;
    protected $mobileassistantHelper;
    protected $productFactory;
    protected $storeManager;
    protected $imageHelper;
    protected $stockRegistry;

    /**
     * @param Context                              $context
     * @param Manager                              $moduleManager
     * @param Http                                 $request
     * @param DateTime                             $date
     * @param JsonFactory                          $jsonFactory
     * @param CurrencyInterface                    $localeCurrency
     * @param \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
     * @param ProductFactory                       $productFactory
     * @param StoreManager                         $storeManager
     * @param Image                                $imageHelper
     * @param StockRegistryInterface               $stockRegistry
     */
    public function __construct(
        Context $context,
        Manager $moduleManager,
        Http $request,
        DateTime $date,
        JsonFactory $jsonFactory,
        CurrencyInterface $localeCurrency,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        ProductFactory $productFactory,
        StoreManager $storeManager,
        Image $imageHelper,
        StockRegistryInterface $stockRegistry
    ) {
        $this->_moduleManager = $moduleManager;
        $this->_request = $request;
        $this->_date = $date;
        $this->_jsonFactory = $jsonFactory;
        $this->_localeCurrency = $localeCurrency;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_productFactory = $productFactory;
        $this->_storeManager = $storeManager;
        $this->_imageHelper = $imageHelper;
        $this->_stockRegistry = $stockRegistry;
        return parent::__construct($context);
    }
    /**
     * This function is used for get the all products list
     * @return Magento\Framework\Controller\Result\JsonFactory
     */
    public function execute()
    {
        $jsonResult = $this->_jsonFactory->create();
        if ($this->_moduleManager->isEnabled('Biztech_Mobileassistant') && $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            if (!$this->_mobileassistantHelper->getHeaders()) {
                $errorResult = array('error' => 'Please make sure your app is proper authenticated');
                $jsonResult->setData($errorResult);
                return $jsonResult;
            }
            $postData = $this->_request->getParams();

            if (array_key_exists('session', $postData)) {
                $sessionId = $postData['session'];
            } else {
                $errorArray = array('error' => "Session key is missing");
                $jsonResult->setData($errorArray);
                return $jsonResult;
            }
            if (!$sessionId || $sessionId == null) {
                $sessionExpire = array('session_expire' => "The Login has expired. Please try log in again");
                $jsonResult->setData($sessionExpire);
                return $jsonResult;
            }
            if (array_key_exists('limit', $postData)) {
                $limit = $postData['limit'];
            } else {
                $limit = null;
            }
            if (array_key_exists('offset', $postData)) {
                $offset = $postData['offset'];
            } else {
                $offset = null;
            }
            if (array_key_exists('storeid', $postData)) {
                $storeId = $postData['storeid'];
            } else {
                $storeId = 1;
            }
            if (array_key_exists('last_fetch_product', $postData)) {
                $lastFetchProduct = $postData['last_fetch_product'];
            } else {
                $lastFetchProduct = null;
            }
            if (array_key_exists('is_refresh', $postData)) {
                $isRefresh = $postData['is_refresh'];
            } else {
                $isRefresh = 0;
            }
            $products = $this->_productFactory->create()->getCollection();
            $products->addStoreFilter($storeId)->setOrder('entity_id', 'desc');

            if ($offset != null) {
                $products->addAttributeToFilter('entity_id', array('lt' => $offset));
            }

            if ($isRefresh == 1) {
                $minFetchProduct = '';
                $lastFetchProduct = '';
                $lastUpdated = '';
                if (isset($postData['last_fetch_product'])) {
                    $lastFetchProduct = $postData['last_fetch_product'];
                }
                if (isset($postData['min_fetch_product'])) {
                    $minFetchProduct = $postData['min_fetch_product'];
                }
                if (isset($postData['last_updated'])) {
                    $lastUpdated = $postData['last_updated'];
                }
                $products->getSelect()->where("(entity_id BETWEEN '" . $minFetchProduct . "'AND '" . $lastFetchProduct . "' AND updated_at > '" . $lastUpdated . "') OR entity_id >'" . $lastFetchProduct . "'");
            }

            $products->getSelect()->limit($limit);
            $productList = array();
            $allProductsIds = array();
            foreach ($products->getData() as $value) {
                $allProductsIds[] = $value['entity_id'];
            }
            $products = $allProductsIds;
            foreach ($products as $product) {
                $productData = $this->_productFactory->create()->setStoreId($storeId)->load($product);

                $status = $productData->getStatus();
                $stockItem = $this->_stockRegistry->getStockItem($product, 1);
                $qty = $stockItem->getQty();
                if ($status == 1) {
                    $status = 'Enabled';
                } else {
                    $status = 'Disabled';
                }
                if ($qty < 0 || $stockItem->getIsInStock() == 0 || $qty == null) {
                    $qty = 'Out of Stock';
                }
                $productList[] = array(
                    'id' => $product,
                    'sku' => $productData->getSku(),
                    'name' => $productData->getName(),
                    'status' => $status,
                    'qty' => $qty,
                    'price' => $this->_mobileassistantHelper->getformattedPrice($productData->getPrice()),
                    'image' => ($productData->getImage()) ? $this->_imageHelper->init($productData, 'product_page_image_medium')->resize(300, 330)->constrainOnly(true)->keepAspectRatio(true)->getUrl() : 'N/A',
                    'type' => $productData->getTypeId()
                );
            }
            $updatedTime = date("Y-m-d H:i:s", $this->_date->timestamp());
            $productResultArr = array('productlistdata' => $productList, 'updated_time' => $updatedTime);
            $jsonResult->setData($productResultArr);
            return $jsonResult;
        } else {
            $returnExtensionArray = array('enable' => false);
            $jsonResult->setData($returnExtensionArray);
            return $jsonResult;
        }
    }
}
