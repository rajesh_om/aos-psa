<?php

namespace Biztech\Mobileassistant\Helper;

use Biztech\Mobileassistant\Model\MobileassistantFactory;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Locale\CurrencyInterface;
use Magento\Store\Model\StoreManager;
use Zend\Json\Json;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_DATA = 'mobileassistant/activation/data';
    const XML_PATH_INSTALLED = 'mobileassistant/activation/installed';
    const XML_PATH_WEBSITES = 'mobileassistant/activation/websites';
    const SECRET = "dfbDSkfdskf17hkfbgqwrsaFBfkdbf6bj";
    const KEY = 'secretkey';
    const XML_GOOGLE_KEY = 'mobileassistant/pushnotification/android_key';
    const XML_IOS_KEY = 'mobileassistant/pushnotification/ios_key';

    protected $collectionFactory;
    protected $JsonFactory;
    protected $storeManager;
    protected $localeCurrency;
    protected $zend;
    protected $encryptor;
    protected $scopeConfig;
    protected $moduleDir;
    protected $mobileassistantCollection;
    protected $mobileassistantModel;
    protected $storeModel;
    protected $dirCurrencyFactory;
    protected $orderModel;
    protected $moduleList;
    protected $pricingHelper;

    public function __construct(
        Context $context,
        MobileassistantFactory $collectionFactory,
        JsonFactory $JsonFactory,
        CurrencyInterface $localeCurrency,
        Json $zend,
        EncryptorInterface $encryptor,
        StoreManager $storeManager,
        \Magento\Framework\Module\Dir\Reader $moduleDir,
        \Biztech\Mobileassistant\Model\Cresource\Mobileassistant\Collection $mobileassistantCollection,
        \Biztech\Mobileassistant\Model\Mobileassistant $mobileassistantModel,
        \Magento\Store\Model\Store $storeModel,
        \Magento\Directory\Model\CurrencyFactory $dirCurrencyFactory,
        \Magento\Sales\Model\Order $orderModel,
        \Magento\Framework\Module\ModuleList $moduleList,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->JsonFactory = $JsonFactory;
        $this->storeManager = $storeManager;
        $this->localeCurrency = $localeCurrency;
        $this->zend = $zend;
        $this->encryptor = $encryptor;
        $this->scopeConfig = $context->getScopeConfig();
        $this->moduleDir = $moduleDir;
        $this->mobileassistantCollection = $mobileassistantCollection;
        $this->mobileassistantModel = $mobileassistantModel;
        ;
        $this->dirCurrencyFactory = $dirCurrencyFactory;
        $this->orderModel = $orderModel;
        $this->moduleList = $moduleList;
        $this->pricingHelper = $pricingHelper;
        parent::__construct($context);
    }

    /**
     * Create function for the push the login details
     * @param  [type] $data [data get the current login customer details]
     * @return [type]       [description]
     */
    
    public function create($data)
    {
        $collections = $this->mobileassistantCollection;
        $collections->addFieldToFilter('username', $data['username'])
            ->addFieldToFilter('password', $data['password'])
            ->addFieldToFilter('device_token', $data['devicetoken']);
        $result = $this->JsonFactory->create();
        $count = count($collections);
        if ($count == 0) {
            $model = $this->mobileassistantModel;
            $model->setUsername($data['username'])
                ->setPassword($data['password'])
                ->setDeviceToken($data['devicetoken'])
                ->setDeviceType($data['device_type'])
                ->setNotificationFlag($data['notification_flag'])
                ->save();
        }
        if ($count == 1) {
            foreach ($collections as $user) {
                $userId = $user->getUserId();
                $flag = $user->getNotificationFlag();
            }
            if ($flag != $data['notification_flag'] || $data['is_logout'] != 1) {
                try {
                    $model = $this->mobileassistantModel;
                    $model->load($userId)
                        ->setNotificationFlag($data['notification_flag'])
                        ->setIsLogout($data['is_logout'])
                        ->save();
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        }

        $successArr[] = array('success_msg' => 'Login sucessfully', 'session_id' => $data['session_id']);

        foreach ($this->storeManager->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores($group);
                foreach ($stores as $store) {
                    $storeArr[] = array('id' => $store->getId(),
                        'name' => $store->getName(),
                    );
                }
            }
        }

        $isPos = 0;
        $sucessArray = array('success' => $successArr, 'stores' => $storeArr, 'is_pos' => $isPos, 'is_Mobileassistantpro' => 0);
        $result->setData($sucessArray);
        return $result;
    }

    /**
     * [getPrice function for the set the convert price ]
     * @param  [type] $price         [current product price]
     * @param  [type] $storeId       [product store id]
     * @param  [type] $orderCurrency [current store currency code]
     * @return [type]                [return price with the currency symbol]     *
     */
    
    public function getPrice($price, $storeId, $orderCurrency)
    {
        $currencyCode = $orderCurrency;
        if ($orderCurrency == null) {
            $store = $this->storeModel;
            $store->load($storeId);
            $price = $store->roundPrice($store->convertPrice($price));
            $currencyCode = $this->storeManager->getStore()->getCurrentCurrencyCode();
        }
        return $price;
    }

    /**
     * [getPriceFormat function for the format the price for the decimal point]
     * @param  [type] $price [product price]
     * @return [type]        [return price with formatted]
     */
    
    public function getPriceFormat($price)
    {
        $price = sprintf("%01.2f", $price);
        return $price;
    }

    /**
     * [getformattedPrice for the get the currency with symbol]
     * @param  [type] $price [product price]
     * @return [type]        [price return with the formatted]
     */
    
    public function getformattedPrice($price)
    {
        $priceHelper = $this->pricingHelper;
        $value =  $price;
        $formattedCurrencyValue = $priceHelper->currency($value, true, false);
        return $formattedCurrencyValue;
    }

    /**
     * [orderFormattedPrice for the get the price of order with currency sign]
     * @param  [type] $price        [current product price]
     * @param  [type] $currencyCode [current store currencycode]
     * @return [type]               [return price with the currency symbol]
     */
    
    public function orderFormattedPrice($price, $currencyCode)
    {
        $this->ordercurrency = $this->dirCurrencyFactory->create()->load($currencyCode);
        $order_currencySymbol = $this->ordercurrency->getCurrencySymbol();
        $formattedorderprice = $this->ordercurrency->formatTxt($price, array());
        return $formattedorderprice;
    }

    /**
     * [getDataInfo for the get the system configuration value of extension active or not]
     * @return [type] [return the bool for the extension is enabled or not]
     */
    public function getDataInfo()
    {
        $data = $this->scopeConfig->getValue(self::XML_PATH_DATA, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return json_decode(base64_decode($this->encryptor->decrypt($data)));
    }

    /**
     * [getFormatUrl for the format the url at the time of the save setting ]
     * @param  [type] $url [current instance url]
     * @return [type]      [return domain]
     */
    
    public function getFormatUrl($url)
    {
        $input = trim($url, '/');
        if (!preg_match('#^http(s)?://#', $input)) {
            $input = 'http://' . $input;
        }
        $urlParts = parse_url($input);
        if (isset($urlParts['path'])) {
            $domain = preg_replace('/^www\./', '', $urlParts['host'] . $urlParts['path']);
        } else {
            $domain = preg_replace('/^www\./', '', $urlParts['host']);
        }
        return $domain;
    }

    /**
     * [getAllWebsites for the fetch the all websties of the current instance]
     * @return [type] [return the all website list of current instance]
     */
    
    public function getAllWebsites()
    {
        $value = $this->scopeConfig->getValue(self::XML_PATH_INSTALLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if (!$value) {
            return array();
        }
        $data = $this->scopeConfig->getValue(self::XML_PATH_DATA, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $web = $this->scopeConfig->getValue(self::XML_PATH_WEBSITES, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $websites = explode(',', str_replace($data, '', $this->encryptor->decrypt($web)));
        $websites = array_diff($websites, array(""));
        return $websites;
    }

    /**
     * [getConfig for the get the system configuration value]
     * @param  [type] $configPath [path of the system config element]
     * @return [type]             [return the value of the give element key]
     */
    
    public function getConfig($configPath)
    {
        return $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * [getAllStoreDomains for the get the all multiwebsite doamins list]
     * @return [type] [return all domains of multi website]
     */
    
    public function getAllStoreDomains()
    {
        $domains = array();
        foreach ($this->storeManager->getWebsites() as $website) {
            $url = $website->getConfig('web/unsecure/base_url');
            if ($domain = trim(preg_replace('/^.*?\/\/(.*)?\//', '$1', $url))) {
                $domains[] = $domain;
            }
            $url = $website->getConfig('web/secure/base_url');
            if ($domain = trim(preg_replace('/^.*?\/\/(.*)?\//', '$1', $url))) {
                $domains[] = $domain;
            }
        }
        return array_unique($domains);
    }
    
    /**
     * [pushNotification use for the send the pushnotification to the application user]
     * @param  [type] $notificationType [this indicate the product,review,order or customer event]
     * @param  [type] $entityId         [entity id use for the order notification for load order]
     * @param  [type] $params           [params use for the get the product details]
     * @return [type]                   [it fire the notification for particular event to the given device]
     */
    
    public function pushNotification($notificationType, $entityId, $params = null)
    {
        $googleApiKey = $this->scopeConfig->getValue(self::XML_GOOGLE_KEY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $passPhrase = 'push2magento';
        $collections = $this->mobileassistantCollection;
        $collections->addFieldToFilter('notification_flag', array('eq' => 1))->addFieldToFilter('is_logout', array('eq' => 0));
        if ($notificationType == 'customer') {
            $notificationData = array();
            $message = $this->getConfig('mobileassistant/mobileassistant_general/customer_register_notification_msg');
            if ($message == null) {
                $message = 'A New customer has been registered on the Store.';
            }
        } elseif ($notificationType == 'order') {
            $order = $order = $this->orderModel->loadByAttribute('increment_id', $entityId);
            $msgString = $this->getConfig('mobileassistant/mobileassistant_general/notification_msg');
            if ($msgString == null) {
                $msgString = 'A New order has been received on the Store.';
            }
            $entityId = $order->getEntityId();
            $message = $msgString . "\nOrder Id: " . $order->getIncrementId() . "\nGrand Total: " . $this->getPrice($order->getGrandTotal(), $order->getStoreId(), $order->getOrderCurrencyCode());
        } elseif ($notificationType == 'product') {
            $msgString = $this->getConfig('mobileassistant/mobileassistant_general/product_inventory_notification_msg');
            if ($msgString == null) {
                $msgString = 'Product Stock Alert';
            }
            $message = $msgString . "\nName: " . $params['name'] . "\nCurrent Qty: " . $params['qty'];
        }
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = $this->scopeConfig->getValue(self::XML_IOS_KEY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        foreach ($collections as $collection) {
            $deviceType = $collection->getDeviceType();

            if ($deviceType == 'ios') {
                $deviceToken = $collection->getDeviceToken();
                $iosNotificationArray = array("to" => $deviceToken, "priority" => "high", "notification" => array("body" => $message, "sound" => "default"), "data" => array("type" => $notificationType, "entity_id" => $entityId));

                $json = json_encode($iosNotificationArray);
                $headers = array(
                    'Content-Type:application/json',
                    'Authorization:key='.$serverKey
                );

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

                $result = curl_exec($ch);
                curl_close($ch);
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log'); 
                $logger = new \Zend\Log\Logger(); 
                $logger->addWriter($writer); 
                $logger->info('IOS Notification');
                $logger->info($result);
            } elseif ($deviceType == 'android') {
                $deviceToken = $collection->getDeviceToken();
                $registrationIds = array($deviceToken);
                $msgArray = array(
                    'message' => $message,
                    'entity_id' => $entityId,
                    'type' => $notificationType
                );

                $fields = array(
                    'registration_ids' => $registrationIds,
                    'data' => $msgArray
                );

                $headers = array(
                    'Authorization: key=' . $googleApiKey,
                    'Content-Type: application/json'
                );

                $ch = curl_init();
                // curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                curl_close($ch);
            }
        }
        return true;
    }
    
    /**
     * [getHeaderInfo use for the create Header if already headler function is not there]
     * @return [type] [reutrn headers value]
     */
    
    public function getHeaderInfo()
    {
        if (!function_exists('getallheaders')) {
            function getallheaders()
            {
                $headers = array();
                foreach ($_SERVER as $name => $value) {
                    if (substr($name, 0, 5) == 'HTTP_') {
                        $headers[str_replace(' ', '-', strtolower(str_replace('_', ' ', substr($name, 5))))] = $value;
                    } else {
                        $headers[$name] = $value;
                    }
                }
                return $headers;
            }
        }
    }

    /**
     * [getHeaders use for the get the all requested call headers]
     * @return [type] [retuen headers value]
     */
    
    public function getHeaders()
    {
        $allHeaders = array();
        $headersSecretKey = "";
        $this->getHeaderInfo();
        $allHeaders = getallheaders();
        if (!array_key_exists(self::KEY, $allHeaders)) {
            if (array_key_exists('Secretkey', $allHeaders)) {
                $headersSecretKey = $allHeaders['Secretkey'];
            }
            $allHeaders[self::KEY] = $headersSecretKey;
            unset($allHeaders['Secretkey']);
        }
        $orignalValue = md5(self::SECRET);
        $key = strtolower(self::KEY);
        foreach ($allHeaders as $name => $value) {
            if (array_key_exists(self::KEY, $allHeaders)) {
                if ($name == self::KEY) {
                    if (isset($value)) {
                        if (strcasecmp($value, $orignalValue) == 0) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        }
    }

    /**
     * [getExtensionVersion for the get the version number of the given extension]
     * @return [type] [return the given extension current version]
     */
    
    public function getExtensionVersion()
    {
        $moduleInfo =  $this->moduleList->getOne('Biztech_Mobileassistant');
        return $moduleInfo['setup_version'];
    }
}
