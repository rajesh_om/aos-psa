<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Model;

use Magento\Framework\Model\AbstractModel;

class Mobileassistant extends AbstractModel
{

    /**
     * Define resource model
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Biztech\Mobileassistant\Model\Cresource\Mobileassistant');
    }
}
