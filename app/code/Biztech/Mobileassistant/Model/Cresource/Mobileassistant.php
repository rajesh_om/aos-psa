<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Model\Cresource;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Mobileassistant extends AbstractDb
{

    /**
     * Define main table
     */
    protected function _construct()
    {
        $this->_init('mobileassistant', 'user_id');
    }
}
