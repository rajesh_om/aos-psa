<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Model\Cresource\Mobileassistant;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Biztech\Mobileassistant\Model\Mobileassistant',
            'Biztech\Mobileassistant\Model\Cresource\Mobileassistant'
        );
    }
}
