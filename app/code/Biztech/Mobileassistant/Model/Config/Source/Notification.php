<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Model\Config\Source;

class Notification implements \Magento\Framework\Option\ArrayInterface
{
    protected $mobileassistantHelper;

    /**
     * @param \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
     */
    public function __construct(
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
    ) {
        $this->_mobileassistantHelper = $mobileassistantHelper;
    }

    /**
     * This function allows to kind of notification type.
     * @return Array
     */
    public function toOptionArray()
    {
        $optionArray = array(
            array(
                'value' => 'order_notification',
                'label' => 'Order Notification',
            ),
            array(
                'value' => 'product_notification',
                'label' => 'Product Notification',
            ),
            array(
                'value' => 'customer_notification',
                'label' => 'Customer Notification',
            ));

        return $optionArray;
    }
}
