<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Model\Config\Source;

class Enabledisable implements \Magento\Framework\Option\ArrayInterface
{
    protected $mobileassistantHelper;

    /**
     * @param \Biztech\Mobileassistant\Helper\Data $helperData
     */
    public function __construct(
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
    ) {
        $this->_mobileassistantHelper = $mobileassistantHelper;
    }

    /**
     * This function is used for allow option of the Yes and No
     * @return Array
     */
    public function toOptionArray()
    {
        $options = array(
            array('value' => 0, 'label' => __('No')),
        );
        $websites = $this->_mobileassistantHelper->getAllWebsites();
        if (!empty($websites)) {
            $options[] = array('value' => 1, 'label' => __('Yes'));
        }
        return $options;
    }
}
