<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Block\Config;

use Magento\Backend\Block\Template\Context;

class Baseurl extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $storeManagerInterface;
    protected $http;

    /**
     * @param Context                             $context
     * @param \Magento\Framework\App\Request\Http $http
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Request\Http $http
    ) {
        $this->_storeManagerInterface = $context->getStoreManager();
        $this->_http = $http;
        return parent::__construct($context);
    }

    /**
     * _getElementHtml use for check baseurl for activation is proper
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = '';
        $storeManager = $this->_storeManagerInterface;
        $currentStore = $storeManager->getStore();
        $request = $this->_http;

        if ($storeId = $request->getParam('store')) {
            $url = $storeManager->getStore($storeId)->getBaseUrl();
            $html = '<span style="color:blue;">' . $url . '</span>';
        } elseif ($request->getParam('website')) {
            $websiteId = $request->getParam('website');
            $storeId = $storeManager->getWebsite($websiteId)->getDefaultStore()->getId();
            $url = $storeManager->getStore($storeId)->getBaseUrl();
            $html = '<span style="color:blue;">' . $url . '</span>';
        } else {
            $defaultId = array();
            $websites = $storeManager->getWebsites();
            if (count($websites) > 1) {
                foreach ($websites as $website) {
                    $defaultId[] = $website->getDefaultGroup(true)->getDefaultStoreId();
                }
                $i = 1;
                foreach ($defaultId as $storeId) {
                    $url = $storeManager->getStore($storeId)->getBaseUrl(); // by default: URL_TYPE_LINK is returned
                    $html = $html . 'Website ' . $i . ': <span style="color:blue;">' . $url . '</span> </br>';
                    $i++;
                }
            } else {
                $storeId = $storeManager->getWebsite(true)->getDefaultGroup()->getDefaultStoreId();
                $url = $storeManager->getStore($storeId)->getBaseUrl();
                $html = '<span style="color:blue;">' . $url . '</span>';
            }
        }
        return $html;
    }
}
