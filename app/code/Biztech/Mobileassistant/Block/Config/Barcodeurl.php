<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Block\Config;

use Magento\Backend\Block\Template\Context;

class Barcodeurl extends \Magento\Config\Block\System\Config\Form\Field
{
    protected $storeManagerInterface;
    protected $http;

    /**
     * @param Context                             $context
     * @param \Magento\Framework\App\Request\Http $http
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Request\Http $http
    ) {
        $this->_storeManagerInterface = $context->getStoreManager();
        $this->_http = $http;
        return parent::__construct($context);
    }
    /**
     * _getElementHtml use for generate the barcode for the baseurl
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = '';
        $storeManager = $this->_storeManagerInterface;
        $currentStore = $storeManager->getStore();
        $request = $this->_http;

        if ($storeId = $request->getParam('store')) {
            $url = $storeManager->getStore($storeId)->getBaseUrl();
        } elseif ($request->getParam('website')) {
            $websiteId = $request->getParam('website');
            $storeId = $storeManager->getWebsite($websiteId)->getDefaultStore()->getId();
            $url = $storeManager->getStore($storeId)->getBaseUrl();
        } else {
            $defaultId = array();
            $websites = $storeManager->getWebsites();
            $url = array();
            if (count($websites) > 1) {
                foreach ($websites as $website) {
                    $defaultId[] = $website->getDefaultGroup(true)->getDefaultStoreId();
                }
                foreach ($defaultId as $storeId) {
                    $url[] = $storeManager->getStore($storeId)->getBaseUrl(); // by default: URL_TYPE_LINK is returned
                }
            } else {
                $storeId = $storeManager->getWebsite(true)->getDefaultGroup()->getDefaultStoreId();
                $url[] = $storeManager->getStore($storeId)->getBaseUrl();
            }
        }
        if (is_array($url)) {
            $html = $html . '<div style="width: 615px ! important;">';
            for ($i = 0; $i < count($url); $i++) {
                $html = $html . '<img style="margin-left: 5px;" title="' . $url[$i] . ' " src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=';
                $html = $html . urlencode($url[$i]) . '" />';
            }
            $html = $html . '</div>';
        } else {
            $html = '<img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl="' . urlencode($url) . '"&choe=UTF-8" />';
        }

        return $html;
    }
}
