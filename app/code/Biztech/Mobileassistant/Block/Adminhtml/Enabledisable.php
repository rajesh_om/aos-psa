<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Block\Adminhtml;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Enabledisable extends Field
{
    const XML_PATH_ACTIVATION = 'mobileassistant/activation/key';

    protected $mobileassistanthelper;
    protected $resourceConfig;
    protected $web;
    protected $store;
    protected $request;

    /**
     * @param \Magento\Backend\Block\Template\Context    $context
     * @param \Biztech\Mobileassistant\Helper\Data       $mobileassistanthelper
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Store\Model\Website               $web
     * @param \Magento\Store\Model\Store                 $store
     * @param \Magento\Framework\App\Request\Http        $Request
     * @param array                                      $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Biztech\Mobileassistant\Helper\Data $mobileassistanthelper,
        \Magento\Config\Model\ResourceModel\Config $resourceConfig,
        \Magento\Store\Model\Website $web,
        \Magento\Store\Model\Store $store,
        \Magento\Framework\App\Request\Http $Request,
        $data = array()
    ) {
        $this->_mobileassistanthelper = $mobileassistanthelper;
        $this->_storeManager = $context->getStoreManager();
        $this->_web = $web;
        $this->_resourceConfig = $resourceConfig;
        $this->_store = $store;
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_request = $Request;
        parent::__construct($context, $data);
    }

    /**
     * _getElementHtml use for check the multidomains
     * @param  AbstractElement $element
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $websites = $this->_mobileassistanthelper->getAllWebsites();

        if (!empty($websites)) {
            $websiteId = $this->getRequest()->getParam('website');
            $website = $this->_web->load($websiteId, 'code');
            if ($website && in_array($website->getWebsiteId(), $websites)) {
                $html = $element->getElementHtml();
            } elseif (!$websiteId) {
                $html = $element->getElementHtml();
            } else {
                $html = sprintf('<strong class="required">%s</strong>', __('Please buy additional domains'));
            }
        } else {
            $websiteCode = $this->_request->getParam('website');
            $websiteId = $this->_store->load($websiteCode)->getWebsiteId();
            $isenabled = $this->_storeManager->getWebsite($websiteId)->getConfig('mobileassistant/activation/key');
            if ($isenabled != null || $isenabled != '') {
                $html = sprintf('<strong class="required">%s</strong>', __('Please select a website'));
                $moduleStatus = $this->_resourceConfig;
                $moduleStatus->saveConfig('mobileassistant/mobileassistant_general/active', 0, 'default', 0);
            } else {
                $html = sprintf('<strong class="required">%s</strong>', __('Please enter a valid key'));
            }
        }

        return $html;
    }
}
