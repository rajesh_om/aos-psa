<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Block\Adminhtml\Config\Form\Renderer;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

class Website extends Field
{
    protected $mobileassistantHelper;
    protected $encryptor;

    /**
     * @param \Magento\Backend\Block\Template\Context          $context
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Biztech\Mobileassistant\Helper\Data             $mobileassistantHelper
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        $data = array()
    ) {
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_encryptor = $encryptor;
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_storeManager = $context->getStoreManager();
        parent::__construct($context, $data);
    }

    /**
     * _getElementHtml use for verify the website activation key
     * @param  AbstractElement $element
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $html = '';
        $data = $this->_scopeConfig->getValue('mobileassistant/activation/data');
        $eleValue = explode(',', str_replace($data, '', $this->_encryptor->decrypt($element->getValue())));
        $eleName = $element->getName();
        $eleId = $element->getId();
        $element->setName($eleName . '[]');
        $dataInfo1 = $this->_mobileassistantHelper->getDataInfo();
        $dataInfo = (array) $dataInfo1;

        if (isset($dataInfo['dom']) && intval($dataInfo['c']) > 0 && intval($dataInfo['suc']) == 1) {
            foreach ($this->_storeManager->getWebsites() as $website) {
                $url = $this->_scopeConfig->getValue('web/unsecure/base_url');
                $url = $this->_mobileassistantHelper->getFormatUrl(trim(preg_replace('/^.*?\/\/(.*)?\//', '$1', $url)));
                foreach ($dataInfo['dom'] as $web) {
                    if ($web->dom == $url && $web->suc == 1) {
                        $element->setChecked(false);
                        $id = $website->getId();
                        $name = $website->getName();
                        $element->setId($eleId . '_' . $id);
                        $element->setValue($id);
                        if (in_array($id, $eleValue) !== false) {
                            $element->setChecked(true);
                        }
                        if ($id != 0) {
                            $html .= '<div><label>' . $element->getElementHtml() . ' ' . $name . ' </label></div>';
                        }
                    }
                }
            }
        } else {
            $html = sprintf('<strong class="required">%s</strong>', __('Please enter a valid key'));
        }

        return $html;
    }
}
