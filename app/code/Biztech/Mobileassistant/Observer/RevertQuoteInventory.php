<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class RevertQuoteInventory implements ObserverInterface
{
    protected $mobileassistantHelper;
    protected $stockRegistryInterface;

    /**
     * @param \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
     * @param StockRegistryInterface               $stockRegistryInterface
     */
    public function __construct(
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        StockRegistryInterface $stockRegistryInterface
    ) {
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_stockRegistryInterface = $stockRegistryInterface;
    }

    /**
     * This function used for send notification based on low product stock
     * @param  \Magento\Framework\Event\Observer $observer
     * @return Void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            $quote = $observer->getEvent()->getQuote();
            
            foreach ($quote->getAllItems() as $item) {
                $params = array();
                $params['product_id'] = $item->getProductId();
                $params['name'] = $item->getName();
                $params['qty'] = $this->_stockRegistryInterface->getStockItem($item->getProductId(), $item->getStore()->getWebsiteId())->getQty() + $item->getTotalQty();
                $minQty = $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/minimum_qty');
                $data = array();
                $data['status'] = $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/notification');
                $statuses = explode(",", $data['status']);
                
                if (in_array('product_notification', $statuses)) {
                    if (($params['qty']) <= $minQty) {
                        $this->_mobileassistantHelper->pushNotification('product', $params['product_id'], $params);
                    }
                }
            }
        }
    }
}
