<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomerRegisterNotification implements ObserverInterface
{
    protected $mobileassistantHelper;

    /**
     * @param \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
     */
    public function __construct(
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
    ) {
        $this->_mobileassistantHelper = $mobileassistantHelper;
    }

    /**
     * This function used for send notification on new customer registration
     * @param \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            $customer = $observer->getEvent()->getCustomer();
            if ($customer) {
                $customerId = $customer->getId();
            }

            if ($customerId) {
                $data = array();
                $data['status'] = $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/notification');
                $statuses = explode(",", $data['status']);
                if (in_array('customer_notification', $statuses)) {
                    $result = $this->_mobileassistantHelper->pushNotification('customer', $customerId);
                }
            }
        }
    }
}
