<?php

namespace Biztech\Mobileassistant\Observer;

use Magento\Framework\Event\ObserverInterface;

class ReviewNotification implements ObserverInterface {

	protected $_helper;
	protected $_requestInterface;

	public function __construct(
		\Biztech\Mobileassistant\Helper\Data $helper,
		\Magento\Framework\App\RequestInterface $requestInterface
	) {
		$this->_helper = $helper;
		$this->_requestInterface = $requestInterface;
	}

	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		if ($this->_helper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
			$requestInterface = $this->_requestInterface;
			
			$object = $observer->getEvent()->getObject();
			$statusId = $object->getStatusId();
			if ($requestInterface->getFullActionName() == 'review_product_post') {
				if ($statusId) {
					$data = array();
					$data['status'] = $this->_helper->getConfig('mobileassistant/mobileassistant_general/notification');
					$statuses = explode(",", $data['status']);					
					if (in_array('review_notification', $statuses)) {						
						$this->_helper->pushNotification('review', $statusId, NULL);
					}
				}
			}
		}
	}
}
