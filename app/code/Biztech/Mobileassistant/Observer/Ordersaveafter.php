<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Observer;

use Magento\Framework\Event\ObserverInterface;

class Ordersaveafter implements ObserverInterface
{
    protected $mobileassistantHelper;
    protected $request;

    /**
     * @param \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper
     * @param \Magento\Framework\App\Request\Http  $request
     */
    public function __construct(
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_request = $request;
    }

    /**
     * This function is used for send the notification on order place
     * @param  \Magento\Framework\Event\Observer $observer
     * @return Void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            $data = array();
            $data['status'] = $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/notification');
            $statuses = explode(",", $data['status']);
            if (in_array('order_notification', $statuses)) {
                $result = $this->_mobileassistantHelper->pushNotification('order', $observer->getOrder()->getIncrementId());
            }
        }
    }
}
