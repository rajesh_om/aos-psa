<?php
/**
 * Copyright © Biztech, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Biztech\Mobileassistant\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\ProductFactory;

class CatalogInventorySave implements ObserverInterface
{
    protected $mobileassistantHelper;
    protected $productFactory;

    public function __construct(
        \Biztech\Mobileassistant\Helper\Data $mobileassistantHelper,
        ProductFactory $productFactory
    ) {
        $this->_mobileassistantHelper = $mobileassistantHelper;
        $this->_productFactory = $productFactory;
    }

    /**
     * This function is used for send the notification on qty low event
     * @param  \Magento\Framework\Event\Observer $observer
     * @return Void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/active') == 1) {
            $product = $observer->getEvent()->getProduct();
            // $this->getSourceQty($product->getSku());
            // $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            // $sourceWiseData = $objectManager->get('Magento\Inventory\Model\SourceItem\Command\GetSourceItemsBySku')->execute($product->getSku());

            // foreach ($sourceWiseData as $key => $sourceData) {
            //     $finalSourceData[] = $sourceData->getData();
            // }
            // echo "<pre>";
            // print_r($finalSourceData);
            // die();
            $stockItemData = $product->getStockData();
            $params = array();
            $params['product_id'] = $product->getId();
            $params['name'] = $this->_productFactory->create()->load($params['product_id'])->getName();

            if (isset($stockItemData['qty'])) {
                $params['qty'] = $stockItemData['qty'];
                $minQty = $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/minimum_qty');

                $data = array();
                $data['status'] = $this->_mobileassistantHelper->getConfig('mobileassistant/mobileassistant_general/notification');
                $statuses = explode(",", $data['status']);

                if (in_array('product_notification', $statuses)) {
                    if ($params['qty'] <= $minQty) {
                        $this->_mobileassistantHelper->pushNotification('product', $params['product_id'], $params);
                    }
                }
            }
        }
    }
    private function getSourceQty($productSku)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productSourceData = array();
        $finalSourceData = array();
        if ($this->allowMultiSource()) {
            $sourceWiseData = $this->_productSourceCommand->execute($productSku);
            $count = 0;
            foreach ($sourceWiseData as $key => $sourceData) {
                $count++;
                $sourceModelObject = $objectManager->get('Magento\Inventory\Model\Source')->load($sourceData['source_code']);
                $lowNotificationData = $objectManager->get('Magento\InventoryLowQuantityNotification\Model\ResourceModel\SourceItemConfiguration\GetData')->execute($sourceData['source_code'], $productSku);
                if (isset($lowNotificationData['notify_stock_qty'])) {
                    $notifyStockQty = $lowNotificationData['notify_stock_qty'];
                    $lowStockQtyDefault = false;
                } else {
                    $notifyStockQty = 1;
                    $lowStockQtyDefault = true;
                }
                if ($sourceModelObject->getEnabled() == 1) {
                    $source_status = 'Enabled';
                } else {
                    $source_status = 'Disabled';
                }
                if ($sourceData['status'] == 1) {
                    $status = 'In Stock';
                } else {
                    $status = 'Out Of Stock';
                }
                $finalSourceData[] = array(
                    'source_item_id' => $sourceData['source_item_id'],
                    'source_code' => $sourceData['source_code'],
                    'source_name' => $sourceModelObject->getName(),
                    'source_status' => $source_status,
                    'status' => $sourceData['status'],
                    'sku' => $sourceData['sku'],
                    'quantity' => $sourceData['quantity'],
                    'low_stock_qty' => $notifyStockQty,
                    'low_stock_qty_default' => $lowStockQtyDefault,
                );
            }
            return $finalSourceData;
        } else {
            return $finalSourceData;
        }
    }

    private function allowMultiSource()
    {
        $allowMultiSource = 0;
        $sourceCollection = $this->_sourceModel->getCollection();
        $sourceCollection->addFieldToFilter('enabled', 1);
        // $sourceCollection->addFieldToFilter('source_code', ['neq' => 'default']);
        if (!empty($sourceCollection->getData()) && count($sourceCollection->getData()) >= 1) {
            $allowMultiSource = true;
        } else {
            $allowMultiSource = false;
        }
        return $allowMultiSource;
    }
}
