<?php

namespace TNW\Aos\Model;

class AosHelper
{
    /**
     * Delete non unique values
     *
     * @param $array
     * @param $key
     * @return array
     */
    public function uniqueMiltiDimentionalArray($array, $key) {
        $tempArray = [];
        $i = 0;
        $keyArray = [];

        foreach($array as $val) {
            if (!in_array($val[$key], $keyArray)) {
                $keyArray[$i] = $val[$key];
                $tempArray[$i] = $val;
            }
            $i++;
        }
        return $tempArray;
    }
}
