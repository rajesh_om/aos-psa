<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */

namespace TNW\Subscriptions\Block\Adminhtml\SubscriptionProfile\Attribute\Edit\Tab;

/**
 * Subscription profile attribute add/edit form options tab.
 *
 * @method \TNW\Subscriptions\Block\Adminhtml\SubscriptionProfile\Attribute\Edit\Tab\Options setReadOnly(bool $value)
 * @method null|bool getReadOnly()
 */
class Options extends \Magento\Eav\Block\Adminhtml\Attribute\Edit\Options\AbstractOptions
{

}
