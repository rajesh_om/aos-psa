/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */
var config = {
    'config':{
        'mixins': {
            'Magento_Ui/js/form/element/abstract': {
                'TNW_Subscriptions/js/form/element/abstract/preview': true
            },
            'Magento_Ui/js/form/element/multiselect': {
                'TNW_Subscriptions/js/form/element/multiselect/preview': true
            }
        }
    }
};
