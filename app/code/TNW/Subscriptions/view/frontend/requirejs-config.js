/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */

var config = {
    map: {
        '*': {
            tnwSubscribePrice: 'TNW_Subscriptions/js/product/subscribe-price',
            tnwSubscribeShipment: 'TNW_Subscriptions/js/subscription-profile/shipment',
            tnwSubscribeShipmentDetails: 'TNW_Subscriptions/js/subscription-profile/shipment-details',
            tnwSubscribeBilling: 'TNW_Subscriptions/js/subscription-profile/billing',
            tnwSubscribeListButtons: 'TNW_Subscriptions/js/product/list/subscribe-list-buttons',
        }
    },
    config: {
        mixins: {
            'Magento_Checkout/js/model/quote' : {
                'TNW_Subscriptions/js/checkout/model/quote-mixin' : true
            },
            'Magento_Checkout/js/view/minicart' : {
                'TNW_Subscriptions/js/checkout/view/minicart-mixin' : true
            },
            'Magento_Checkout/js/view/summary/item/details/subtotal' : {
                'TNW_Subscriptions/js/checkout/view/summary/item/details/subtotal-mixin' : true
            },
            'Magento_Checkout/js/view/summary/cart-items' : {
                'TNW_Subscriptions/js/checkout/view/summary/cart-items-mixin' : true
            },
            'Magento_Swatches/js/swatch-renderer' : {
                'TNW_Subscriptions/js/swatch-renderer-mixin' : true
            },
            'Magento_Catalog/js/catalog-add-to-cart' : {
                'TNW_Subscriptions/js/catalog-add-to-cart-mixin' : true
            },
            'Magento_ConfigurableProduct/js/configurable' : {
                'TNW_Subscriptions/js/configurable-mixin' : true
            },

        }
    }
};
