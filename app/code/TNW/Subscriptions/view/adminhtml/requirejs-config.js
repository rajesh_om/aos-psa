/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */

var config = {
    map: {
        '*': {
            currency_percent: 'TNW_Subscriptions/js/system/config/currency_percent',
            orderProfileLink: 'TNW_Subscriptions/js/profile/order/link'
        }
    }
};
