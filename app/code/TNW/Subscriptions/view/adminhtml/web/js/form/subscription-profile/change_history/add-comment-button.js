/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */

define([
    'Magento_Ui/js/form/components/button',
    'jquery',
    'uiRegistry'
], function (Button, $, registry) {
    'use strict';

    return Button.extend({

        defaults: {
            displayPrimary: true,
            changeHistoryGridIndex: null
        },

        /** @inheritdoc */
        initObservable: function () {
            return this._super()
                .observe([
                    'disabled',
                    'displayPrimary',
                    'subButtonLeft',
                    'subButtonRight'
                ]);
        },

        /**
         * @inheritdoc
         */
        action: function () {
            var url = typeof this.imports.url === "undefined" ? '' : this.imports.url,
                comment = this.source.get(this.parentScope + '.comment_area'),
                notify = this.source.get(this.parentScope + '.comment_notify');

            this.sendAjaxAddComment(url, comment, notify)
        },

        /**
         * Send ajax and assign subscriptions to existing customer.
         *
         * @param url
         * @param comment
         *
         * @param notify
         * @return void
         */
        sendAjaxAddComment: function (url, comment, notify) {
            var _self = this;
            $.ajax({
                showLoader: true,
                url: url,
                data: {
                    form_key: window.FORM_KEY,
                    comment: comment,
                    notify: notify
                },
                type: "POST",
                dataType: 'json'
            }).done(function (data) {
                if (data.result) {
                    $("textarea[name='change_history[comment_area]").val('');
                    _self.reloadOrderHistoryChangeGrid();
                }
            })
        },

        /**
         * Update order change history grid.
         *
         * @return void
         */
        reloadOrderHistoryChangeGrid: function () {
            if (this.changeHistoryGridIndex) {
                var params = [];
                var target = registry.get(this.changeHistoryGridIndex);
                if (target && typeof target === 'object') {
                    target.set('params.t ', Date.now());
                }
            }
        }
    });
});
