<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */

namespace TNW\Subscriptions\Controller\Ui\Render;

use Magento\Ui\Controller\Adminhtml\Index\Render\Handle as MagentoHandle;

/**
 * Class Handle
 */
class Handle extends MagentoHandle
{

}