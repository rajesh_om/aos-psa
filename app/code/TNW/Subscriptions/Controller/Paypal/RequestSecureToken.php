<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */

namespace TNW\Subscriptions\Controller\Paypal;

use TNW\Subscriptions\Controller\Adminhtml\Paypal\RequestSecureToken as Base;

/**
 * Controller to get a secure token from PayPal.
 */
class RequestSecureToken extends Base
{

}
