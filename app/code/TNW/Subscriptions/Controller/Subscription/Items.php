<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */
namespace TNW\Subscriptions\Controller\Subscription;

/**
 * Controller for subscription items at customer account dashboard.
 */
class Items extends \TNW\Subscriptions\Controller\Subscription\AbstractView
{

}
