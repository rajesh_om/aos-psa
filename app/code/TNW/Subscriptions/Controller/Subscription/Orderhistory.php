<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */
namespace TNW\Subscriptions\Controller\Subscription;

/**
 * Controller for subscription order history at customer account dashboard.
 */
class Orderhistory extends \TNW\Subscriptions\Controller\Subscription\AbstractView
{

}
