<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */
namespace TNW\Subscriptions\Model\Payment;

/**
 * Class VoidDataBuilder
 * @package TNW\Subscriptions\Model\Payment
 */
class VoidDataBuilder
{
    /**
     * @param $data
     * @return mixed
     */
    public function build($data)
    {
        return $data;
    }
}
