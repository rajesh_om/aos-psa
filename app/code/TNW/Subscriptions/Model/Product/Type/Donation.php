<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */

namespace TNW\Subscriptions\Model\Product\Type;

/**
 * Donation product type implementation
 */
class Donation extends \Magento\Catalog\Model\Product\Type\Virtual
{

}
