<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */

namespace TNW\Subscriptions\Model\SubscriptionProfile\Process;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class Response Pool
 */
class ResponsePool implements PoolInterface
{
    /**
     * @var array
     */
    protected $processors = [];

    /**
     * @var array
     */
    protected $processorsInstances = [];

    /**
     * Object manager.
     *
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * Pool constructor.
     * @param ObjectManagerInterface $objectManager
     * @param array $processors
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        array $processors
    ) {
        $this->objectManager = $objectManager;
        $this->processors = $processors;
    }

    /**
     * {@inheritdoc}
     */
    public function getProcessors()
    {
        return $this->processors;
    }

    /**
     * {@inheritdoc}
     */
    public function getProcessorsInstances()
    {
        if (!$this->processorsInstances) {
            foreach ($this->processors as $modifier) {
                if (empty($modifier['class'])) {
                    throw new LocalizedException(__('Parameter "class" must be present.'));
                }

                if (empty($modifier['sortOrder'])) {
                    throw new LocalizedException(__('Parameter "sortOrder" must be present.'));
                }

                $modifierObject = $this->objectManager->create($modifier['class']);
                if (!$modifierObject instanceof ResponseInterface) {
                    throw new \InvalidArgumentException(
                        'Type "' . $modifier['class'] . '" is not instance on ' . ResponseInterface::class
                    );
                }
                $this->processorsInstances[$modifier['class']] = $modifierObject;
            }
        }

        return $this->processorsInstances;
    }
}
