<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */

namespace TNW\Subscriptions\Model\SubscriptionProfile\Edit\Response\Save\Profile;

/**
 * Modified products response processor.
 */
class ModifiedProducts extends Base
{
    /**
     * @inheritdoc
     */
    public function process(array $data)
    {
        $result = [];

        return $result;
    }
}
