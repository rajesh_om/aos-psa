<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */
namespace TNW\Subscriptions\Model;

use TNW\Subscriptions\Model\Source\ProfileStatus;

/**
 * Class EmailNotifier
 * @package TNW\Subscriptions\Model
 */
class EmailNotifier
{
    const XML_PATH_EMAIL_IDENTITY = 'tnw_subscriptions_profile_options/emails/email_identity';
    const XML_PATH_RENEWAL_NOTIFICATION_PERIOD = 'tnw_subscriptions_profile_options/notifications/renewals';
    const XML_PATH_EXPIRED_CARD_NOTIFICATION_PERIOD = 'tnw_subscriptions_profile_options/notifications/expired_card';

    const XML_PATH_STATUS_CHANGE_TEMPLATE = 'tnw_subscriptions_profile_options/emails/profile_status_change';
    const XML_PATH_COMMENT_ADDED_TEMPLATE = 'tnw_subscriptions_profile_options/emails/comment_added';
    const XML_PATH_CARD_EXPIRE = 'tnw_subscriptions_profile_options/emails/card_expire';
    const XML_PATH_PAYMENT_FAILED = 'tnw_subscriptions_profile_options/emails/payment_failed';
    const XML_PATH_OUT_OF_STOCK = 'tnw_subscriptions_profile_options/emails/out_of_stock';
    const XML_PATH_RENEWAL = 'tnw_subscriptions_profile_options/emails/renewal';

    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var Source\ProfileStatusFactory
     */
    protected $profileStatusFactory;

    /**
     * @var \TNW\Subscriptions\Api\SubscriptionProfileRepositoryInterface
     */
    protected $subscriptionProfileRepository;
    /**
     * @var SubscriptionProfileOrder\Manager
     */
    private $profileOrderManager;

    /**
     * EmailNotifier constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param Source\ProfileStatusFactory $profileStatusFactory
     * @param \TNW\Subscriptions\Api\SubscriptionProfileRepositoryInterface $subscriptionProfileRepository
     * @param SubscriptionProfileOrder\Manager $profileOrderManager
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        Source\ProfileStatusFactory $profileStatusFactory,
        \TNW\Subscriptions\Api\SubscriptionProfileRepositoryInterface $subscriptionProfileRepository,
        SubscriptionProfileOrder\Manager $profileOrderManager
    ) {
        $this->subscriptionProfileRepository = $subscriptionProfileRepository;
        $this->profileStatusFactory = $profileStatusFactory;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->inlineTranslation = $inlineTranslation;
        $this->profileOrderManager = $profileOrderManager;
    }

    /**
     * @param $subscriptionProfile
     * @param $oldStatus
     * @param $newStatus
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function profileStatusChange($subscriptionProfile, $oldStatus, $newStatus)
    {
        if ($this->checkEmailTemplateSetting(self::XML_PATH_STATUS_CHANGE_TEMPLATE)) {
            $customer = $subscriptionProfile->getCustomer();
            $statusModel = $this->profileStatusFactory->create();
            $date = $this->getNextProfileRelation($subscriptionProfile)
                ? date('F jS, Y', strtotime($this->getNextProfileRelation($subscriptionProfile)->getScheduledAt()))
                : null;
            $this->sendNotificationEmail(
                $this->scopeConfig->getValue(
                    self::XML_PATH_STATUS_CHANGE_TEMPLATE,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                $customer->getStoreId(),
                [
                    'subscription' => $subscriptionProfile,
                    'oldStatus' => $statusModel->getLabelByValue($oldStatus),
                    'newStatus' => $statusModel->getLabelByValue($newStatus),
                    'date' => $date,
                    'customer' => $customer
                ],
                [
                    'email' => $customer->getEmail(),
                    'name' => $customer->getFirstname() . ' ' . $customer->getLastName()
                ]
            );
        }
    }

    /**
     * @param $subscriptionProfile
     * @param $comment
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function addComment($subscriptionProfile, $comment)
    {
        if ($this->checkEmailTemplateSetting(self::XML_PATH_COMMENT_ADDED_TEMPLATE)) {
            if (is_numeric($subscriptionProfile)) {
                try {
                    $subscriptionProfile = $this->subscriptionProfileRepository->getById($subscriptionProfile);
                } catch (\Exception $e) {
                    $subscriptionProfile = null;
                }
            }
            if ($subscriptionProfile) {
                $customer = $subscriptionProfile->getCustomer();
                $this->sendNotificationEmail(
                    $this->scopeConfig->getValue(
                        self::XML_PATH_COMMENT_ADDED_TEMPLATE,
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                    ),
                    $customer->getStoreId(),
                    [
                        'subscription' => $subscriptionProfile,
                        'comment' => $comment,
                        'customer' => $customer
                    ],
                    [
                        'email' => $customer->getEmail(),
                        'name' => $customer->getFirstname() . ' ' . $customer->getLastName()
                    ]
                );
            }
        }
    }

    /**
     * @param $subscriptionProfile
     * @param $date
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function cardExpire($subscriptionProfile, $date)
    {
        if ($this->checkEmailTemplateSetting(self::XML_PATH_CARD_EXPIRE)) {
            $customer = $subscriptionProfile->getCustomer();
            $this->sendNotificationEmail(
                $this->scopeConfig->getValue(
                    self::XML_PATH_CARD_EXPIRE,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                $customer->getStoreId(),
                [
                    'subscription' => $subscriptionProfile,
                    'customer' => $customer,
                    'date' => date('F jS, Y', strtotime($date))
                ],
                [
                    'email' => $customer->getEmail(),
                    'name' => $customer->getFirstname() . ' ' . $customer->getLastName()
                ]
            );
        }
    }

    /**
     * @param $subscriptionProfile
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function paymentFailed($subscriptionProfile)
    {
        if ($this->checkEmailTemplateSetting(self::XML_PATH_PAYMENT_FAILED)) {
            $customer = $subscriptionProfile->getCustomer();
            $this->sendNotificationEmail(
                $this->scopeConfig->getValue(
                    self::XML_PATH_PAYMENT_FAILED,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                $customer->getStoreId(),
                [
                    'subscription' => $subscriptionProfile,
                    'customer' => $customer,
                    'attempt_interval' => $this->scopeConfig->getValue(
                        'tnw_subscriptions_profile_options/past_due_profile_options/attempt_interval',
                        \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                        $customer->getStoreId()
                    )
                ],
                [
                    'email' => $customer->getEmail(),
                    'name' => $customer->getFirstname() . ' ' . $customer->getLastName()
                ]
            );
        }
    }

    /**
     * @param $subscriptionProfile
     * @param $products
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function outOfStockProducts($subscriptionProfile, $products)
    {
        if ($this->checkEmailTemplateSetting(self::XML_PATH_OUT_OF_STOCK)) {
            $customer = $subscriptionProfile->getCustomer();
            $this->sendNotificationEmail(
                $this->scopeConfig->getValue(
                    self::XML_PATH_OUT_OF_STOCK,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                ),
                $customer->getStoreId(),
                [
                    'subscription' => $subscriptionProfile,
                    'customer' => $customer,
                    'products' => implode(', ', $products)
                ],
                [
                    'email' => $customer->getEmail(),
                    'name' => $customer->getFirstname() . ' ' . $customer->getLastName()
                ]
            );
        }
    }

    /**
     * @param $subscriptionProfile
     * @param $date
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function renewal($subscriptionProfile, $date)
    {
        if ($this->checkEmailTemplateSetting(self::XML_PATH_RENEWAL)) {
            $subscriptionProfiles = [];
            if (is_numeric($subscriptionProfile)) {
                try {
                    $subscriptionProfiles[] = $this->subscriptionProfileRepository->getById($subscriptionProfile);
                } catch (\Exception $e) {
                    $subscriptionProfiles = [];
                }
            } elseif (is_array($subscriptionProfile)) {
                foreach ($subscriptionProfile as $profileId) {
                    try {
                        $subscriptionProfiles[] = $this->subscriptionProfileRepository->getById($profileId);
                    } catch (\Exception $e) {
                        continue;
                    }
                }
            } else {
                $subscriptionProfiles[] = $subscriptionProfile;
            }

            if ($subscriptionProfiles) {
                $subscriptionProfile = reset($subscriptionProfiles);
                $customer = $subscriptionProfile->getCustomer();
                if ($subscriptionProfile->getStatus() == ProfileStatus::STATUS_PAST_DUE
                    || $subscriptionProfile->getStatus() == ProfileStatus::STATUS_ACTIVE
                ) {
                    $this->sendNotificationEmail(
                        $this->scopeConfig->getValue(
                            self::XML_PATH_RENEWAL,
                            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                        ),
                        $customer->getStoreId(),
                        [
                            'subscriptions' => $subscriptionProfiles,
                            'customer' => $customer,
                            'date' => date('F jS, Y', strtotime($date))
                        ],
                        [
                            'email' => $customer->getEmail(),
                            'name' => $customer->getFirstname() . ' ' . $customer->getLastName()
                        ]
                    );
                }
            }
        }
    }

    /**
     * @param $templateIdentifier
     * @param $storeId
     * @param array $vars
     * @param array $to
     * @return $this
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendNotificationEmail($templateIdentifier, $storeId, $vars = [], $to = [])
    {
        $this->inlineTranslation->suspend();
        $this->transportBuilder->setTemplateIdentifier($templateIdentifier)
            ->setTemplateOptions(
                [
                    'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                    'store' => $storeId,
                ]
            )->setTemplateVars(
                $vars
            )->setFrom(
                $this->scopeConfig->getValue(
                    self::XML_PATH_EMAIL_IDENTITY,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                )
            )->addTo(
                $to['email'],
                $to['name']
            );
        $transport = $this->transportBuilder->getTransport();
        $transport->sendMessage();
        $this->inlineTranslation->resume();
        return $this;
    }

    /**
     * @param $configPath
     * @return bool
     */
    private function checkEmailTemplateSetting($configPath)
    {
        if (!$this->scopeConfig->getValue(
                $configPath,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            ) || !$this->scopeConfig->getValue(
                self::XML_PATH_EMAIL_IDENTITY,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            )
        ) {
            return false;
        }
        return true;
    }

    /**
     * Returns next profile relation instance.
     *
     * @param SubscriptionProfile $profile
     * @return false|\TNW\Subscriptions\Api\Data\SubscriptionProfileOrderInterface
     */
    private function getNextProfileRelation(SubscriptionProfile $profile)
    {
        return $this->profileOrderManager->getNextProfileRelation($profile, false);
    }
}
