<?php
/**
* Copyright © 2021 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */
namespace TNW\Subscriptions\Api\Data;

/**
 * Interface for subscription order item extension attributes.
 */
interface CreditmemoItemExtensionAttributesInterface extends SalesExtensionAttributesInterface
{
    /**#@+
     * Constant for creditmemo item extension attributes
     */
    const CREDITMEMO_ITEM_EXTENSION_TABLE = 'tnw_subscriptions_creditmemo_item_extension_entity';
    /**#@-*/

}
