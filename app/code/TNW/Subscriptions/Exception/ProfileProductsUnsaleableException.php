<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */
namespace TNW\Subscriptions\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class ProfileProductsUnsaleableException
 * @package TNW\Subscriptions\Exception
 */
class ProfileProductsUnsaleableException extends LocalizedException
{
}
