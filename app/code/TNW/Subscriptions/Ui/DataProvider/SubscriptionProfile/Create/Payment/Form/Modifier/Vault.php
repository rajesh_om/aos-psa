<?php
/**
 * Copyright © 2018 TechNWeb, Inc. All rights reserved.
 * See TNW_LICENSE.txt for license details.
 */
namespace TNW\Subscriptions\Ui\DataProvider\SubscriptionProfile\Create\Payment\Form\Modifier;

use Magento\Framework\DataObject;
use Magento\Payment\Model\CcConfig;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Ui\Component\Form\Element\Checkbox;
use Magento\Ui\Component\Form\Field;
use Magento\Vault\Model\Ui\VaultConfigProvider;
use TNW\Subscriptions\Model\Config;
use TNW\Subscriptions\Model\QuoteSessionInterface;
use TNW\Subscriptions\Model\SubscriptionProfileOrder\Manager as OrderRelationManager;
use TNW\Subscriptions\Model\SubscriptionProfileRepository;

/**
 * Class Vault
 */
class Vault extends Base
{
    /**
     * @var string
     */
    private $tokensConfig = [];

    /**
     * @var null
     */
    private $tokensConfigProvider;

    /**
     * @var string
     */
    private $currentVaultMethod = 'vault';

    /**
     * @var array
     */
    private $vaultMethods = [];

    /**
     * @var Config
     */
    private $config;

    /**
     * @var QuoteSessionInterface
     */
    private $session;

    /**
     * @var CcConfig
     */
    private $ccConfig;

    /**
     * @var VaultConfigProvider
     */
    private $vaultConfigProvider;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    private $sessionManager;

    /**
     * @var \Magento\Vault\Api\PaymentTokenManagementInterface
     */
    private $paymentTokenManagement;

    /**
     * @var array
     */
    private $currentProfilePublicHash = [];

    /**
     * Vault constructor.
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param CcConfig $ccConfig
     * @param VaultConfigProvider $vaultConfigProvider
     * @param Config $config
     * @param QuoteSessionInterface $session
     * @param SubscriptionProfileRepository $profileRepository
     * @param OrderRelationManager $relationManager
     * @param CartRepositoryInterface $cartRepository
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Session\SessionManagerInterface $sessionManager
     * @param \Magento\Vault\Api\PaymentTokenManagementInterface $paymentTokenManagement
     * @param string $tokensConfigClass
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        CcConfig $ccConfig,
        VaultConfigProvider $vaultConfigProvider,
        Config $config,
        QuoteSessionInterface $session,
        SubscriptionProfileRepository $profileRepository,
        OrderRelationManager $relationManager,
        CartRepositoryInterface $cartRepository,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Session\SessionManagerInterface $sessionManager,
        \Magento\Vault\Api\PaymentTokenManagementInterface $paymentTokenManagement,
        $tokensConfigClass = ''
    ) {
        parent::__construct($config, $session, $profileRepository, $relationManager, $cartRepository);
        if ($tokensConfigClass) {
            $this->tokensConfigProvider = $objectManager->get($tokensConfigClass);
        } else {
            $this->tokensConfigProvider = null;
        }
        $this->paymentTokenManagement = $paymentTokenManagement;
        $this->config = $config;
        $this->session = $session;
        $this->ccConfig = $ccConfig;
        $this->vaultConfigProvider = $vaultConfigProvider;
        $this->scopeConfig = $scopeConfig;
        $this->sessionManager = $sessionManager;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        if ($this->currentProfilePublicHash) {
            $data['payment'][$this->currentProfilePublicHash['method']]['additional']['publicHash']
                = $this->currentProfilePublicHash['value'];
        }
        return $data;
    }

    /**
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        foreach ($this->vaultConfigProvider->getConfig()['vault'] as $vaultCode => $enabledConfig) {
            if (
            $this->config->isPaymentMethodAvailableForSubscription(
                str_replace(['_cc_vault', '_vault'], '', $vaultCode),
                $this->session->getStoreId()
            )
            ) {
                $this->vaultMethods[] = $vaultCode;
            }
        }
        if (empty($this->vaultMethods)) {
            return $meta;
        }
        $customerId = $this->session->getCustomerId();
        if (
            (!$this->sessionManager->getCustomerId() && $customerId)
            || $this->sessionManager->getCustomerId() != $customerId
        ) {
            $this->sessionManager->setCustomerId($customerId);
        }
        if (!$this->sessionManager->getCustomerId() && $this->getProfile()) {
            $this->sessionManager->setCustomerId($this->getProfile()->getCustomerId());
        }
        if ($this->tokensConfigProvider) {
            switch (get_class($this->tokensConfigProvider)) {
                case 'Magento\Vault\Model\Ui\TokensConfigProvider':
                    $this->processTokensConfigData($this->tokensConfigProvider->getConfig());
                    foreach ($this->vaultMethods as $method) {
                        if (empty($this->tokensConfig[$method])) continue;
                        $this->currentVaultMethod = $method;
                        $meta = array_replace_recursive(
                            $meta,
                            $this->getPaymentFields()
                        );
                    }
                    break;
                case 'Magento\Vault\Model\Ui\Adminhtml\TokensConfigProvider':
                    foreach ($this->vaultMethods as $method) {
                        $this->tokensConfig[$method] = $this->tokensConfigProvider->getTokensComponents($method);
                        if (empty($this->tokensConfig[$method])) continue;
                        $this->currentVaultMethod = $method;

                        $meta = array_replace_recursive(
                            $meta,
                            $this->getPaymentFields()
                        );
                    }
                    break;
                default: break;
            }
        }
        return $meta;
    }

    /**
     * @param $configData
     */
    protected function processTokensConfigData($configData)
    {
        if (isset($configData['payment']['vault']) && is_array($configData['payment']['vault'])) {
            foreach ($this->vaultMethods as $method) {
                foreach ($configData['payment']['vault'] as $code => $data) {
                    if (strpos($code, $method) !== false) {
                        $this->tokensConfig[$method][] = new DataObject($data);
                    }
                }
            }
        }
    }

    /**
     * @return array
     */
    protected function getAdditionalFields()
    {
        $cards = [];
        $paymentToken = null;
        if ($this->getProfile() && $this->getProfile()->getPayment()) {
            $paymentToken = $this->getProfile()->getPayment()->getPaymentToken();
        }
        if ($paymentToken) {
            try {
                $vaultToken = $this->paymentTokenManagement->getByGatewayToken(
                    $paymentToken,
                    $this->getPaymentMethodCodeByVaultCode($this->currentVaultMethod),
                    $this->getProfile()->getCustomerId()
                );
            } catch (\Exception $e) {
                $vaultToken = null;
            }
            if ($vaultToken) {
                $this->currentProfilePublicHash['value'] = $vaultToken->getPublicHash();
                $this->currentProfilePublicHash['method'] = $this->getProfile()->getPayment()->getEngineCode();
            }
        }
        foreach ($this->tokensConfig[$this->currentVaultMethod] as $ccToken) {
            $ccType = isset($ccToken->getConfig()['details']['type'])
                ? $ccToken->getConfig()['details']['type']
                : $ccToken->getConfig()['details']['cc_type'];
            $ccTypeLabel = $this->getCcTypeLabel($ccType);
            $maskedCC = isset($ccToken->getConfig()['details']['maskedCC'])
                ? $ccToken->getConfig()['details']['maskedCC']
                : $ccToken->getConfig()['details']['cc_last_4'];
            $expDate = isset($ccToken->getConfig()['details']['expirationDate'])
                ? $ccToken->getConfig()['details']['expirationDate']
                : $ccToken->getConfig()['details']['cc_exp_month']
                    . '/'
                    . $ccToken->getConfig()['details']['cc_exp_year'];
            $ccTitle = $ccTypeLabel
                . ' ending '
                . $maskedCC
                . ' (expires: '
                . $expDate
                . ')';
            $pubHash = $ccToken->getConfig()['publicHash'];
            if ($this->currentProfilePublicHash && $this->currentProfilePublicHash['value'] == $pubHash) {
                $checked = true;
            } else {
                $checked = false;
            }
            $cards[$pubHash] = [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'formElement' => Checkbox::NAME,
                            'componentType' => Field::NAME,
                            'prefer' => 'radio',
                            'description' => $ccTitle,
                            'value' => $pubHash,
                            'checked' => $checked,
                            'dataScope' => 'publicHash',
                            'elementTmpl' => 'TNW_Subscriptions/form/element/radio',
                            'validation' => [
                                'required-entry' => true
                            ],
                            'imports' => [
                                'visible' => $this->getFieldsetName() . '.additional_fields:visible',
                            ],
                        ],
                    ],
                ]
            ];
        }
        return $cards;
    }

    /**
     * @return array
     */
    protected function getAdditionalConfig()
    {
        return [
            'component' => 'TNW_Subscriptions/js/form/subscription-profile/payment/base',
            'options' => [
                'formName' => $this->getPaymentFormName(),
            ],
        ];
    }

    /**
     * @param $ccCode
     * @return mixed
     */
    protected function getCcTypeLabel($ccCode)
    {
        return $this->ccConfig->getCcAvailableTypes()[$ccCode];
    }

    /**
     * @return string
     */
    protected function getPaymentCode()
    {
        return $this->currentVaultMethod;
    }

    /**
     * @return string
     */
    protected function getPaymentTitle()
    {
        return $this->scopeConfig->getValue('payment/' . $this->getPaymentCode() . '/title');
    }

    /**
     * @param $vaultCode
     * @return mixed
     */
    private function getPaymentMethodCodeByVaultCode($vaultCode)
    {
        return str_replace(['_cc_vault', '_vault'], '', $vaultCode);
    }
}
