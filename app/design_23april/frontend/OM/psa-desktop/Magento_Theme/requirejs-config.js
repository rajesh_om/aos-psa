var config = {
    paths: {            
            'owlcarousel': "Magento_Theme/owlcarousel/js/owlcarousel",
            'aos': "Magento_Theme/owlcarousel/js/aos",
        },   
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        },
         'aos': {
            deps: ['jquery']
        },
    }
};