var config = {
    paths: {            
            'owlcarousel': "Magento_Theme/owlcarousel/js/owlcarousel"
        },   
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};