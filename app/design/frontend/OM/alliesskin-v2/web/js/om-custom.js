require(
    ['jquery', 'jquery/ui', 'owlcarousel'], 
    function($) {
        /* taking homepageslider,category and cms page  content out of maincontent to make it full width*/
         
         $('.category-image').insertBefore('#maincontent');
         $('.category-description').insertBefore('#maincontent').addClass('full-width');
         $('.content-heading').insertBefore('#maincontent').addClass('full-width');
         $(document).on('click', '.close-top', function() {
                 $(document).find('.header_label').fadeOut("slow");
            });
        
        if($('#popup-box .close').length > 0) {
            setTimeout(function() {
                if($.cookie('homepagePopup') != 1) {
                    $("#popup-box").fadeIn();
                    $.cookie('homepagePopup', 1);
                }
            }, 3000);
            $(document).on('click', '#popup-box .close', function() {
                $(this).parent("div").fadeOut();
            });
        }
        /*regimen pop up open and close*/
        if(!$('.cms-home').length > 0) {
            setTimeout(function() {
                if($.cookie('regimenPopup') != 1) {
                    $("#regimen-popup").fadeIn();
                    $.cookie('regimenPopup', 1);
                }
            }, 2000);
            $(document).on('click', '.regimen-close', function() {
                $(this).parents("#regimen-popup").fadeOut();
            });
        }
        if($(document).find('.product-attr').length > 0) {
            var priceSymbole = $(document).find('.product-attr').data('symbol');
            var price = $(document).find(".price-wrapper").data("price-amount");
            var optionPrice = $(document).find(".select-options option:selected").attr("price");
            var total = price;
            if( typeof optionPrice != 'undefined' ) {
                total = parseInt(price) + parseInt(optionPrice);
            }
            /*$(document).find(".price-wrapper .price").text(priceSymbole+total.toFixed(2));*/
            $(document).on('change', '.select-options', function() {
                var target = $(this).parent("div").parent("div");
                price = target.find(".price-wrapper").data("price-amount");
                optionPrice = $(this).find("option:selected").attr("price");
                total = price;
                if( typeof optionPrice != 'undefined' ) {
                    total = parseInt(price) + parseInt(optionPrice);
                }
                target.find(".price-wrapper .price").html(priceSymbole+total.toFixed(2));
            });
        }
        


        if($('.agenda-box').length > 0) {
            $('.agenda-box').owlCarousel({
                loop: true,
                margin: 0,
                slideSpeed: 1500,
                dots: true,
                items: 4,
                autoplay:true,
                responsiveClass: true,
                responsive: responsive(),
                scrollPerPage: true,
                lazyLoad: true,
                navigation: false,
                slideBy: 4,
            });
        }

        if($('ol.related-product').length > 0) {
            $('ol.related-product').owlCarousel({
                loop: false,
                margin: 0,
                slideSpeed: 1500,
                dots: false,
                items: 3,
                autoplay:false,
                responsiveClass: true,
                scrollPerPage: true,
                responsive: responsive(),
                lazyLoad: true,
                nav: true,
                slideBy: 3
            });
        }

        /* add slider for all kit packeges */
        if($('.product-gallery-image').length > 0) {
            $('.product-gallery-image').owlCarousel({
                loop: false,
                margin: 0,
                slideSpeed: 1500,
                dots: true,
                autoplay:true,
                items: 1,
                scrollPerPage: true,
                lazyLoad: true,
                navigation: false,
                slideBy: 1,
            });
        }

        function responsive() {
            return {
                0: {
                    items: 1,
                    loop: false,
                    margin: 0,
                    slideBy: 1
                },
                600: {
                    items: 3,
                    loop: false,
                    margin: 0,
                    slideBy: 3
                },
                1000: {
                    items: 4,
                    loop: false,
                    margin: 0,
                    slideBy: 4
                }
            }
        }

        /* quantity increment / decrement */
        var __target = $(document).find(".product-qty");
        if(__target.length > 0) {
            var oldValue = __target.val();
            var newValue = 0;
            $(document).on('click', '#increment', function(e) {
                e.preventDefault();
                e.stopPropagation();
              if(oldValue > 0) {
                newValue = Number(oldValue) + 1;
                __target.val(newValue);
                $(document).find(".bottom-product-qty").val(newValue);
                oldValue = newValue;
              }
            });

            $(document).on('click', '#decerement', function(e) {
                e.preventDefault();
                e.stopPropagation();
              if(oldValue > 1) {
                newValue = Number(oldValue) - 1;
                __target.val(newValue);
                $(document).find(".bottom-product-qty").val(newValue);
                oldValue = newValue;
              }
            });
        }

        /* back to top scroll */
        $(document).on("click","#toTop", function() {
            $('html, body').animate({
                scrollTop: 0
            }, 1000);
        });
        $("#toTop").css("display", "none");
        $(document).scroll(function() {
            var position = $(window).scrollTop();
           
            if (position >= 500) {
                $("#toTop").fadeIn(300);
            } else {
                $("#toTop").fadeOut(300);
            }
        });

        /* Show home page content after window load */
        $('.cms-home #maincontent').show();

        /* Wrapping product gallery and product conatiner into a single container*/
        $('.product-info-main').wrapInner('<div class="product-info-main-container"></div>');

        /* opening a menu from left side */
        $('.header-main-menu').click(function(){
            $(this).parent('.main-menu').siblings('.slide-menu-show').toggleClass('active');
        });

        /* Closing a menu to left side */
        $('.close-inner-menu').click(function(){
            $(this).parents('.slide-menu-show').toggleClass('active');
        });

        /* Open login container on login menu click */
        $('.mobile-only').click(function(){
           $(this).parents('.slide-menu-show').removeClass('active');
        });

         $('.reviews-actions .add').click(function(){
               
            $(this).parents('.column').find('.review-add').addClass('active');
        });
        
        /* back to main menu from login container*/

        $('.back-to-main-menu a').click(function(){
            $(this).parents('.login-overlay').siblings('#custom-header-id').find('.slide-menu-show').addClass('active');
            $(this).parents('.login-overlay').hide();
            $(this).parents('.login-overlay').removeClass('active');
        });

        $('.close-login-menu').click(function(){
            $(this).parents('.login-overlay').hide();
            $(this).parents('.login-overlay').removeClass('active');
        });

        $('.authorization-link').click(function(){
            $(this).parents('#custom-header-id').siblings('.login-overlay').addClass('active');
        });
        $('.register-link').click(function(){
            $(this).parents('#custom-header-id').siblings('.login-overlay').find('.login-container').hide();
            $(this).parents('#custom-header-id').siblings('.login-overlay').find('.register-container').show();
        });
        $('.mobile-only').click(function(){
             $(this).parents('#custom-header-id').siblings('.login-overlay').find('.register-container').hide();
             $(this).parents('#custom-header-id').siblings('.login-overlay').find('.login-container').show();
        });
        $('.menu-overlay').click(function(){
            $(this).siblings('.slide-menu-show').removeClass('active');
        });
        $('body').on("click",".more, .less",function(){
            var obj = $(this);
            var currentQty = obj.siblings('.cart-item-qty').val();
            var iid = obj.siblings('.update-cart-item').attr('data-cart-item');

            if(obj.hasClass('more')){
                var newAdd = parseInt(currentQty)+parseInt(1);
                obj.siblings('.cart-item-qty').val(newAdd);
                obj.siblings('.cart-item-qty').attr('data-item-qty',newAdd);
                $('#update-cart-item-'+iid).click();
            }else{
                if(currentQty > 0){
               var newAdd = parseInt(currentQty)-parseInt(1);
                obj.siblings('.cart-item-qty').val(newAdd); 
                obj.siblings('.cart-item-qty').attr('data-item-qty',newAdd);
                $('#update-cart-item-'+iid).click();
                }
            }
        });

        $('.stamped-product-reviews-badge').click(function(){
            var attrUrl = $(this).attr('product_url');
            var stampedId = '#review-loader';
            window.location.href = attrUrl.concat(stampedId);
        });
        if(window.location.href.indexOf("review-loader") > -1) {
            $('#review-loader').show();
            setTimeout(function() {
                 $('#review-loader').hide();
            }, 3700);
        }
        $('#notify-product-btn').click(function(e){
            $('.amxnotif-block').addClass('show');
        });
        $('.close-popup-subs').click(function(e){
            
            $(this).parents('.amxnotif-block').removeClass('show');
        });
        $('.close-registration-success').click(function(){
            $(this).parents('.registration-after-success-container').hide();
        });
       

        /*Open Christmas pop up through specific url*/

        if (window.location.href.indexOf("?hohoho") > -1) {
            setTimeout(function(){ 
                $("#popup-box").css('display','none !important');
                $("#popup-box").hide();

            }, 3000);
            
            setTimeout(function(){ 
                $('.christmas-popup').show();
                $('.christmas-popup').addClass('active');
            }, 2000);
          
        }

        $('.close-christmas-popup').click(function(){
            $(this).parents('.christmas-popup').hide();
            $(this).parents('.christmas-popup').removeClass('active');
        });
        
        /*Open Christmas pop up through specific url end*/

/*For Subscription page review slider start*/
jQuery(document).ready(function($) {
"use strict";
jQuery('#customers-reviews').owlCarousel( {
        loop: true,
        center: true,
        items: 1,
        margin: 0,
        autoplay: false,
        dots:true,
        nav:true,
        
        smartSpeed: 450,
    navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 1
            },
            1170: {
                items: 1
            }
        }
    });
});

/*For Subscription page review slider end*/
        
    }
);