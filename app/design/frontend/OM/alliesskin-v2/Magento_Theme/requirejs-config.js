var config = {
    paths: {            
            'owlcarousel': "Magento_Theme/owlcarousel/js/owlcarousel",
            'customscroll': "Magento_Theme/owlcarousel/js/customscroll",
            'aos': "Magento_Theme/owlcarousel/js/aos",
            'backdetect':"Magento_Theme/owlcarousel/js/backdetect",
        },   
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        },
        'customscroll': {
            deps: ['jquery']
        },
         'aos': {
            deps: ['jquery']
        },
        'backdetect': {
            deps: ['jquery']
        },
    }
};