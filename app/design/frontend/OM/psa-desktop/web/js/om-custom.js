require(
    ['jquery', 'jquery/ui', 'owlcarousel'], 
    function($) {
		
		$('.home-slider').owlCarousel({
			loop: true,
			margin: 0,
			slideSpeed: 1200,
			dots: false,
			items: 1,
			autoplay:true,
			responsiveClass: true,
			scrollPerPage: true,
			lazyLoad: true,
			nav: false,
			slideBy: 1,
			autoPlay: 7000
		});
        
        $('.preshslider').owlCarousel({
			loop: true,
			margin: 0,
			slideSpeed: 1200,
			dots: false,
			items: 1.20,
			autoplay:true,
			responsiveClass: true,
			scrollPerPage: true,
			lazyLoad: true,
			nav: false,
			slideBy: 1,
			autoPlay: 7000
		});
		
		$('.mobile-preshslider').owlCarousel({
			loop: true,
			margin: 0,
			slideSpeed: 1200,
			dots: false,
			items: 1,
			autoplay:true,
			responsiveClass: true,
			scrollPerPage: true,
			lazyLoad: true,
			nav: false,
			slideBy: 1,
			autoPlay: 7000
		});
		
		
		if ($(window).width() < 768) {
			
			/*for the mobile header fixed position*/
			
			$(window).scroll(function() {    
				var scroll = $(window).scrollTop();
				var headerTopBarOuter = $(".header-topbar").outerHeight();
				if (scroll >= headerTopBarOuter) {
					$("#custom-header").addClass("scrolled");
					$(".header-fixed-options").addClass("scrolled");
				}
				else{
					$("#custom-header").removeClass("scrolled");
					$(".header-fixed-options").removeClass("scrolled");
				}
			});

			$('.top-close').click(function(){
				$(".header-topbar").fadeOut(500);
				$(this).parents('.page-header').find('#custom-header').addClass('shift-to-top');
				$(this).parents('.page-header').find('.header-fixed-options').addClass('shift-to-top');
			});
			
			$('.menu-toogler').click(function(){
				$(".menu-links").toggleClass("active");
			});
			
		} else {
			
			/*for the desktop header sticky and cart box fixed position*/
		
			$(window).scroll(function() {    
				var scroll = $(window).scrollTop();
				var headerTopBarOuter = $(".header-topbar").outerHeight();
				if (scroll >= headerTopBarOuter) {
					$(".header-fixed-options").addClass("scrolled");
					$(".product-info-customsidebar").addClass("scrolledup");
					$(".storynavigation").addClass("scrolledup");
					$("#layered-filter-block").addClass("scrolledup");
					
				}
				else{
					$(".header-fixed-options").removeClass("scrolled");
					$(".product-info-customsidebar").removeClass("scrolledup");
					$(".storynavigation").removeClass("scrolledup");
					$("#layered-filter-block").removeClass("scrolledup");
				}
			});

			$('.top-close').click(function(){
				$(".header-topbar").fadeOut(500);
				$(this).parents('.page-header').find('.header-fixed-options').addClass('shift-to-top');
			}); 
			
		}
        
        
        $(document).ready(function(){ 
			$('.tab-a').click(function(){  
				$(".tab").removeClass('tab-active');
				$(".tab[data-id='"+$(this).attr('data-id')+"']").addClass("tab-active");
				$(".tab-a").removeClass('active-a');
				$(this).parent().find(".tab-a").addClass('active-a');
				$(".closebutton").click(function(){
					$(".modalbox").fadeOut(500).removeClass("tab-active");
				});
			});
        });
        
		
		$('.faqlistingsection button.parent-accordion').click(function() {
			$(this).siblings('.panel').slideToggle();
            $(this).parents('li').siblings('li').find('.panel').slideUp();
            $(this).toggleClass('plus');
            $(this).toggleClass('minus');
            if($(this).parents('li').siblings('li').find('.parent-accordion').hasClass('minus')){
				$(this).parents('li').siblings('li').find('.parent-accordion').removeClass('minus');
                $(this).parents('li').siblings('li').find('.parent-accordion').addClass('plus');
			}
		});
            
			
        $('.panel li button.accordion').click(function() {
			$(this).siblings('.panel-child').slideToggle();
            $(this).parents('li').siblings('li').find('.panel-child').slideUp();
            $(this).toggleClass('plus');
            $(this).toggleClass('minus');
            if($(this).parents('li').siblings('li').find('.accordion').hasClass('minus')){
				$(this).parents('li').siblings('li').find('.accordion').removeClass('minus');
                $(this).parents('li').siblings('li').find('.accordion').addClass('plus');
			}
		});
        
		
        if (window.location.href.indexOf("our-story") > -1) {
            $('.our-story a').addClass('active');
            $('.our-story a').parents('.menu-item').siblings().find('a').removeClass('active');
        }
        
		
		if (window.location.href.indexOf("nofilter") > -1) {
            $('.nofilter a').addClass('active');
            $('.nofilter a').parents('.menu-item').siblings().find('a').removeClass('active');
        }
        
		
		if (window.location.href.indexOf("jumpstart-your-glow") > -1) {
			$('.jumpstart-glow a').addClass('active');
            $('.jumpstart-glow a').parents('.menu-item').siblings().find('a').removeClass('active');
        }
        
		
		if (window.location.href.indexOf("get-schooled") > -1) {
            $('.get-schooled a').addClass('active');
            $('.get-schooled a').parents('.menu-item').siblings().find('a').removeClass('active');
        }
        
		
		if (window.location.href.indexOf("shop") > -1) {
            $('.shop a').addClass('active');
            $('.shop a').parents('.menu-item').siblings().find('a').removeClass('active');
        }


        if($('#popup-box .close').length > 0) {
            setTimeout(function() {
                if($.cookie('homepagePopup') != 1) {
                    $("#popup-box").fadeIn();
                    $.cookie('homepagePopup', 1);
                }
            }, 3000);
            $(document).on('click', '#popup-box .close', function() {
                $(this).parent("div").fadeOut();
            });
        }


        $('.diamond-box a').click(function(){
            $('.swell-tab').click();
        });
		

        $('.chat-box a').click(function(){
            $('.freshwidget-container').css('display','block');
            $('#freshwidget-button a').click();
            $('.widget-close').click();
            $('.freshwidget-container').css('display','block');
        });
		

        $('.eye-icon').click(function(){
            $(this).parents('.product-item').find('.bss-quickview').click();
        });
		

        $('.mobile-menu-icon').click(function(){
            $(this).siblings('.menu-links').toggle();
        });
				
        $('body').on("click",".more, .less",function(){
            var obj = $(this);
            var currentQty = obj.siblings('.cart-item-qty').val();
            var iid = obj.siblings('.update-cart-item').attr('data-cart-item');

            if(obj.hasClass('more')){
                var newAdd = parseInt(currentQty)+parseInt(1);
                obj.siblings('.cart-item-qty').val(newAdd);
                obj.siblings('.cart-item-qty').attr('data-item-qty',newAdd);
                $('#update-cart-item-'+iid).click();
            }else{
                if(currentQty > 0){
               var newAdd = parseInt(currentQty)-parseInt(1);
                obj.siblings('.cart-item-qty').val(newAdd); 
                obj.siblings('.cart-item-qty').attr('data-item-qty',newAdd);
                $('#update-cart-item-'+iid).click();
                }
            }
        });
		
		
		/*for the Get Schooled page Content box opening and closing*/
		
		$('.gs-grid-titles').click(function(){
            $('.gs-grid-block').removeClass('open');
            $(this).closest('.gs-grid-block').addClass('open');
        });
		
		$('.gs-close-btn').click(function(){
            $(this).closest('.gs-grid-block').removeClass('open');
        });
		
		/*for the product page tabs toggling*/
		
		$('.pro-bg-title').click(function(){
            $(this).children('.content-tab-toggler').toggleClass('plus');
            $(this).next('.product-info-content').toggleClass('close');
        }); 

        if ($(window).width() < 768) {
            var innertext = $('#account-nav').find('li.nav.item.current').text();
            $('.title.account-nav-title').find('strong').text(innertext);
        }
		
		
		/*for the product page mobile view add to cart bar sticky*/
		
		/*if ($(window).width() < 768) {
			
			$(window).scroll(function() {  
				var hT = $('.page-footer').offset().top,
				   hH = $('.page-footer').outerHeight(),
				   wH = $(window).height(),
				   wS = $(this).scrollTop();
				   
				   alert(hT); 6360.18
				   
				   alert(hH); 404
				   
				   alert(wH); 568
				   
				   alert(wS); 6195
				   
			   if (wS > (hT-hH)){
				   $('.product-info-customsidebar').addClass('hide');
			   }				   
			   else{
				   $('.product-info-customsidebar').removeClass('hide');
			   }		
			});
			
		}*/
        $('.shoplinks').click(function(){
            $('.menu-links').addClass("menuslideup");
        });
         $('.mobile-menu-icon').click(function(){
            $('.menu-links').removeClass("menuslideup");
        });
		
		
		
		/*for the product page ingredient info pop-up on the click of ingredients*/
		
		$('.key-ingredients-list li').click(function(){
            $('.product-keyingredient-content').addClass("open");
			$('.ing-info').css("display", "none");
			$(this).find('.ing-info').css("display", "block");
        });
		
		$(document).click(function(e){
			// Check if click was triggered on or within #menu_content
			if( $(e.target).closest(".key-ingredients-list").length <= 0 ) {
				$('.product-keyingredient-content').removeClass("open");
				$('.ing-info').css("display", "none");
			}
		});
		
		
});

